#+======================================================================
# $HeadURL: https://svnpub.iter.org/codac/iter/codac/dev/units/m-epics-asyn/trunk/epics-asyn.spec $
# $Id: epics-asyn.spec 28398 2012-06-04 13:31:40Z zagara $
#
# Project       : CODAC Core System
#
# Description   : Spec file
#
# Author(s)     : Cosylab
#
# Copyright (c) : 2010-2012 ITER Organization,
#                 CS 90 046
#                 13067 St. Paul-lez-Durance Cedex
#                 France
#
# This file is part of ITER CODAC software.
# For the terms and conditions of redistribution or use of this software
# refer to the file ITER-LICENSE.TXT located in the top level directory
# of the distribution package.
#
#-======================================================================
%define archive_dir epics-nds
%define install_dir %{epics_modules}/nds

Name:           %{codac_rpm_prefix}-epics-nds
Summary:        Nominal Device Support -- library for writing drivers with a standardized interface.
Version:        %{codac_version_full}.v%{unit_version_full}
Release:        %{codac_packaging_release}
Packager:       ITER Organization
Vendor:         Cosylab
License:        LGPL
Group:          Development/CODAC
URL:		http://www.cosylab.com

Source0:	epics-nds.tar.gz

BuildRoot:      %{_tmppath}/%{name}-%{unit_version_full}-%{release}-root-%(%{__id_u} -n)

%provides_self

BuildRequires:  %{codac_rpm_prefix}-epics-devel 
BuildRequires:	chrpath

%requires_current %{codac_rpm_prefix}-epics
%requires_current %{codac_rpm_prefix}-epics-asyn

AutoReq:        no


%description
Nominal Device Support (NDS) is a development library that enables developers
of drivers (e.g., for data acquisition and image acquisition) to write a
driver in a concise manner, while ensuring a consistent API in terms of
PV names.


%package doc
Summary:        Documentation files for Nominal Device Support (NDS)
Group:          Development/CODAC
Requires:       %{name} = %{version}-%{release}
%provides_self doc

%description doc
Documentation files for Nominal Device Support (NDS).

%package devel
Summary:        Development files for EPICS Nominal Device Support (NDS)
Group:          Development/CODAC
Requires:       %{name} = %{version}-%{release}
%provides_self devel

%description devel
Development files for EPICS Nominal Device Support (NDS).

#%package opi
#Summary:        Generic OPI files for EPICS Nominal Device Support (NDS).
#Group:          Development/CODAC
#Requires:       %{name} = %{version}-%{release}
#%requires_current %{codac_rpm_prefix}-epics-edm
#%provides_self devel
#
#%description opi
#Generic OPI files for EPICS Nominal Device Support (NDS).

%package src
Summary:    EPICS Nominal Device Support (NDS) sources
Group:      Development/CODAC
Requires:   %{name} = %{version}-%{release}
%provides_self src
AutoReq:    no

%description src
EPICS Nominal Device Support (NDS) sources.

%package tests
Summary:    EPICS Nominal Device Support (NDS) tests
Group:      Test/CODAC
Requires:   %{name} = %{version}-%{release}

%description tests
EPICS Nominal Device Support (NDS) tests.

%prep
%setup -n %{archive_dir}
# force configuration
cat > configure/RELEASE <<___
LINUX_GPIB=NO
EPICS_BASE=%{epics_base}
ASYN=%{epics_modules}/asyn
___

%build
make
make doc

%install
rm -rf %{buildroot}
install -d %{buildroot}%{install_dir}
cp -r lib %{buildroot}%{install_dir}/
cp -r include %{buildroot}%{install_dir}/
cp -r doc %{buildroot}%{install_dir}/
cp -r dbd %{buildroot}%{install_dir}/
cp -r ndsApp %{buildroot}%{install_dir}/
cp -r configure %{buildroot}%{install_dir}/
cp LICENSE %{buildroot}%{install_dir}/
cp Makefile %{buildroot}%{install_dir}/
mkdir -p %{buildroot}%{epics_root}/base/bin/%{epics_host_arch}
mkdir -p %{buildroot}%{codac_root}/tests/nds
cp -r test/data %{buildroot}%{codac_root}/tests/nds/
cp -r test/* %{buildroot}%{codac_root}/tests/nds/
#install -d %{buildroot}%{codac_opi}/edm
#cp -r opi/edm/*.edl %{buildroot}%{codac_opi}/edm/


# templates
mkdir -p %{buildroot}%{epics_root}/base/templates/makeBaseApp/top
cp -r templates/* %{buildroot}%{epics_root}/base/templates/makeBaseApp/top
rm %{buildroot}%{epics_root}/base/templates/makeBaseApp/top/Makefile
#chmod a+x %{buildroot}%{epics_root}/base/templates/makeBaseApp/top/ndsBoot/ioc/st.cmd

# clean sources
#make -C %{buildroot}%{install_dir} clean-all
#sed -i -e 's|$(TOP)/nds|$(TOP)/src|' %{buildroot}%{install_dir}/src/Makefile
#make -C %{buildroot}%{install_dir}/src realclean
#make -C %{buildroot}%{install_dir}/configure realclean
rm -r %{buildroot}%{install_dir}/configure

# Searching ELF shared libraries.
ELF_FILES=
ELF_MAGIC=`echo $"\177ELF"`

for file in `find %{buildroot}%{install_dir}/lib -type f`; do
    MAGIC_NO=`head --bytes 4 $file`
    if [ "$MAGIC_NO" = $'\177ELF' ]; then
        ELF_FILES="$ELF_FILES $file"
    fi
done

# replace build path for destination path (install path)
BUILD_LIB_DIR=%{_builddir}/%{archive_dir}/lib/%{epics_host_arch}
DEST_LIB_DIR=%{install_dir}/lib/%{epics_host_arch}

for elf in $ELF_FILES; do
    ORIGINAL_PERMISSION=`stat  --format='%a' $elf`
    NEW_PATHS=`chrpath -l $elf | awk -F '=' '/RPATH=/{print $2}' | sed "s#$BUILD_LIB_DIR#$DEST_LIB_DIR#"`
    chmod u+w $elf
    chrpath -r $NEW_PATHS $elf
    chmod $ORIGINAL_PERMISSION $elf
done

# make sure .a files are writeable for rpmbuild's strip phase
for file in %{buildroot}%{install_dir}/lib/%{epics_host_arch}/*.a; do
  chmod u+w $file
done

%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%dir %{codac_root}
%dir %{epics_root}
%dir %{epics_modules}
%dir %{install_dir}
%dir %{install_dir}/dbd
%{install_dir}/dbd/nds.dbd
%dir %{install_dir}/lib
%dir %{install_dir}/lib/%{epics_host_arch}
%{install_dir}/lib/%{epics_host_arch}/libnds.so
%{install_dir}/LICENSE
%dir %{codac_root}/tests
%{codac_root}/tests/nds

%files doc
%defattr(-,root,root,-)
%dir %{codac_root}
%dir %{epics_root}
%dir %{epics_modules}
%dir %{install_dir}
%docdir %{install_dir}/doc
%{install_dir}/doc

%files devel
%defattr(-,root,root,-)
%dir %{codac_root}
%dir %{epics_root}
%dir %{epics_root}/base
%dir %{epics_root}/base/bin
%dir %{epics_root}/base/bin/%{epics_host_arch}
%dir %{epics_root}/base/templates/makeBaseApp/top
%dir %{epics_modules}
%dir %{install_dir}
%dir %{install_dir}/lib
%dir %{install_dir}/lib/%{epics_host_arch}
%{install_dir}/lib/%{epics_host_arch}/libnds.a
%{install_dir}/lib/%{epics_host_arch}/libnds.so
%{install_dir}/include
%{epics_root}/base/templates/makeBaseApp/top

#%files opi
#%defattr(644,root,root,-)
#%dir %{codac_root}
#%dir %{codac_opi}
#%{codac_opi}/edm

%files src
%defattr(-,root,root,-)
%dir %{codac_root}
%dir %{epics_root}
%dir %{epics_modules}
%dir %{install_dir}
%{install_dir}/ndsApp
%{install_dir}/Makefile

%files tests
%defattr(-,root,root,-)
%dir %{codac_root}
%dir %{codac_root}/tests
%{codac_root}/tests/nds

%changelog
* Wed Jul 11 2012 Klemen Zagar <klemen.zagar@cosylab.com>
- Initial version.
