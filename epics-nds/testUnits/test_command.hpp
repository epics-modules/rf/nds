/*
 * test_adiochannel.hpp
 *
 *  Created on: 5. okt. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef TEST_ADIOCHANNEL_HPP_
#define TEST_ADIOCHANNEL_HPP_

#include "ndsDriverCommand.h"

class DriverCommandUnitTestSuite : public CxxTest::TestSuite
{
  public:
	/* TEST CASES */
	void testReasonOnly()
	{
		std::string drvInfo = "reason";
		nds::DriverCommand command(drvInfo);

		TSM_ASSERT_EQUALS("", command.getCommand(), drvInfo);
		TSM_ASSERT_EQUALS("", command.getReason(), drvInfo);
	}


	void testEmptyArgList()
	{
		std::string drvInfo = "reason()";
		nds::DriverCommand command(drvInfo);

		TSM_ASSERT_EQUALS("", command.getCommand(), drvInfo);
		TSM_ASSERT_EQUALS("", command.getReason(), "reason");
	}

	void testParseOneArgumnet()
	{
		std::string drvInfo = "reason(arg1)";
		nds::DriverCommand command(drvInfo);

		TSM_ASSERT_EQUALS("", command.getCommand(), drvInfo);
		TSM_ASSERT_EQUALS("", command.getReason(), "reason");

		TSM_ASSERT_EQUALS("", command.Args.size(), 1);
		TSM_ASSERT_EQUALS("", command.Args[0], "arg1");
	}

	void testParseMultipleArguments()
	{
		nds::DriverCommand command("reason(arg1, arg2, arg3, arg4)");
		TSM_ASSERT_EQUALS("", command.Args.size(), 4);
		TSM_ASSERT_EQUALS("", command.Args[0], "arg1");
		TSM_ASSERT_EQUALS("", command.Args[1], "arg2");
		TSM_ASSERT_EQUALS("", command.Args[2], "arg3");
		TSM_ASSERT_EQUALS("", command.Args[3], "arg4");
	}

	void testError1()
	{
		std::string drvInfo = "reason)(";
		nds::DriverCommand command(drvInfo);

		TSM_ASSERT_EQUALS("", command.getCommand(), drvInfo);
		TSM_ASSERT_EQUALS("", command.getReason(), drvInfo);

	}

	void testError2()
	{
		std::string drvInfo = "reason((arg1,arg2,arg3)";
		nds::DriverCommand command(drvInfo);

		TSM_ASSERT_EQUALS("", command.getCommand(), drvInfo);
		TSM_ASSERT_EQUALS("", command.getReason(), "reason");
		TSM_ASSERT_EQUALS("", command.Args.size(), 3);
		TSM_ASSERT_EQUALS("", command.Args[0], "(arg1");
		TSM_ASSERT_EQUALS("", command.Args[1], "arg2");
		TSM_ASSERT_EQUALS("", command.Args[2], "arg3");

	}

	void testError3()
	{
		std::string drvInfo = "reason(arg1,arg2,arg3))";
		nds::DriverCommand command(drvInfo);

		TSM_ASSERT_EQUALS("", command.getCommand(), drvInfo);
		TSM_ASSERT_EQUALS("", command.getReason(), "reason");
		TSM_ASSERT_EQUALS("", command.Args.size(), 3);
		TSM_ASSERT_EQUALS("", command.Args[0], "arg1");
		TSM_ASSERT_EQUALS("", command.Args[1], "arg2");
		TSM_ASSERT_EQUALS("", command.Args[2], "arg3");
	}

	void testEmptyArgs()
	{
		std::string drvInfo = "reason(arg1,arg2,arg3,)";
		nds::DriverCommand command(drvInfo);

		TSM_ASSERT_EQUALS("", command.getCommand(), drvInfo);
		TSM_ASSERT_EQUALS("", command.getReason(), "reason");
		TSM_ASSERT_EQUALS("", command.Args.front(), "arg1");
		TSM_ASSERT_EQUALS("", command.Args.size(), 4);
		TSM_ASSERT_EQUALS("", command.Args[1], "arg2");
		TSM_ASSERT_EQUALS("", command.Args[2], "arg3");
		TSM_ASSERT_EQUALS("", command.Args[3], "");
	}

	void testEmptyLastArg()
	{
		std::string drvInfo = "reason(,,,)";
		nds::DriverCommand command(drvInfo);

		TSM_ASSERT_EQUALS("", command.getCommand(), drvInfo);
		TSM_ASSERT_EQUALS("", command.getReason(), "reason");
		TSM_ASSERT_EQUALS("", command.Args.front(), "");
		TSM_ASSERT_EQUALS("", command.Args.size(), 4);
	}

};



#endif /* TEST_ADIOCHANNEL_HPP_ */
