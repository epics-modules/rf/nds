/*
 * test_channelgroup.hpp
 *
 *  Created on: 5. okt. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef TEST_CHANNELGROUP_HPP_
#define TEST_CHANNELGROUP_HPP_

#include "ndsChannelGroup.h"

using namespace std;

class ChannelGroupUnitTest: public nds::ChannelGroup
{
	friend class ChannelGroupUnitTestSuite;
public:
	ChannelGroupUnitTest(const std::string& prefix) : nds::ChannelGroup(prefix)
	{}

	virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers)
	{
		ChannelGroup::registerHandlers(pvContainers);

		NDS_TRC("TestDevice::registerHandlers");
		return ndsSuccess;
	}
};

class ChannelGroupUnitTestSuite : public CxxTest::TestSuite
{
  public:

	/* TEST CASES */
	void testCreateChannelGroup()
	{
		ChannelGroupUnitTest channelGroup("ai");
	}

	void testHandlersRegistration()
	{
		nds::PVContainers containers;
		ChannelGroupUnitTest channelGroup("ai");
		ndsStatus result;
		reason_type reason;
		int portAddress=1;

		channelGroup._portAddr = portAddress;
		channelGroup._portName = "testPort";

		result = channelGroup.registerHandlers(&containers);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		result = containers.getReason(portAddress, "Command", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		result = containers.getReason(portAddress, "State", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
	}

};



#endif /* TEST_CHANNELGROUP_HPP_ */
