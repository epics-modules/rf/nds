/*************************************************************************
 *
 * Project       : NDS
 *
 * Description   : nds::Message class unit test
 *
 * Author        : Slava Isaev (Cosylab)
 *
 * Copyright (c) : 2010-2012
 ************************************************************************/

#ifndef MESSAGE_TEST_H_
#define MESSAGE_TEST_H_


#include <cstdio>
#include <cstring>
#include <iostream>
#include "ndsMessage.h"
using namespace std;


class MessageTestSuite : public CxxTest::TestSuite
{
  public:


	/* TEST CASES */

	void testCreateMessage()
	{
		nds::Message message;
    }

	void testMessage()
	{
		nds::Message message;

		std::string msg("MSG_TYPE PARAM1=VALUE1,param2=value2");
		message.load(msg);

		TSM_ASSERT_EQUALS("Message type is read incorrectly", message.messageType,"MSG_TYPE" );
		TSM_ASSERT_EQUALS("Message is read incorrectly", message.parameters["PARAM1"],"VALUE1" );
		TSM_ASSERT_EQUALS("Message is read incorrectly", message.parameters["param2"],"value2" );

    }

	void testMessageCommandOnly()
	{
		nds::Message message;

		std::string msg("MSG_CommandOnly");
		message.load(msg);

		TSM_ASSERT_EQUALS("Message type is read incorrectly", message.messageType, msg );
    }

	void testLoadFromChar()
	{
		nds::Message message;

		std::string msg("MSG_TYPE PARAM1=VALUE1,param2=value2");
		size_t len = msg.length()+1;
		char *buff = (char*)malloc(msg.length()+1);
		snprintf(buff, len, "%s",msg.c_str() );
		TSM_ASSERT_EQUALS("Not all string was read", msg, buff );

		size_t written;
		message.load(buff, msg.length(), &written);
		TSM_ASSERT_EQUALS("Not all string was read", written, msg.length() );
		TSM_ASSERT_EQUALS("Message type is read incorrectly", message.messageType,"MSG_TYPE" );
		TSM_ASSERT_EQUALS("Message is read incorrectly", message.parameters["PARAM1"],"VALUE1" );
		TSM_ASSERT_EQUALS("Message is read incorrectly", message.parameters["param2"],"value2" );
    }

	void testLoadErroString1()
	{
		nds::Message message;
		std::string msg("MSG_TYPE PARAM1=VALUE1, param2=value2");
		message.load(msg);
		TSM_ASSERT_EQUALS("Message type is read incorrectly", message.messageType,"MSG_TYPE" );
		TSM_ASSERT_EQUALS("Message is read incorrectly", message.parameters["PARAM1"],"VALUE1" );
		TSM_ASSERT_EQUALS("Message is read incorrectly", message.parameters["param2"],"value2" );
    }


	void testLoadErroString2()
	{
		nds::Message message;
		std::string msg("MSG_TYPE PARAM1=,param2=value2");
		message.load(msg);
		TSM_ASSERT_EQUALS("Message type is read incorrectly", message.messageType,"MSG_TYPE" );
		TSM_ASSERT_EQUALS("Message is read incorrectly", message.parameters["PARAM1"],"" );
		TSM_ASSERT_EQUALS("Message is read incorrectly", message.parameters["param2"],"value2" );
    }

	void testLoadErroString3()
	{
		nds::Message message;
		std::string msg("MSG_TYPE PARAM1= param2=value2");
		message.load(msg);
		TSM_ASSERT_EQUALS("Message type is read incorrectly", message.messageType,"MSG_TYPE" );
		TSM_ASSERT_DIFFERS("Message is read incorrectly", message.parameters["PARAM1"],"VALUE1" );
		TSM_ASSERT_DIFFERS("Message is read incorrectly", message.parameters["param2"],"value2" );
    }

	void testOutMessage1()
	{
		nds::Message message;
		std::string msg("MSG_TYPE PARAM1=VALUE1,param2=value2");
		message.load(msg);

		TSM_ASSERT_EQUALS("Message type is read incorrectly", message.messageType,"MSG_TYPE" );
		TSM_ASSERT_EQUALS("Message read incorrectly", message.parameters["PARAM1"],"VALUE1" );
		TSM_ASSERT_EQUALS("Message read incorrectly", message.parameters["param2"],"value2" );

		TSM_ASSERT_EQUALS("Message encoded incorrectly", message.str(), msg );
    }

	void testOutMessage2()
	{
		nds::Message message;
		std::string msg("MSG_TYPE PARAM1=VALUE1,param2=value2");

		message.messageType="MSG_TYPE";
		message.insert("PARAM1","VALUE1");
		message.insert("param2","value2");

		TSM_ASSERT_EQUALS("Message encoded incorrectly", message.str(), msg );
    }

};


#endif /* COUNTERTEST_H_ */
