/*
 * test_adiochannel.hpp
 *
 *  Created on: 5. okt. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef TEST_ADIOCHANNEL_HPP_
#define TEST_ADIOCHANNEL_HPP_

#include "ndsChannel.h"

class ChannelUnitTest: public nds::Channel
{
	friend class ADIOChannelUnitTestSuite;
public:
	ChannelUnitTest() : nds::Channel()
	{}
};

class ADIOChannelUnitTestSuite : public CxxTest::TestSuite
{
  public:
	/* TEST CASES */
	void testCreateADIOChannel()
	{
		ChannelUnitTest channel;
	}

	void testHandlerRegistration()
	{
		nds::PVContainers containers;
		ndsStatus result;
		reason_type reason;
		int portAddress=1;

		ChannelUnitTest channel;
  	    channel._portAddr = portAddress;

  	    result = channel.registerHandlers( &containers );
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		result = containers.getReason(portAddress, "Command", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		result = containers.getReason(portAddress, "State", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);


		result = containers.getReason(portAddress, "ValueInt32", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		/// Interrupt ID to dispatch DecimationFactor event
		result = containers.getReason(portAddress, "DecimationFactor", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		/// Interrupt ID to dispatch DecimationOffset event
		result = containers.getReason(portAddress, "DecimationOffset", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		/// Interrupt ID to dispatch SignalType event
		result = containers.getReason(portAddress, "SignalType", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		/// Interrupt ID to dispatch FFTWindow event
		result = containers.getReason(portAddress, "FFTWindow", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		/// Interrupt ID to dispatch ValueFloat64 event
		result = containers.getReason(portAddress, "ValueFloat64", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		/// Interrupt ID to dispatch SignalFrequency event
		result = containers.getReason(portAddress, "SignalFrequency", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		/// Interrupt ID to dispatch SignalAmplitude event
		result = containers.getReason(portAddress, "SignalAmplitude", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		/// Interrupt ID to dispatch SignalOffset event
		result = containers.getReason(portAddress, "SignalOffset", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		/// Interrupt ID to dispatch SignalPhase event
		result = containers.getReason(portAddress, "SignalPhase", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		/// Interrupt ID to dispatch SignalDutyCycle event
		result = containers.getReason(portAddress, "SignalDutyCycle", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		/// Interrupt ID to dispatch FFTSize event
		result = containers.getReason(portAddress, "FFTSize", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		/// Interrupt ID to dispatch FFTOverlap event
		result = containers.getReason(portAddress, "FFTOverlap", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		/// Interrupt ID to dispatch FFTSmoothing event
		result = containers.getReason(portAddress, "FFTSmoothing", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		/// Interrupt ID to dispatch BufferInt32 event
		result = containers.getReason(portAddress, "BufferInt32", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		/// Interrupt ID to dispatch BufferInt16 event
		result = containers.getReason(portAddress, "BufferInt16", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		/// Interrupt ID to dispatch BufferInt8 event
		result = containers.getReason(portAddress, "BufferInt8", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		/// Interrupt ID to dispatch BufferFloat32 event
		result = containers.getReason(portAddress, "BufferFloat32", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		/// Interrupt ID to dispatch BufferFloat64 event
		result = containers.getReason(portAddress, "BufferFloat64", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		/// Interrupt ID to dispatch Conversion event
		result = containers.getReason(portAddress, "Conversion", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		/// Interrupt ID to dispatch Filter event
		result = containers.getReason(portAddress, "Filter", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		/// Interrupt ID to dispatch FFTBuffer event
		result = containers.getReason(portAddress, "FFTBuffer", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
	}



};



#endif /* TEST_ADIOCHANNEL_HPP_ */
