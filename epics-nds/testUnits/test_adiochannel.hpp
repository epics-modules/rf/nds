/*
 * test_adiochannel.hpp
 *
 *  Created on: 5. okt. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef TEST_ADIOCHANNEL_HPP_
#define TEST_ADIOCHANNEL_HPP_

#include "ndsADIOChannel.h"

class ADIOChannelUnitTest: public nds::ADIOChannel
{
	friend class ADIOChannelUnitTestSuite;
public:
	ADIOChannelUnitTest() : nds::ADIOChannel()
	{}

	virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers)
	{
		ADIOChannel::registerHandlers(pvContainers);
		NDS_TRC("ADIOChannelUnitTest::registerHandlers");

		return ndsSuccess;
	}
};

class ADIOChannelUnitTestSuite : public CxxTest::TestSuite
{
  public:
	/* TEST CASES */
	void testCreateADIOChannel()
	{
		ADIOChannelUnitTest channel;
	}

	void testHandlerRegistration()
	{
		nds::PVContainers containers;
		ndsStatus result;
		reason_type reason;
		int portAddress=1;

		ADIOChannelUnitTest channel;
  	    channel._portAddr = portAddress;

  	    result = channel.registerHandlers( &containers );
		TSM_ASSERT_EQUALS("", result, ndsSuccess);


		result = containers.getReason(portAddress, "Command", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		result = containers.getReason(portAddress, "State", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);


		result = containers.getReason(portAddress, "Spline", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		result = containers.getReason(portAddress, "DIODirection", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		result = containers.getReason(portAddress, "Gain", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		result = containers.getReason(portAddress, "Coupling", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		result = containers.getReason(portAddress, "Differential", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		result = containers.getReason(portAddress, "Ground", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		result = containers.getReason(portAddress, "Offset", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		result = containers.getReason(portAddress, "MeasuredOffset", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		result = containers.getReason(portAddress, "Bandwidth", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		result = containers.getReason(portAddress, "Impedance", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
	}

};



#endif /* TEST_ADIOCHANNEL_HPP_ */
