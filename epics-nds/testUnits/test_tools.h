/*
 * test_tools.h
 *
 *  Created on: 04.03.2013
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef TEST_TOOLS_H_
#define TEST_TOOLS_H_


#define CHECK_REASON_REGISTRATION(reason_name) \
		result = containers.getReason(portAddress, reason_name, &reason);\
		TSM_ASSERT_EQUALS("Reason is not found: "#reason_name, result, ndsSuccess);

#define NDS_TEST_CHECK_REASON_REGISTRATION(type, function) \
		CHECK_REASON_REGISTRATION(function)

#define NDS_TEST_CHECK_REASON_REGISTRATION_OCTET(type, function) \
		CHECK_REASON_REGISTRATION(function)

#define NDS_TEST_CHECK_REASON_REGISTRATION_ARRAY(type, function) \
		CHECK_REASON_REGISTRATION(function)

#define NDS_TEST_CHECK_REASON_VALUE(type, function) \
		epics##type val##function, control##function;\
		val##function=3.33; \
		obj.set##function(0, val##function);\
		obj.get##function(0, &control##function);\
		TSM_ASSERT_EQUALS("Got value doesn't match set for "#function, val##function, control##function);

#define NDS_TEST_CHECK_REASON_VALUE_OCTET(type, function) ;

#define NDS_TEST_CHECK_REASON_VALUE_ARRAY(type, function) ;



#endif /* TEST_TOOLS_H_ */
