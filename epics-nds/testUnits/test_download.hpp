/*
 * test_download.hpp
 *
 *  Created on: 11. mar. 2013
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef TEST_DOWNLOAD_HPP_
#define TEST_DOWNLOAD_HPP_

#include "ndsFirmware.h"

class FirmwareUnitTestSuite : public CxxTest::TestSuite
{
  public:
	/* TEST CASES */
	void testHttp()
	{
		nds::Firmware firmware;
		TSM_ASSERT_EQUALS("Can't load xml file.", firmware.loadFile("http.xml"), ndsSuccess );

		firmware.downloadImage();
	}

	void testFtp()
	{

	}

	void testSamba()
	{

	}

	void testNFS()
	{

	}

	void testLocal()
	{

	}

};

#endif /* TEST_DOWNLOAD_HPP_ */
