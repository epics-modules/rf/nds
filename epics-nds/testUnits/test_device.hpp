/*
 * test_handlers.hpp
 *
 *  Created on: 30. avg. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef TEST_HANDLERS_HPP_
#define TEST_HANDLERS_HPP_

#include <cstdio>
#include <cstring>
#include <iostream>

#include "ndsPV.h"
#include "ndsPVContainer.h"
#include "ndsDevice.h"
#include "ndsDebug.h"

using namespace std;

#define NDS_LOG_OUT_TO(LEVEL, args...) TS_TRACE(args)

#define OCTET_TEST_VAL "ValueTestOctet"

class TestDevice : public nds::Device
{
public:

	virtual ndsStatus updateFirmware(const std::string& module, const std::string& image, const std::string& method)
	{
		return ndsSuccess;
	}
};


class DeviceTestSuite : public CxxTest::TestSuite
{
  public:

	/* TEST CASES */
	void testFirmwareUplaod()
	{

    }
};


#endif /* TEST_HANDLERS_HPP_ */
