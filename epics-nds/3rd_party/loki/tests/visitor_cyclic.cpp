/** sample test for the Loki library 
 *
 * @author marco corvi <marcounderscorecorviatgeocitiesdotcom>
 * @date dec 2004
 *
 * This code is part of a set of sample test programs for the Loki library.
 *
 * The Loki library is copyright(c) 2001 by Andrei Alexandrescu.
 * It is described in the book:
 *     Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and
 *     Design Patterns Applied". Copyright (c) 2001. Addison-Wesley.
 * and is available from 
 *     http://sourceforge.net/projects/loki-lib/
 *
 * Permission to use, copy, modify, distribute and sell this software 
 *     (ie, the test programs) for any purpose is hereby granted without fee.
 * Permission to use, copy, modify, distribute and sell the software
 *     changes made to the Loki library is hereby granted without fee
 *     for any purposes provided the original Loki library copyright notice
 *     appears in all copies and that both that notice and this 
 *     permission notice appear in supporting documentation.
 * The author makes no representations about the suitability of this 
 *     software for any purpose. It is provided "as is" without express 
 *     or implied warranty.
 *
 * =============================================================
 * Sample cyclic visitor pattern (see pg. ...)
 * 
 */

#include <iostream>

#include "Visitor.h"


// [1] Forward declaration of visitable classes
//
class Derived1;
class Derived2;
class Derived3;

typedef Loki::CyclicVisitor
<
	void, 
	TYPELIST_3( Derived1, Derived2, Derived3 )
> Visitor1;

typedef Loki::CyclicVisitor
<
	void, 
	TYPELIST_2( Derived1, Derived2 )
> Visitor2;


// ======================================================

class Base 
{
	public:
};

// ======================================================
// [3] Declare each member of the hierarchy that is visitable as such
//     Use the macro DEFINE_CYCLIC_VISITABLE
//

class Derived1 : public Base
{
	public:
		DEFINE_CYCLIC_VISITABLE( Visitor1 );
		DEFINE_CYCLIC_VISITABLE( Visitor2 );
};

class Derived2 : public Derived1
{
	public:
		// A class may not declare that it is visitable
		// DEFINE_CYCLIC_VISITABLE( Visitor1 );
		DEFINE_CYCLIC_VISITABLE( Visitor2 );
};

class Derived3 : public Derived2
{
	public:
		DEFINE_CYCLIC_VISITABLE( Visitor1 );
		// cannot declare visitable by Visitor2
};

// ======================================================
// [4] The concrete Visitor must implement Visit() for all the visitable
//     classes in the typelist, even if some do not declare that are
//     visitable (like Derived2).

class TheVisitor1 : public Visitor1
{
	public:
		void Visit(Derived1 &) { std::cout << "1: visits Derived1 \n"; }
		void Visit(Derived2 &) { std::cout << "1: visits Derived2 \n"; }
		void Visit(Derived3 &) { std::cout << "1: visits Derived3 \n"; }
};

class TheVisitor2 : public Visitor2
{
	public:
		void Visit(Derived1 &) { std::cout << "2: visits Derived1 \n"; }
		void Visit(Derived2 &) { std::cout << "2: visits Derived2 \n"; }
};

// ======================================================

int main()
{
	TheVisitor1 v1;
	TheVisitor2 v2;

	Derived1 d1;
	Derived2 d2;
	Derived3 d3;

	d1.Accept( v1 );
	// d2 cannot Accept( v1 ) because has not declared so
	d3.Accept( v1 );
	d1.Accept( v2 );
	d2.Accept( v2 ); 
}
