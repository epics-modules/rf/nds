/** sample test for the Loki library 
 *
 * @author marco corvi <marcounderscorecorviatgeocitiesdotcom>
 * @date dec 2004
 *
 * This code is part of a set of sample test programs for the Loki library.
 *
 * The Loki library is copyright(c) 2001 by Andrei Alexandrescu.
 * It is described in the book:
 *     Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and
 *     Design Patterns Applied". Copyright (c) 2001. Addison-Wesley.
 * and is available from 
 *     http://sourceforge.net/projects/loki-lib/
 *
 * Permission to use, copy, modify, distribute and sell this software 
 *     (ie, the test programs) for any purpose is hereby granted without fee.
 * Permission to use, copy, modify, distribute and sell the software
 *     changes made to the Loki library is hereby granted without fee
 *     for any purposes provided the original Loki library copyright notice
 *     appears in all copies and that both that notice and this 
 *     permission notice appear in supporting documentation.
 * The author makes no representations about the suitability of this 
 *     software for any purpose. It is provided "as is" without express 
 *     or implied warranty.
 *
 * ==============================================================
 * Sample program for GenLinearHierarchy (see pg. 71)
 *
 */
#include <iostream>
#include <stdio.h>

#include "TypeManip.h"
#include "HierarchyGenerators.h"

class Base
{
	public:
		virtual ~Base() {};
		virtual void print() { printf("Base \n"); }
		virtual bool handle( int id ) = 0;
};

class C0 : public Base
{
	public:
		C0() { printf("C0 cstr. %p\n", (void*)this); }
		void print() { printf("print C0 %p\n", (void*)this); }
		bool handle( int id ) { 
			std::cout << "C0 handle " << id << " ";
			return id == 0;
		}
};

class C1 : public Base
{
	public:
		C1() { printf("C1 cstr. %p\n", (void*)this); }
		void print() { printf("print C1 %p\n", (void*)this); }
		bool handle( int id ) { 
			std::cout << "C1 handle " << id << " ";
			return id == 1;
		}
};

class C2 : public Base
{
	public:
		C2() { printf("C2 cstr. %p\n", (void*)this); }
		void print() { printf("print C2 %p\n", (void*)this); }
		bool handle( int id ) { 
			std::cout << "C2 handle " << id << " ";
			return id == 2;
		}
};


// the constructor of the Model invokes the constructor of the 
// tamplate class T, when the hierarchy is built
//

template< typename T, typename R >
class Model : public R
{
	private:
		T value_;
	public:
		T * Get()  { return &value_; }
		void Set(T t) { value_ = t; }
	public:
		bool handle( int id )
		{
			if ( value_.handle(id) ) return true; 
			std::cout << "trying base ... \n";
			return R::handle( id );
		}
};

template< typename T >
class Model< T, Loki::EmptyType > 
{
	private:
		T value_;
	public:
		T * Get()  { return &value_; }
		void Set(T t) { value_ = t; }
	public:
                bool handle( int id )
                {
                        if ( value_.handle(id) ) return true;
                        std::cout << "trying base ... \n";
                        return false;
                }
};


// The root of the linear hierarchy is Loki::EmptyType
//
typedef Loki::GenLinearHierarchy < TYPELIST_3(C0,C1,C2), Model > GLH;



int main()
{
  
  GLH glh;

  // same as Loki::Field<C0, GLH>( glh ).Get();
  Base * pc0 = Loki::Field<C0>( glh ).Get();
  pc0->print();
  // C1 * pc1 = Loki::Field<1>( glh ).Get();
  Base * pc1 = Loki::Field<C1>( glh ).Get();
  pc1->print();
  Base * pc2 = Loki::Field<C2>( glh ).Get();
  pc2->print();


  
  for (int k=0; k<3; k++) 
    std::cout << "handle " << k << " " 
	      << (glh.handle( k )? "true" : "false") << "\n";
 


#if 0
  // this gives a warning by the compiler
  // base class Loki::GenScatterHierarchy<int, Model> inacessible 
  // due to ambiguity
  //
  typedef Loki::GenScatterHierarchy< TYPELIST_2(int,int), Model > GSI;
  GSI gsi;

  // this is an error
  // int * i = Loki::Field<int>( gsi ).Get();
  int * i = Loki::Field<0>(gsi).Get();
  //
  printf("int %p\n", (void*)i);
#endif


}
