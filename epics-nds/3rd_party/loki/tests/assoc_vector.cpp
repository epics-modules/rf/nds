/** sample test for the Loki library 
 *
 * @author marco corvi <marcounderscorecorviatgeocitiesdotcom>
 * @date dec 2004
 *
 * This code is part of a set of sample test programs for the Loki library.
 *
 * The Loki library is copyright(c) 2001 by Andrei Alexandrescu.
 * It is described in the book:
 *     Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and
 *     Design Patterns Applied". Copyright (c) 2001. Addison-Wesley.
 * and is available from 
 *     http://sourceforge.net/projects/loki-lib/
 *
 * Permission to use, copy, modify, distribute and sell this software 
 *     (ie, the test programs) for any purpose is hereby granted without fee.
 * Permission to use, copy, modify, distribute and sell the software
 *     changes made to the Loki library is hereby granted without fee
 *     for any purposes provided the original Loki library copyright notice
 *     appears in all copies and that both that notice and this 
 *     permission notice appear in supporting documentation.
 * The author makes no representations about the suitability of this 
 *     software for any purpose. It is provided "as is" without express 
 *     or implied warranty.
 *
 * ===================================================================
 * AssocVector is cited in the book pg. 210, 277-278
 *
 * This sample program show the usage of AssocVector to store
 * key-value pairs.
 */

#include <iostream>
#include <string>

#include "AssocVector.h"

const char * keys[] = { "one", "two", "three", "four", "five" };
const char * vals[] = { "ein", "zwei", "drei", "fir", "funf" };

int main()
{
	typedef Loki::AssocVector< std::string, std::string > AVHash;

	AVHash hash;

	std::cout << "initial size " << hash.size() << "\n";

	for (int i=0; i<5; i++) {
		std::cout << "insert " << i << " " <<
		  hash.insert( std::make_pair(keys[i], vals[i]) ).second
		  << "\n";
	}
	std::cout << "after insert size " << hash.size() << "\n";

	// try to insert a pair twice:
	// the pair is not inserted and the method return false
        const char * zero = "zero";
	std::cout << "insert again " << 0 << " " <<
	  hash.insert( std::make_pair(keys[0], zero) ).second
	  << "\n";
	std::cout << "after insert size " << hash.size() << "\n";

        AVHash::iterator it = hash.find( keys[3] );
        if ( it != hash.end() ) {
           const char * new_value = "new_value";
	   hash.erase( it );
           hash.insert( std::make_pair( keys[3], new_value) );
	} else {
	  std::cout << "key " << keys[3] << "not found\n";
	}

	for (it = hash.begin(); it != hash.end(); it ++) {
		std::cout << it->first << " --> " << it->second << "\n";
	}
}
