/** sample test for the Loki library 
 *
 * @author marco corvi <marcounderscorecorviatgeocitiesdotcom>
 * @date dec 2004
 *
 * This code is part of a set of sample test programs for the Loki library.
 *
 * The Loki library is copyright(c) 2001 by Andrei Alexandrescu.
 * It is described in the book:
 *     Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and
 *     Design Patterns Applied". Copyright (c) 2001. Addison-Wesley.
 * and is available from 
 *     http://sourceforge.net/projects/loki-lib/
 *
 * Permission to use, copy, modify, distribute and sell this software 
 *     (ie, the test programs) for any purpose is hereby granted without fee.
 * Permission to use, copy, modify, distribute and sell the software
 *     changes made to the Loki library is hereby granted without fee
 *     for any purposes provided the original Loki library copyright notice
 *     appears in all copies and that both that notice and this 
 *     permission notice appear in supporting documentation.
 * The author makes no representations about the suitability of this 
 *     software for any purpose. It is provided "as is" without express 
 *     or implied warranty.
 *
 * ==================================================================
 * Example of CloneFactory (pg. 211 and followings)
 *
 */
#include <iostream>
#include <string>

#include "Factory.h"

class Base
{
	public:
		virtual ~Base() {}
		virtual Base * Clone( ) const = 0;
		virtual void print() { std::cout << "Base print \n"; }
};

Base * Creator( const Base * b )
{
	return b->Clone();
}


class Derived1 : public Base
{
	public:
		Derived1() { std::cout << "cstr. Derived1 \n"; }
		~Derived1() { std::cout << "dstr. Derived1 \n"; }

		Derived1( const Derived1 & d )
		{
			std::cout << "copy cstr. Derived1 \n"; 
		}
		
		virtual Derived1 * Clone() const 
		{
			return new Derived1( *this );
		}

		void print() { std::cout << "Derived1 print\n"; }
};

class Derived2 : public Base
{
	public:
		Derived2() { std::cout << "cstr. Derived2 \n"; }
		~Derived2() { std::cout << "dstr. Derived2 \n"; }

		Derived2( const Derived2 & d )
		{
			std::cout << "copy cstr. Derived2 \n"; 
		}
	
		virtual Derived2 * Clone() const 
		{
			return new Derived2( *this );
		}
};


int main()
{
	Loki::CloneFactory< Base > factory;

	Derived1 d1;
	Derived2 d2;

	std::cout << "Ready to clone \n";

	std::cout << factory.Register( typeid(Derived1), Creator ) << "\n";
	std::cout << factory.Register( typeid(Derived2), Creator ) << "\n";

	// if i register again "base" i fail, even if the function is the same

	Base * b1 = factory.CreateObject( &d1 );
	Base * b2 = factory.CreateObject( &d2 );

	b1->print();
	b2->print();

	// delete b1;
	// delete b2;
}
