/** sample test for the Loki library 
 *
 * @author marco corvi <marcounderscorecorviatgeocitiesdotcom>
 * @date dec 2004
 *
 * This code is part of a set of sample test programs for the Loki library.
 *
 * The Loki library is copyright(c) 2001 by Andrei Alexandrescu.
 * It is described in the book:
 *     Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and
 *     Design Patterns Applied". Copyright (c) 2001. Addison-Wesley.
 * and is available from 
 *     http://sourceforge.net/projects/loki-lib/
 *
 * Permission to use, copy, modify, distribute and sell this software 
 *     (ie, the test programs) for any purpose is hereby granted without fee.
 * Permission to use, copy, modify, distribute and sell the software
 *     changes made to the Loki library is hereby granted without fee
 *     for any purposes provided the original Loki library copyright notice
 *     appears in all copies and that both that notice and this 
 *     permission notice appear in supporting documentation.
 * The author makes no representations about the suitability of this 
 *     software for any purpose. It is provided "as is" without express 
 *     or implied warranty.
 *
 * ========================================================
 * Sample program that shows the outcome of SUPESUBCLASS
 * (pg. ...)
 */
#include <iostream>

#include "TypeManip.h"

#include "base_derived.h"

int main()
{
	std::cout << "Base <-- Derived: " 
		  << SUPERSUBCLASS( Base, Derived) << "\n";
	std::cout << "Derived <-- Base: "
		  << SUPERSUBCLASS( Derived, Base) << "\n";
	std::cout << "Base <-- Base:    "
		  << SUPERSUBCLASS( Base, Base) << "\n";
}
