#include <iostream>

class Soldier
{
	public:
		virtual ~Soldier() {}
		virtual void fire() = 0;
		virtual Soldier * Clone() = 0;
};

class Monster
{
	public:
		virtual ~Monster() {}
		virtual void grin() = 0;
		virtual Monster * Clone() = 0;
};

class BadMonster
{
	public:
		virtual ~BadMonster() {}
		virtual void kill() = 0;
		virtual BadMonster * Clone() = 0;
};

// ============================================================
// Easy level classes

class SillySoldier : public Soldier
{
	public:
		~SillySoldier() { std::cout << "SillySoldier dies\n"; }
		void fire() { std::cout << "SillySoldier fires\n"; }
		SillySoldier * Clone() { return new SillySoldier; }
};

class SillyMonster : public Monster
{
	public:
		~SillyMonster() { std::cout << "SillyMonster dies\n"; }
		void grin() { std::cout << "SillyMonster grins\n"; }
		SillyMonster * Clone() { return new SillyMonster; }
};

class SillyBadMonster : public BadMonster
{
	public:
		~SillyBadMonster() { std::cout << "SillyBadMonster dies\n"; }
		void kill() { std::cout << "SillyBadMonster kills\n"; }
		SillyBadMonster * Clone() { return new SillyBadMonster; }
};



// ============================================================
// Hard level classes

class HardSoldier : public Soldier
{
	public:
		~HardSoldier() { std::cout << "HardSoldier dies\n"; }
		void fire() { std::cout << "HardSoldier fires\n"; }
		HardSoldier * Clone() { return new HardSoldier; }
};

class HardMonster : public Monster
{
	public:
		~HardMonster() { std::cout << "HardMonster dies\n"; }
		void grin() { std::cout << "HardMonster grins\n"; }
		HardMonster * Clone() { return new HardMonster; }
};

class HardBadMonster : public BadMonster
{
	public:
		~HardBadMonster() { std::cout << "HardBadMonster dies\n"; }
		void kill() { std::cout << "HardBadMonster kills\n"; }
		HardBadMonster * Clone() { return new HardBadMonster; }
};

// -------------------------------------------------------------------

