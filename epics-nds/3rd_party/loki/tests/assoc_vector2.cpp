/** sample test for the Loki library 
 *
 * @author marco corvi <marcounderscorecorviatgeocitiesdotcom>
 * @date dec 2004
 *
 * This code is part of a set of sample test programs for the Loki library.
 *
 * The Loki library is copyright(c) 2001 by Andrei Alexandrescu.
 * It is described in the book:
 *     Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and
 *     Design Patterns Applied". Copyright (c) 2001. Addison-Wesley.
 * and is available from 
 *     http://sourceforge.net/projects/loki-lib/
 *
 * Permission to use, copy, modify, distribute and sell this software 
 *     (ie, the test programs) for any purpose is hereby granted without fee.
 * Permission to use, copy, modify, distribute and sell the software
 *     changes made to the Loki library is hereby granted without fee
 *     for any purposes provided the original Loki library copyright notice
 *     appears in all copies and that both that notice and this 
 *     permission notice appear in supporting documentation.
 * The author makes no representations about the suitability of this 
 *     software for any purpose. It is provided "as is" without express 
 *     or implied warranty.
 *
 * ===================================================================
 * AssocVector is cited in the book pg. 210, 277-278
 *
 * This sample program show how to initialize (and use) an AssocVector
 * of functors (see also pg. 127).
 */

#include <iostream>

#include "AssocVector.h"
#include "Functor.h"

// ===========================================================
// custom items (see the book pg. 127)

void fct( int i ) { std::cout << "fct() " << i << "\n"; }

struct Fun 
{
	void operator()( int i ) { std::cout << "Fun() " << i << "\n"; }
	void fct( int i ) { std::cout << "Fun::fct() " << i << "\n"; }
};




// ===========================================================

typedef Loki::Functor< void, TYPELIST_1(int)> MyFunctor;


int main()
{

	Fun fun;
	const char * keys[] = { "one", "two", "three", "four" };

	MyFunctor cmd[4];
	cmd[0] = MyFunctor( fct );
	cmd[1] = MyFunctor( fun );
  	cmd[2] = MyFunctor( &fun, &Fun::fct );
  	cmd[3] = MyFunctor( cmd[1] );

	typedef Loki::AssocVector< std::string, MyFunctor > AVHash;

	AVHash hash;

	std::cout << "initial size " << hash.size() << "\n";

	for (int i=0; i<4; i++) {
	  std::cout << "insert " << i << " "
	  	    << hash.insert( std::make_pair(keys[i], cmd[i]) ).second
		    << "\n";
	}
	std::cout << "after insert size " << hash.size() << "\n";

	int k = 10;
	AVHash::iterator it;
	for (it = hash.begin(); it != hash.end(); it ++, k++) {
		std::cout << it->first << ": ";
	       	(it->second)(k);
	}
}
