/** sample test for the Loki library 
 *
 * @author marco corvi <marcounderscorecorviatgeocitiesdotcom>
 * @date dec 2004
 *
 * This code is part of a set of sample test programs for the Loki library.
 *
 * The Loki library is copyright(c) 2001 by Andrei Alexandrescu.
 * It is described in the book:
 *     Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and
 *     Design Patterns Applied". Copyright (c) 2001. Addison-Wesley.
 * and is available from 
 *     http://sourceforge.net/projects/loki-lib/
 *
 * Permission to use, copy, modify, distribute and sell this software 
 *     (ie, the test programs) for any purpose is hereby granted without fee.
 * Permission to use, copy, modify, distribute and sell the software
 *     changes made to the Loki library is hereby granted without fee
 *     for any purposes provided the original Loki library copyright notice
 *     appears in all copies and that both that notice and this 
 *     permission notice appear in supporting documentation.
 * The author makes no representations about the suitability of this 
 *     software for any purpose. It is provided "as is" without express 
 *     or implied warranty.
 *
 * ============================================================
 * AbstractFactory is described in chapter 9.
 *
 * Note auto_ptr is not used to show where the memory is allocated
 * (understanding the memory management is mandatory).
 */

#include <iostream>

// import the game abstract hierarchy and the two concrete hierarchies.
//   - Soldier fire()
//   - Monster grin()
//   - BadMonster kill()
// 
#include "game.h"

#include "AbstractFactory.h"

// Define my abstract factory type
//
typedef Loki::AbstractFactory 
<
  TYPELIST_3( Soldier, Monster, BadMonster )
> AbstractEnemyFactory;

// ======================================================
// Define my concrete factories, for the silly objects and for the 
// hard ones.

typedef Loki::ConcreteFactory <
  AbstractEnemyFactory,
  Loki::OpNewFactoryUnit,
  TYPELIST_3( SillySoldier, SillyMonster, SillyBadMonster )
> SillyEnemyFactory;

typedef Loki::ConcreteFactory <
  AbstractEnemyFactory,
  Loki::OpNewFactoryUnit,
  TYPELIST_3( HardSoldier, HardMonster, HardBadMonster )
> HardEnemyFactory;

// Define the prototype factory, which uses the Loki::PrototypeFactoryUnit.
// This unit implements DoCreate() by invoking the prototype' Clone() method
// Therefore the prototypes must implement it (see the file game.h)
//
typedef Loki::ConcreteFactory <
  AbstractEnemyFactory,
  Loki::PrototypeFactoryUnit
> PrototypeEnemyFactory;

// ---------------------------------------------------------
// sample program
//
int main()
{
	AbstractEnemyFactory * ps = new SillyEnemyFactory;
	AbstractEnemyFactory * ph = new HardEnemyFactory;

	Monster * sm = ps->Create<Monster>();
	sm->grin();

	Soldier * ss = ps->Create<Soldier>();
	ss->fire();

	Monster * hm = ph->Create<Monster>();
	hm->grin();

	std::cout << "Test prototypr factory\n";
	PrototypeEnemyFactory * pp = new PrototypeEnemyFactory;

	pp->SetPrototype( hm );
	pp->SetPrototype( ss );

	Monster * mp = pp->Create<Monster>();
	Soldier * sp = pp->Create<Soldier>();
	mp->grin();
	sp->fire();

	delete mp;
	delete sp;

	std::cout << "... get prototypes \n";
	pp->GetPrototype<Monster>( mp );
	pp->GetPrototype<Soldier>( sp );
	mp->grin();
        sp->fire();

	delete ss;
	delete sm;
	delete hm;

}



