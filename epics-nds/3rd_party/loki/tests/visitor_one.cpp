/** sample test for the Loki library 
 *
 * @author marco corvi <marcounderscorecorviatgeocitiesdotcom>
 * @date dec 2004
 *
 * This code is part of a set of sample test programs for the Loki library.
 *
 * The Loki library is copyright(c) 2001 by Andrei Alexandrescu.
 * It is described in the book:
 *     Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and
 *     Design Patterns Applied". Copyright (c) 2001. Addison-Wesley.
 * and is available from 
 *     http://sourceforge.net/projects/loki-lib/
 *
 * Permission to use, copy, modify, distribute and sell this software 
 *     (ie, the test programs) for any purpose is hereby granted without fee.
 * Permission to use, copy, modify, distribute and sell the software
 *     changes made to the Loki library is hereby granted without fee
 *     for any purposes provided the original Loki library copyright notice
 *     appears in all copies and that both that notice and this 
 *     permission notice appear in supporting documentation.
 * The author makes no representations about the suitability of this 
 *     software for any purpose. It is provided "as is" without express 
 *     or implied warranty.
 *
 * ===============================================================
 * Sample Visitor pattern with a single catch-all method
 * (see pg. ...)
 */
#include <iostream>
#include <typeinfo>

// Visitor with the catch-all method

// [1] must forward declare Visitor
//
class Visitor;

// [2] The Base class of the hierarchy declares a pure virtual Accept()
//     with a Visitor parameter
//     
class Base
{
	public:
		virtual void Accept( Visitor * ) = 0;
};

// [3] The Visitor declares a pure virtual Visit() with the hierarchy Base
//     class as parameter
//
class Visitor
{
	public:
		virtual void Visit( Base * ) = 0;
};

// ======================================================
// [4] Derived classes implements Accept()

class Derived1 : public Base
{
	public:
		void Accept( Visitor * v ) 
		{ 
			std::cout << "Derived1 accepts visitor " 
				  << typeid( *v ).name() << "\n";
			v->Visit( this ); 
		}
};

class Derived2 : public Derived1
{
	public:
		void Accept( Visitor * v ) 
		{ 
			std::cout << "Derived2 accepts visitor " 
				  << typeid( *v ).name() << "\n";
			v->Visit( this ); 
		}
};


// ======================================================
// [5] Concrete Visistors implements list-switch (using if's) based
//     on the dynamic type of the parameter
//     Warning: the order of the if's must respect the hierarchy
//     if Derived1 is checked before Derived2 there is a wrong dispatch
//     at runtime.

class Visitor1: public Visitor
{
	public:
		void Visit( Base * b ) 
		{
			if (Derived2* d2 = dynamic_cast<Derived2*>(b)) {
				std::cout << "Visitor1 visits Derived2 \n";
			} else if (Derived1* d1 = dynamic_cast<Derived1*>(b) ) {
				std::cout << "Visitor1 visits Derived1 \n";
			}
		}
};

class Visitor2: public Visitor
{
	public:
		void Visit( Base * b ) 
		{
			if (Derived2* d2 = dynamic_cast<Derived2*>(b)) {
				std::cout << "Visitor2 visits Derived2 \n";
			} else if (Derived1* d1 = dynamic_cast<Derived1*>(b) ) {
				std::cout << "Visitor2 visits Derived1 \n";
			}
		}
};

// ==========================================================

int main()
{
	Visitor1 v1;
	Visitor2 v2;

	Derived1 d1;
	Derived2 d2;

	d1.Accept( &v1 );
	d1.Accept( &v2 );
	d2.Accept( &v1 );
}
