/** sample test for the Loki library 
 *
 * @author marco corvi <marcounderscorecorviatgeocitiesdotcom>
 * @date dec 2004
 *
 * This code is part of a set of sample test programs for the Loki library.
 *
 * The Loki library is copyright(c) 2001 by Andrei Alexandrescu.
 * It is described in the book:
 *     Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and
 *     Design Patterns Applied". Copyright (c) 2001. Addison-Wesley.
 * and is available from 
 *     http://sourceforge.net/projects/loki-lib/
 *
 * Permission to use, copy, modify, distribute and sell this software 
 *     (ie, the test programs) for any purpose is hereby granted without fee.
 * Permission to use, copy, modify, distribute and sell the software
 *     changes made to the Loki library is hereby granted without fee
 *     for any purposes provided the original Loki library copyright notice
 *     appears in all copies and that both that notice and this 
 *     permission notice appear in supporting documentation.
 * The author makes no representations about the suitability of this 
 *     software for any purpose. It is provided "as is" without express 
 *     or implied warranty.
 *
 * =============================================================
 * Cyclic visitor pattern.
 * This is essentially the example page 253 of the book.
 * The Accept method is explicitly impemented for the visitable classes.
 */

#include <iostream>
#include <typeinfo>

#include "Visitor.h"

// [1] Declare the root oh the hierarchy as visitable, by inheriting
//     from BaseVisitable< ReturnType >
//     The return type of Visit is void by default
//
class Base : public Loki::BaseVisitable< >
{
};

// ======================================================
// [2] Declare each member of the hierarchy that is visitable as such
//     It implements Accept(BaseVisitor&)
//     Note. It would be nice to factor out the if-dispatching to the
//     Base class of the hierarchy. However the template type of the
//     Visitor must be resolved statically, and cannot be done with
//     this simple design.
//

class Derived1 : public Base
{
	public:
		typedef Derived1 MyType;

		ReturnType Accept( Loki::BaseVisitor & v ) 
		{ 
			std::cout << "Derived1 accepts visitor " 
				  << typeid( v ).name() << "\n";
			if ( Loki::Visitor<MyType> * p = 
				dynamic_cast<Loki::Visitor<MyType> * >( &v ) )
			{
				return p->Visit( *this ); 
			} else {
				std::cout << "Not a Derived1 visitor\n";
				return ReturnType();
			}
		}
};

class Derived2 : public Derived1
{
	public:
		typedef Derived2 MyType;

		ReturnType Accept( Loki::BaseVisitor & v ) 
		{ 
			std::cout << "Derived2 accepts visitor " 
				  << typeid( v ).name() << "\n";
			if ( Loki::Visitor<MyType> * p = 
				dynamic_cast<Loki::Visitor<MyType> * >( &v ) )
			{
				return p->Visit( *this ); 
			} else {
				std::cout << "Not a Derived2 visitor\n";
				return ReturnType();
			}
		}
};

// ======================================================

class MyVisitor :
	public Loki::BaseVisitor,
	public Loki::Visitor< Derived1 >,
	public Loki::Visitor< Derived2 >
{
	public:
		void Visit(Derived1 &) { std::cout << "visiting Derived1 \n"; }
		void Visit(Derived2 &) { std::cout << "visiting Derived2 \n"; }
};

// ======================================================

int main()
{
	MyVisitor v1;

	Derived1 d1;
	Derived2 d2;

	d1.Accept( v1 );
	d2.Accept( v1 );
}
