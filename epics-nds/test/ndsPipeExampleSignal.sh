#!/bin/bash

# define initial values of signals and their increments
signal0=0
signal1=0
a=0.1
b=1

while [ 1 ]
do
	sleep 0.1

	#calculate the new value of signal0 
	signal0=`echo "scale=2;$signal0+$a" | bc -l`

	#Echoes signal0 to the pipe
	echo Signal on AI0: $signal0
	echo $signal0 >> /tmp/q.ai.0

	#When signal0 reaches a limit, negate the increment
	if [ 1 -eq `echo "${signal0} == 10" | bc` ]
	then  
    	a=-0.1
    elif [ 1 -eq `echo "${signal0} == 0" | bc` ]
    then
		a=0.1
	fi    

	#calculate the new value of signal1 
	signal1=`echo "scale=2;$signal1+$b" | bc -l`

	#Echoes signal to the pipe
	echo Signal on AI1: $signal1
	echo $signal1 >> /tmp/q.ai.1

	#When signal reaches a limit, negate the increment
	if [ 1 -eq `echo "${signal1} == 100" | bc` ]
	then  
    	b=-1
    elif [ 1 -eq `echo "${signal1} == -100" | bc` ]
    then
		b=1
	fi    

done
