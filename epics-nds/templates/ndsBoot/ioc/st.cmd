#!../../bin/_ARCH_/_APPNAME_

## You may have to change _APPNAME_ to something else
## everywhere it appears in this file

< envPaths

cd ${TOP}

epicsEnvSet("DIV", "-")
epicsEnvSet("PREFIX", "_APPNAME_")
epicsEnvSet("PORT",   "PIPE")

## areaDetector path definition
#epicsEnvSet("AREA_DETECTOR", $(EPICS_MODULES)/areaDetector-1-8)

## Register all support components
dbLoadDatabase "dbd/_APPNAME_.dbd"
_CSAFEAPPNAME__registerRecordDeviceDriver pdbbase

## Load record instances
## ndsCreateDevice - initializes NDS device of selected type
## and populates it with specified parameters
## _APPNAME_  - device driver name
##			    List of supported drivers could be get by calling ndsListDrivers
## $(PORT) 	  - Device name in IOC for references, also asyn port name.
## Parameters - List of parameters represented by Key=Value pair and separated by   
##              comma   		
ndsCreateDevice "_APPNAME_", "$(PORT)", "FILE=/tmp/q,N_AI=2,N_AO=3,N_DI=4,N_DO=5,N_DIO=6,N_IMAGE=7"

#yourIOCFunction "$(PORT)",<arguments>

dbLoadRecords "db/_APPNAME_.db", "PREFIX=_APPNAME_, ASYN_PORT=$(PORT), TIMEOUT=1"

#asynSetTraceMask "PIPE",       0, "0xFFFF"
#asynSetTraceMask "PIPE.ai",    0, "0xFFFF"
#asynSetTraceMask "PIPE.ao",    0, "0xFFFF"
#asynSetTraceMask "PIPE.di",    0, "0xFFFF"
#asynSetTraceMask "PIPE.do",    0, "0xFFFF"
#asynSetTraceMask "PIPE.c", 	0, "0xFFFF"

#
# TIFF plugin (areaDetector)
#
#NDFileTIFFConfigure("TIFF1", 3000, 0, "$(PORT).c", 0)
#dbLoadRecords("$(AREA_DETECTOR)/db/NDPluginBase.template","P=$(PREFIX)$(DIV),R=TIFF1$(DIV),PORT=TIFF1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT).c,NDARRAY_ADDR=0")
#dbLoadRecords("$(AREA_DETECTOR)/db/NDFile.template",      "P=$(PREFIX)$(DIV),R=TIFF1$(DIV),PORT=TIFF1,ADDR=0,TIMEOUT=1")
#dbLoadRecords("$(AREA_DETECTOR)/db/NDFileTIFF.template",  "P=$(PREFIX)$(DIV),R=TIFF1$(DIV),PORT=TIFF1,ADDR=0,TIMEOUT=1")


## Run this to trace the stages of iocInit
#traceIocInit

cd ${TOP}/iocBoot/${IOC}
iocInit

## Start any sequence programs
#seq sncExample, "user=_USER_Host"
