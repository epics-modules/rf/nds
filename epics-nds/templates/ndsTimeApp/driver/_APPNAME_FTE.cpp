/*
 * _APPNAME_FTE.cpp
 *
 *  Created on: 22. apr. 2013
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#include <ndsDriverCommand.h>

#include "_APPNAME_FTE.h"

FTE::FTE()
{
}

FTE::~FTE()
{

}

ndsStatus FTE::setOriginTime(asynUser* pasynUser, epicsFloat64 value)
{
    nds::DriverCommand *command = (nds::DriverCommand*)pasynUser->userData;
    std::cout << "FTE: setOriginTime EventID: "<< command->Args[0] << " TerminalID: "<<command->Args[1]  << std::endl;
    return nds::TimeEvent::setOriginTime(pasynUser, value);
}

ndsStatus FTE::getOriginTime(asynUser* pasynUser, epicsFloat64* value)
{
    nds::DriverCommand *command = (nds::DriverCommand*)pasynUser->userData;
    std::cout << "FTE: getOriginTime EventID: "<< command->Args[0] << " TerminalID: "<<command->Args[1]  << std::endl;
    return nds::TimeEvent::getOriginTime(pasynUser, value);
}

ndsStatus FTE::setDelay(asynUser* pasynUser, epicsFloat64 value)
{
    nds::DriverCommand *command = (nds::DriverCommand*)pasynUser->userData;
    std::cout << "FTE: setDelay EventID: "<< command->Args[0] << " TerminalID: "<<command->Args[1]  << std::endl;
    return nds::TimeEvent::setDelay(pasynUser, value);
}

ndsStatus FTE::getDelay(asynUser* pasynUser, epicsFloat64* value)
{
    nds::DriverCommand *command = (nds::DriverCommand*)pasynUser->userData;
    std::cout << "FTE: getDelay EventID: "<< command->Args[0] << " TerminalID: "<<command->Args[1]  << std::endl;
    return nds::TimeEvent::getDelay(pasynUser, value);
}

ndsStatus FTE::setEnabled(asynUser* pasynUser, epicsInt32 value)
{
    nds::DriverCommand *command = (nds::DriverCommand*)pasynUser->userData;
    std::cout << "FTE: setEnabled EventID: "<< command->Args[0] << " TerminalID: "<<command->Args[1]  << std::endl;
    return nds::TimeEvent::setEnabled(pasynUser, value);
}

ndsStatus FTE::getEnabled(asynUser* pasynUser, epicsInt32* value)
{
    nds::DriverCommand *command = (nds::DriverCommand*)pasynUser->userData;
    std::cout << "FTE: getEnabled EventID: "<< command->Args[0] << " TerminalID: "<<command->Args[1]  << std::endl;
    return nds::TimeEvent::getEnabled(pasynUser, value);
}

ndsStatus FTE::setLevel(asynUser* pasynUser, epicsInt32 value)
{
    nds::DriverCommand *command = (nds::DriverCommand*)pasynUser->userData;
    std::cout << "FTE: setLevel EventID: "<< command->Args[0] << " TerminalID: "<<command->Args[1]  << std::endl;
    return nds::TimeEvent::setLevel(pasynUser, value);
}

ndsStatus FTE::getLevel(asynUser* pasynUser, epicsInt32* value)
{
    nds::DriverCommand *command = (nds::DriverCommand*)pasynUser->userData;
    std::cout << "FTE: getLevel EventID: "<< command->Args[0] << " TerminalID: "<<command->Args[1]  << std::endl;
    return nds::TimeEvent::getLevel(pasynUser, value);
}
