/*
 * exChannel.h
 *
 *  Created on: 18. jul. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef EXCHANNEL_H_
#define EXCHANNEL_H_

#include <string>
#include <epicsMutex.h>
#include <epicsTypes.h>
#include <epicsTimer.h>

#include "ndsTypes.h"
#include "ndsTerminal.h"
#include "ndsTimer.h"

class Event;

class Terminal: public nds::Terminal
{
protected:
	int eventCounter;

	epicsTime timestamp;
    nds::TimerPtr eventTimer;

	int interruptEvent;
    int interruptTimeStamp;
    int interruptReason;

    volatile bool _stopThread;
public:
    /// Constructor
	Terminal();

    /// Destructor
    virtual ~Terminal();

    /// Test will register additional device PV handlers
    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);

    epicsTimerNotify::expireStatus  generateEvent(nds::TaskService &service, const epicsTime & currentTime);

	virtual void destroy();

	ndsStatus setReason(asynUser* pasynUser, epicsInt32 value);

    ndsStatus setStreamingURL(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransfered);

    ndsStatus doTest(asynUser *pasynUser, const nds::Message& value);

    ndsStatus onSwitchOn();

    void process();
};

#endif /* EXCHANNEL_H_ */
