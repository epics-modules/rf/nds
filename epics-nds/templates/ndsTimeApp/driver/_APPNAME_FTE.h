/*
 * _APPNAME_FTE.h
 *
 *  Created on: 22. apr. 2013
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef APPNAME_FTE_H_
#define APPNAME_FTE_H_

#include "ndsTimeEvent.h"

class FTE : public nds::TimeEvent
{
public:
    FTE();
    virtual ~FTE();

    ndsStatus setOriginTime(asynUser* pasynUser, epicsFloat64 value);
    ndsStatus getOriginTime(asynUser* pasynUser, epicsFloat64* value);

    ndsStatus setDelay(asynUser* pasynUser, epicsFloat64 value);
    ndsStatus getDelay(asynUser* pasynUser, epicsFloat64* value);

    ndsStatus setInverted(asynUser* pasynUser, epicsInt32 value);
    ndsStatus getInverted(asynUser* pasynUser, epicsInt32* value);

    ndsStatus setEnabled(asynUser* pasynUser, epicsInt32 value);
    ndsStatus getEnabled(asynUser* pasynUser, epicsInt32* value);

    ndsStatus setLevel(asynUser* pasynUser, epicsInt32 value);
    ndsStatus getLevel(asynUser* pasynUser, epicsInt32* value);

};

#endif /* APPNAME_FTE_H_ */
