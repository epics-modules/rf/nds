/*
 * testEventGroup.h
 *
 *  Created on: 21. avg. 2013
 *      Author: Slava Isaev
 */

#ifndef TESTEVENTGROUP_H_
#define TESTEVENTGROUP_H_

#include <ndsTypes.h>
#include <ndsChannelGroup.h>


class EventGroup : public nds::ChannelGroup
{
public:
    EventGroup(const std::string& name);
    virtual ~EventGroup();
};


#endif /* TESTEVENTGROUP_H_ */
