/*
 * ExampleDevice.cpp
 *
 *  Created on: 26.07.2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#include <stdint.h>
#include <boost/filesystem.hpp>

#include <ndsDeviceStates.h>
#include <ndsManager.h>
#include <ndsChannelGroup.h>
#include <ndsAutoChannelGroup.h>
#include <ndsChannel.h>

#include <ndsTimeEvent.h>
#include <ndsPulse.h>
#include <ndsClock.h>

#include "_APPNAME_Terminal.h"
#include "_APPNAME_Device.h"

#include "_APPNAME_FTE.h"
#include "_APPNAME_Pulse.h"
#include "_APPNAME_Clock.h"
#include "_APPNAME_EventGroup.h"


/** Registering new device specific driver
 *
 * This construction accepts 2 arguments:
 *  \lo Specific Device type;
 *  \lo Name of the driver by which it will be known in NDS Manager.
 * This driver name will be used in st.cmd file
 * ndsCreateDevice "testNDSDriver", "DAQDevice", "Parameter=Value"
 */
namespace
{
	// exDevice      - is a Specific Device type
	// exampleDevice - is auxiliary variable
	// testNDSDriver - is a driver name with which this driver will be populate
	nds::RegisterDriver<_APPNAME_Device> reg_APPNAME_Device("_APPNAME_");
}

/**
 *  Fabric function for automatic channels creation.
 */
nds::Channel* createPFI()
{
    return new Terminal();
}

_APPNAME_Device::_APPNAME_Device(const std::string& name):nds::Device(name)
,_counter(0)
,_timeRequestCounter(0)
{
	// C-tor of the Device object.
	// It is good place to register event handlers.

	// Registering state handlers...
	// See documentation to get state transfer allowance.

	// onRequest state handlers
	// this handler will be called when actor requests transfer to new state.
	// E.g. onSwitchOn handler will be called when actor calls on() member of the device.
	// this handler should return ndsSuccess to allow device go the ON state.
	// If this handler returns ndsError then device will go to the Error state.
	// (see documentation for details)
	registerOnRequestStateHandler(
			nds::DEVICE_STATE_INIT,   // Current state
			nds::DEVICE_STATE_ON,  // Requested state
			boost::bind(&_APPNAME_Device::onSwitchOn, this, _1, _2) );

	// onLeave state handlers
	// E.g. onLeaveOn handler will be called when device lives ON state.
	// It means that all request handlers return ndsSuccess and device
	// will definitely leave ON state
	registerOnLeaveStateHandler(nds::DEVICE_STATE_ON,
			boost::bind(&_APPNAME_Device::onLeaveOn, this, _1, _2 ) );

	registerOnLeaveStateHandler(nds::DEVICE_STATE_OFF,
			boost::bind(&_APPNAME_Device::onLeaveOff, this, _1, _2 ) );

	// onEnter state Handlers
	//E.g. onError handler will be called when device enters to the ERROR state.
	// Note: ERROR state doesn't support request schema it means that transfer to the
	// ERROR state is unconditional

    // If all ChannelGrous should be switched ON automatically,
	//when device is switched ON, uncomment lines below
//    registerOnEnterStateHandler(nds::DEVICE_STATE_ON,
//            boost::bind(&nds::Device::switchChannelGroupsOn, this, _1, _2 ) );

    // If all ChannelGrous should be switched OFF automatically,
    // when device is switched OFF, uncomment lines below
//    registerOnEnterStateHandler(nds::DEVICE_STATE_OFF,
//            boost::bind(&nds::Device::switchChannelGroupsOff, this, _1, _2 ) );

	registerOnEnterStateHandler(nds::DEVICE_STATE_ERROR,
			boost::bind(&_APPNAME_Device::onError, this, _1, _2 ) );

	registerOnEnterStateHandler(nds::DEVICE_STATE_OFF,
			boost::bind(&_APPNAME_Device::onEnterOff, this, _1, _2 ) );

	registerOnEnterStateHandler(nds::DEVICE_STATE_ON,
			boost::bind(&_APPNAME_Device::onEnterOn, this, _1, _2 ) );

    registerOnEnterStateHandler(nds::DEVICE_STATE_RESETTING,
            boost::bind(&_APPNAME_Device::onReset, this, _1, _2 ) );

}

ndsStatus _APPNAME_Device::registerHandlers(nds::PVContainers* pvContainers)
{
	// Example of additional PV Record handlers' registration

	// IMPORTANT! It is important to call parent register function
	// to register all default handlers.
	nds::Device::registerHandlers(pvContainers);

	NDS_PV_REGISTER_INT32ARRAY(
			"Time",           		 // The reason. It is from the IN/OUT field of PV's record.
			&_APPNAME_Device::setInt32Array,   	 // Here is stub for the Octet write operation is used.
									 // This stub should be used when operation doesn't have write
									 // or read operation
			&_APPNAME_Device::getTime, // Real read operation handler.
			&interruptTime);           	 // Address of variable to store interrupt ID of the reason.
									 // it used to dispathc interrupt though the
									 // doCallbacks... functions.

	return ndsSuccess;
}

_APPNAME_Device::~_APPNAME_Device()
{
	// TODO Auto-generated destructor stub
}


ndsStatus _APPNAME_Device::createStructure(const char* portName, const char* params)
{
    // Creating event's representors.
    // Order of event's representros is important because of default DB settings.
    nds::ChannelGroup *channelGroup = new EventGroup("events");
    registerChannelGroup( channelGroup );

    channelGroup->registerChannel(new FTE()   );
    channelGroup->registerChannel(new Pulse() );
    channelGroup->registerChannel(new Clock() );

   	//Getting int parameter
    int nChannels = getIntParam("TRG", 0);
    if (nChannels > 0 )
    {
        	// Creating Channel Group object.
        	// NDS C++ requires Channel Group object.
        	// For this oject AsynPort will be created to support new
        	// NDS asyn addressing. (see documentation for details)
        	nds::ChannelGroup *channelGroup = new nds::AutoChannelGroup("trg", &createPFI );

        	// All Channel groups should be registered.
			registerChannelGroup( channelGroup );
    }
    return ndsSuccess;
}

void _APPNAME_Device::destroy()
{
	/// Deterministic destroy operation.
	/// Good place to put closing of device specific
	/// file handlers.
	/// Proper way is to switch OFF the device from here.
	/// Note: the same member will be called
	/// for each child object (ChannelGroups and Channels)
}

ndsStatus _APPNAME_Device::onEnterInit(nds::DeviceStates, nds::DeviceStates)
{
	// This is an example how to set records values from code asynchronously.

	int size = 40;
	// This is one way of buffer initialization for string propogation.
	// Other way is make this buffer a class filed or static.
	char* buff = (char*)malloc(size);
    int32_t version = 123;
	memset(buff, 0,  size);
	snprintf(buff, size, "%d", version);

	// Setting of software version string
	// Note EPCIS limits strings with 40 symbols
	// _interruptIdFirmware - is a specification which records should be updated.
	doCallbacksOctet(buff, size, ASYN_EOM_END, _interruptIdFirmware, _portAddr );
	return ndsSuccess;
}

ndsStatus _APPNAME_Device::onEnterOn(nds::DeviceStates, nds::DeviceStates)
{
	NDS_TRC("onEnterOn");
	return ndsSuccess;
}

ndsStatus _APPNAME_Device::onEnterOff(nds::DeviceStates, nds::DeviceStates)
{
	NDS_TRC("onEnterOff");


	return ndsSuccess;
}

ndsStatus _APPNAME_Device::onSwitchOn(nds::DeviceStates, nds::DeviceStates)
{
	// Handling switch ON device request
	// If this function returns ndsError device will not be swtched ON.
	NDS_TRC("onSwitchOn");
	return ndsSuccess;
}

ndsStatus _APPNAME_Device::onLeaveOn(nds::DeviceStates, nds::DeviceStates)
{
	// Handling leave of ON state
	// Good place to close device specific file handlers.
	NDS_TRC("onLeaveOn");
	return ndsSuccess;
}

ndsStatus _APPNAME_Device::onError(nds::DeviceStates, nds::DeviceStates)
{
	// Good place to add error logging functionality.
	NDS_TRC("onError");
	return ndsSuccess;
}

ndsStatus _APPNAME_Device::onLeaveOff(nds::DeviceStates, nds::DeviceStates)
{
	NDS_TRC("onLeaveOff");
	return ndsSuccess;
}

ndsStatus _APPNAME_Device::getTime(asynUser *pasynUser, epicsInt32 *value, size_t nelements, size_t *nIn)
{
    epicsTime time = epicsTime::getCurrent();
    epicsTimeStamp stamp = (epicsTimeStamp)time;
    pasynUser->timestamp = stamp;

    *nIn = 2;
    value[0] = stamp.secPastEpoch;
    value[1] = stamp.nsec;
    return ndsSuccess;
}

ndsStatus _APPNAME_Device::onReset(nds::DeviceStates prevState,
        nds::DeviceStates currState)
{
    NDS_INF("Resetting device.");
    epicsThreadSleep(1.0);
    NDS_INF("Resetting complete.");
    getCurrentStateObj()->requestState(this, nds::DEVICE_STATE_ON);
    return ndsSuccess;
}

