/*
 * _APPNAME_Pulse.cpp
 *
 *  Created on: 22. apr. 2013
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#include <ndsDriverCommand.h>

#include "_APPNAME_Pulse.h"

Pulse::Pulse()
{
    // TODO Auto-generated constructor stub

}

Pulse::~Pulse()
{
    // TODO Auto-generated destructor stub
}

ndsStatus Pulse::setOriginTime(asynUser* pasynUser, epicsFloat64 value)
{
    nds::DriverCommand *command = (nds::DriverCommand*)pasynUser->userData;
    std::cout << "Pulse: setOriginTime EventID: "<< command->Args[0] << " TerminalID: "<<command->Args[1]  << std::endl;

    return nds::Pulse::setOriginTime(pasynUser, value);
}

ndsStatus Pulse::getOriginTime(asynUser* pasynUser, epicsFloat64* value)
{
    nds::DriverCommand *command = (nds::DriverCommand*)pasynUser->userData;
    std::cout << "Pulse: getOriginTime EventID: "<< command->Args[0] << " TerminalID: "<<command->Args[1]  << std::endl;

    return nds::Pulse::getOriginTime(pasynUser, value);
}

ndsStatus Pulse::setDelay(asynUser* pasynUser, epicsFloat64 value)
{
    nds::DriverCommand *command = (nds::DriverCommand*)pasynUser->userData;
    std::cout << "Pulse: setDelay EventID: "<< command->Args[0] << " TerminalID: "<<command->Args[1]  << std::endl;

    return nds::Pulse::setDelay(pasynUser, value);
}

ndsStatus Pulse::getDelay(asynUser* pasynUser, epicsFloat64* value)
{
    nds::DriverCommand *command = (nds::DriverCommand*)pasynUser->userData;
    std::cout << "Pulse: getDelay EventID: "<< command->Args[0] << " TerminalID: "<<command->Args[1]  << std::endl;

    return nds::Pulse::getDelay(pasynUser, value);
}

ndsStatus Pulse::setEnabled(asynUser* pasynUser, epicsInt32 value)
{
    nds::DriverCommand *command = (nds::DriverCommand*)pasynUser->userData;
    std::cout << "Pulse: setEnabled EventID: "<< command->Args[0] << " TerminalID: "<<command->Args[1]  << std::endl;

    return nds::Pulse::setEnabled(pasynUser, value);
}

ndsStatus Pulse::getEnabled(asynUser* pasynUser, epicsInt32* value)
{
    nds::DriverCommand *command = (nds::DriverCommand*)pasynUser->userData;
    std::cout << "Pulse: getEnabled EventID: "<< command->Args[0] << " TerminalID: "<<command->Args[1]  << std::endl;

    return nds::Pulse::getEnabled(pasynUser, value);
}

ndsStatus Pulse::setWidth(asynUser* pasynUser, epicsFloat64 value)
{
    nds::DriverCommand *command = (nds::DriverCommand*)pasynUser->userData;
    std::cout << "Pulse: setWidth EventID: "<< command->Args[0] << " TerminalID: "<<command->Args[1]  << std::endl;

    return nds::Pulse::setWidth(pasynUser, value);
}

ndsStatus Pulse::getWidth(asynUser* pasynUser, epicsFloat64* value)
{
    nds::DriverCommand *command = (nds::DriverCommand*)pasynUser->userData;
    std::cout << "Pulse: getWidth EventID: "<< command->Args[0] << " TerminalID: "<<command->Args[1]  << std::endl;

    return nds::Pulse::getWidth(pasynUser, value);
}

