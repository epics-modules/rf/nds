# ------------------------------- [ Channel  ] -------------------------------
# Template file: _APPNAME_ChannelGroup.template
#

record(mbbi, "$(PREFIX)-$(CHANNEL_ID)-STAT") {
	field(DESC, "The state of the channel.")
	field(DTYP, "asynInt32")
	field(INP,  "@asyn($(ASYN_PORT), $(ASYN_ADDR))State")
	field(ZRVL, "0")
	field(ZRST, "UNKNOWN")
	field(ONVL, "1")
	field(ONST, "IOCINIT")
	field(TWVL, "2")
	field(TWST, "DISABLED")
	field(THVL, "3")
	field(THST, "ENABLED")
	field(FRVL, "4")
	field(FRST, "PROCESSING")
	field(FVVL, "5")
	field(FVST, "DEGRADED")
	field(SXVL, "6")
	field(SXST, "ERROR")
	field(SVVL, "7")
	field(SVST, "RESET")
	field(EIVL, "8")
	field(EIST, "DEFUNCT")
	field(SCAN, "I/O Intr")	
}

record(mbbo, "$(PREFIX)-$(CHANNEL_ID)-CLKS") {
	field(DESC, "Set the clock source for sampling.")
	field(DTYP, "asynInt32")
	field(OUT,  "@asyn($(ASYN_PORT), $(ASYN_ADDR))ClockSource")
	field(ZRVL, "0")
	field(ZRST, "INT")
	field(ONVL, "1")
	field(ONST, "INT1")
	field(TWVL, "2")
	field(TWST, "INT2")
	field(THVL, "3")
	field(THST, "INT3")
	field(FRVL, "4")
	field(FRST, "TCN")
	field(FVVL, "5")
	field(FVST, "EXT1")
	field(FVVL, "6")
	field(FVST, "EXT2")
	field(FVVL, "7")
	field(FVST, "EXT3")
#	field(VAL,  "0")
#	field(IVOV, "0")
}

record(ao, "$(PREFIX)-$(CHANNEL_ID)-CLKF") {
	field(DESC, "Set the frequency of the internal clock.")
	field(DTYP, "asynFloat64")
	field(OUT,  "@asyn($(ASYN_PORT), $(ASYN_ADDR))ClockFrequency")
#	field(VAL,  "2.5")
#	field(PINI, "YES")
}

record(ao, "$(PREFIX)-$(CHANNEL_ID)-CLKM") {
	field(DESC, "Set the external clock multiplier.")
	field(DTYP, "asynFloat64")
	field(OUT,  "@asyn($(ASYN_PORT), $(ASYN_ADDR))ClockMultiplier")
	field(VAL,  "1")
#	field(PINI, "NO")
}

record(bo, "$(PREFIX)-$(CHANNEL_ID)-CLKO") {
	field(DESC, "Enable trigger routing")
	field(DTYP, "asynInt32")
	field(OUT,	"@asyn($(ASYN_PORT), $(ASYN_ADDR))ClockRouting")
	field(ZNAM, "Disabled")
	field(ONAM, "Enabled")
}

record(stringout, "$(PREFIX)-$(CHANNEL_ID)-CLKA") {
	field(DESC, "Set the trigger's routing target")
	field(DTYP, "asynOctetWrite")
	field(OUT,	"@asyn($(ASYN_PORT), $(ASYN_ADDR))ClockTarget")
}

record(bo, "$(PREFIX)-$(CHANNEL_ID)-CLKP") {
	field(DESC, "Setting the clock polarity")
	field(DTYP, "asynInt32")
	field(OUT,	"@asyn($(ASYN_PORT), $(ASYN_ADDR))ClockPolarity")
	field(ZNAM, "Rising")
	field(ONAM, "Falling")
}

# Messaging
record(waveform, "$(PREFIX)-$(CHANNEL_ID)-MSGS") {
	field(DESC, "Send message to device driver.")
	field(DTYP, "asynOctetWrite")
	field(INP, "@asyn($(ASYN_PORT), $(ASYN_ADDR))Command")
	field(FTVL, "UCHAR")
	field(NELM, "255")	
}

record(waveform, "$(PREFIX)-$(CHANNEL_ID)-MSGR") {
	field(DESC, "Receive message from device driver.")
	field(DTYP, "asynOctetRead")
	field(INP, "@asyn($(ASYN_PORT), $(ASYN_ADDR))Command")
	field(SCAN, "I/O Intr")	
	field(FTVL, "UCHAR")
	field(NELM, "255")
}

record(mbbo, "$(PREFIX)-$(CHANNEL_ID)-PM") {
	field(DESC, "Set processing mode for sampling.")
	field(DTYP, "asynInt32")
	field(OUT,  "@asyn($(ASYN_PORT), $(ASYN_ADDR))ProcessingMode")
	field(ZRVL, "0")
	field(ONVL, "1")
	field(TWVL, "2")
	field(ZRST, "Single-point")
	field(ONST, "Finite")
	field(TWST, "Continues")
}

record(longout, "$(PREFIX)-$(CHANNEL_ID)-SMNM") {
	field(DESC, "Number of samples for finite mode")
	field(DTYP, "asynInt32")
	field(OUT,	"@asyn($(ASYN_PORT), $(ASYN_ADDR))SamplesCount")
}


#Start trigger
record(ao, "$(PREFIX)-$(CHANNEL_ID)-TRGD") {
	field(DESC, "Setting the trigger delay")
	field(DTYP, "asynFloat64")
	field(OUT,	"@asyn($(ASYN_PORT), $(ASYN_ADDR))TriggerDelay")
}

record(longout, "$(PREFIX)-$(CHANNEL_ID)-TRGR") {
	field(DESC, "Setting the trigger repeat number")
	field(DTYP, "asynInt32")
	field(OUT,	"@asyn($(ASYN_PORT), $(ASYN_ADDR))TriggerRepeat")
}

record(waveform, "$(PREFIX)-$(CHANNEL_ID)-TRG") {
	field(DESC, "Set the triggering time")
	field(DTYP, "asynOctetWrite")
	field(INP,	"@asyn($(ASYN_PORT), $(ASYN_ADDR))Trigger")
	field(FTVL, "UCHAR")
	field(NELM, "255")	
}

record(bo, "$(PREFIX)-$(CHANNEL_ID)-TRGT") {
	field(DESC, "Trigger type software or hardware")
	field(DTYP, "asynInt32")
	field(OUT,	"@asyn($(ASYN_PORT), $(ASYN_ADDR))TriggerType")
	field(ZNAM, "Software")
	field(ONAM, "Hardware")
}

record(bo, "$(PREFIX)-$(CHANNEL_ID)-TRGP") {
	field(DESC, "Setting the trigger polarity")
	field(DTYP, "asynInt32")
	field(OUT,	"@asyn($(ASYN_PORT), $(ASYN_ADDR))TriggerPolarity")
	field(ZNAM, "Rising")
	field(ONAM, "Falling")
}

record(bo, "$(PREFIX)-$(CHANNEL_ID)-TRGO") {
	field(DESC, "Enable trigger routing")
	field(DTYP, "asynInt32")
	field(OUT,	"@asyn($(ASYN_PORT), $(ASYN_ADDR))TriggerRouting")
	field(ZNAM, "Disabled")
	field(ONAM, "Enabled")
}

record(stringout, "$(PREFIX)-$(CHANNEL_ID)-TRGA") {
	field(DESC, "Set the trigger's routing target")
	field(DTYP, "asynOctetWrite")
	field(OUT,	"@asyn($(ASYN_PORT), $(ASYN_ADDR))TriggerTarget")
}

# Pause trigger

record(ao, "$(PREFIX)-$(CHANNEL_ID)-PTGD") {
	field(DESC, "Setting the trigger delay")
	field(DTYP, "asynFloat64")
	field(OUT,	"@asyn($(ASYN_PORT), $(ASYN_ADDR))PauseTriggerDelay")
}

record(longout, "$(PREFIX)-$(CHANNEL_ID)-PTGR") {
	field(DESC, "Setting the trigger repeat number")
	field(DTYP, "asynInt32")
	field(OUT,	"@asyn($(ASYN_PORT), $(ASYN_ADDR))PauseTriggerRepeat")
}

record(waveform, "$(PREFIX)-$(CHANNEL_ID)-PTRG") {
	field(DESC, "Set the triggering time")
	field(DTYP, "asynOctetWrite")
	field(INP,	"@asyn($(ASYN_PORT), $(ASYN_ADDR))PauseTrigger")
	field(FTVL, "UCHAR")
	field(NELM, "255")
}

record(bo, "$(PREFIX)-$(CHANNEL_ID)-PTGT") {
	field(DESC, "Trigger type software or hardware")
	field(DTYP, "asynInt32")
	field(OUT,	"@asyn($(ASYN_PORT), $(ASYN_ADDR))PauseTriggerType")
	field(ZNAM, "Software")
	field(ONAM, "Hardware")
}

record(bo, "$(PREFIX)-$(CHANNEL_ID)-PTGP") {
	field(DESC, "Setting the trigger polarity")
	field(DTYP, "asynInt32")
	field(OUT,	"@asyn($(ASYN_PORT), $(ASYN_ADDR))PauseTriggerPolarity")
	field(ZNAM, "Rising")
	field(ONAM, "Falling")
}

record(bo, "$(PREFIX)-$(CHANNEL_ID)-PTGO") {
	field(DESC, "Enable trigger routing")
	field(DTYP, "asynInt32")
	field(OUT,	"@asyn($(ASYN_PORT), $(ASYN_ADDR))PauseTriggerRouting")
	field(ZNAM, "Disabled")
	field(ONAM, "Enabled")
}

record(stringout, "$(PREFIX)-$(CHANNEL_ID)-PTGA") {
	field(DESC, "Set the trigger's routing target")
	field(DTYP, "asynOctetWrite")
	field(OUT,	"@asyn($(ASYN_PORT), $(ASYN_ADDR))PauseTriggerTarget")
}

# Reference trigger

record(ao, "$(PREFIX)-$(CHANNEL_ID)-RTGD") {
	field(DESC, "Setting the trigger delay")
	field(DTYP, "asynFloat64")
	field(OUT,	"@asyn($(ASYN_PORT), $(ASYN_ADDR))ReferenceTriggerDelay")
}

record(longout, "$(PREFIX)-$(CHANNEL_ID)-RTGR") {
	field(DESC, "Setting the trigger repeat number")
	field(DTYP, "asynInt32")
	field(OUT,	"@asyn($(ASYN_PORT), $(ASYN_ADDR))ReferenceTriggerRepeat")
}

record(waveform, "$(PREFIX)-$(CHANNEL_ID)-RTRG") {
	field(DESC, "Set the triggering time")
	field(DTYP, "asynOctetWrite")
	field(INP,	"@asyn($(ASYN_PORT), $(ASYN_ADDR))ReferenceTrigger")
	field(FTVL, "UCHAR")
	field(NELM, "255")	
}

record(bo, "$(PREFIX)-$(CHANNEL_ID)-RTGT") {
	field(DESC, "Trigger type software or hardware")
	field(DTYP, "asynInt32")
	field(OUT,	"@asyn($(ASYN_PORT), $(ASYN_ADDR))ReferenceTriggerType")
	field(ZNAM, "Software")
	field(ONAM, "Hardware")
}

record(bo, "$(PREFIX)-$(CHANNEL_ID)-RTGP") {
	field(DESC, "Setting the trigger polarity")
	field(DTYP, "asynInt32")
	field(OUT,	"@asyn($(ASYN_PORT), $(ASYN_ADDR))ReferenceTriggerPolarity")
	field(ZNAM, "Rising")
	field(ONAM, "Falling")
}

record(bo, "$(PREFIX)-$(CHANNEL_ID)-RTGO") {
	field(DESC, "Enable trigger routing")
	field(DTYP, "asynInt32")
	field(OUT,	"@asyn($(ASYN_PORT), $(ASYN_ADDR))ReferenceTriggerRouting")
	field(ZNAM, "Disabled")
	field(ONAM, "Enabled")
}

record(stringout, "$(PREFIX)-$(CHANNEL_ID)-RTGA") {
	field(DESC, "Set the trigger's routing target")
	field(DTYP, "asynOctetWrite")
	field(OUT,	"@asyn($(ASYN_PORT), $(ASYN_ADDR))ReferenceTriggerTarget")
}

#Convert clock

record(stringout, "$(PREFIX)-$(CHANNEL_ID)-CCLB") {
	field(DESC, "Set the trigger's routing target")
	field(DTYP, "asynOctetWrite")
	field(OUT,	"@asyn($(ASYN_PORT), $(ASYN_ADDR))ConvertClockBase")
}

record(longout, "$(PREFIX)-$(CHANNEL_ID)-CCLD") {
	field(DESC, "Setting the convert clock delay")
	field(DTYP, "asynInt32")
	field(OUT,	"@asyn($(ASYN_PORT), $(ASYN_ADDR))ConvertClockDelay")
}

record(bo, "$(PREFIX)-$(CHANNEL_ID)-CCLP") {
	field(DESC, "Setting the convert clock polarity")
	field(DTYP, "asynInt32")
	field(OUT,	"@asyn($(ASYN_PORT), $(ASYN_ADDR))ConvertClockPolarity")
	field(ZNAM, "Rising")
	field(ONAM, "Falling")
}

record(bo, "$(PREFIX)-$(CHANNEL_ID)-CCLO") {
	field(DESC, "Enable convert clock routing")
	field(DTYP, "asynInt32")
	field(OUT,	"@asyn($(ASYN_PORT), $(ASYN_ADDR))ConvertClockRouting")
	field(ZNAM, "Disabled")
	field(ONAM, "Enabled")
}

record(stringout, "$(PREFIX)-$(CHANNEL_ID)-CCLA") {
	field(DESC, "Set the convert clock routing target")
	field(DTYP, "asynOctetWrite")
	field(OUT,	"@asyn($(ASYN_PORT), $(ASYN_ADDR))ConvertClockTarget")
}

record(bo, "$(PREFIX)-$(CHANNEL_ID)-ENBL")
{
	field(DTYP, "asynInt32")
    field(OUT,"@asyn( $(ASYN_PORT), $(ASYN_ADDR))Enabled")
    field(PINI, "YES")        
    field(VAL, "$(ENABLED)")
}
