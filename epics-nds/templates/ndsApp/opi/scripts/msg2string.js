importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

// It is expected that 1st PV is a MSGR 
var valueArray = PVUtil.getLongArray( pvs[0] );
var stringArray = Array(); 

for( id in valueArray)
{
	stringArray.push(String.fromCharCode( valueArray[id] ));
}

// Print out received message
display.getWidget("lblDebug").setPropertyValue("text", stringArray.join("") );
