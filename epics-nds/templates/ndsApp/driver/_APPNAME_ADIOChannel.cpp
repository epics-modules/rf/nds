/*
 * exChanel.cpp
 *
 *  Created on: 18. jul. 2012
 *      Author: Slava Isaev, Klemen Zagar
 *
 *      (C) Cosylab. Slovenia 2012
 */

#include <errlog.h>
#include <iocsh.h>
#include <string.h>

#include <sys/epoll.h>
#include <sys/eventfd.h>
#include <sys/stat.h>
#include <sys/timerfd.h>
#include <epicsThread.h>
#include <epicsMutex.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <time.h>

#include "ndsChannelStates.h"
#include "ndsDriverCommand.h"
#include "ndsThreadTask.h"
#include "ndsPeriodicTask.h"
#include "ndsPollingTask.h"

#include "_APPNAME_.h"
#include "_APPNAME_ADIOChannel.h"

ExADIOChannel::ExADIOChannel(
    int type,
    const std::string& prefix,
    const std::string&  suffix,
    int i,
    int bufSize):
        outFileMutex(epicsMutexCreate()),
        fileFD(-1),
        channelType(type),
		prefix(prefix),
		suffix(suffix),
		userIndex(i),
		eventCounter(0),
        timestamp()
{
    _ClockFrequency = 0;
	/*
	 * Registering state change handlers.
	 * State machine supports 3 levels of handlers.
	 */

	/*
	 * 1st level onRequest handlers
	 * These handlers are executed before state change and should succeed
	 *  to allow transfer to new state.
	 */
    registerOnRequestStateHandler(
            nds::CHANNEL_STATE_OFF,  // Current state
            nds::CHANNEL_STATE_PROCESSING,   // Requested state
            boost::bind(&ExADIOChannel::onProcessingRequested, this, _1, _2) );


    // If you would like to forbid transition from OFF state to ON state uncomment code below.
    // This default request handler returns ndsBlockTransition so device will stay in the same state (OFF).
//    registerOnRequestStateHandler(
//            nds::CHANNEL_STATE_OFF,   // Current state
//            nds::CHANNEL_STATE_ON,  // Requested state
//            boost::bind(&nds::BaseChannel::onWrongStateRequested, this, _1, _2) );

	/*
	 * 2nd level onLeave handlers for the current state
	 * These handlers are executed when state machine leaving current state.
	 */
	registerOnLeaveStateHandler(nds::CHANNEL_STATE_PROCESSING,   // Requested state
            boost::bind(&ExADIOChannel::stopProcessing, this, _1, _2) );

	/*
	 *  3rd level onEnter handler for the new state
	 *  These Hnadlers are executed right after the state was changed.
	 */
    registerOnEnterStateHandler(nds::CHANNEL_STATE_PROCESSING,
            boost::bind(&ExADIOChannel::onSwitchOn, this, _1, _2) );

    //No need
    if(type == CHANNEL_TYPE_IMAGE) {
    } else {
          _BufferFloat32Size = bufSize;
          _BufferFloat32 = (epicsFloat32*)callocMustSucceed(
        		  bufSize,
        		  sizeof(epicsFloat32),
        		  "Unable to allocate memory!");
          NDS_TRC("Buffer: %p", _BufferFloat32);
    }

	// Registration of the handlers for the messaging mechanism
	registerMessageWriteHandler(
	    		"FREQ", /// Name of the message type which will be handled by this handler
	    		boost::bind(
	    				&ExADIOChannel::printFrequency, /// Address of the handler
	    				this, 					   /// Object which owns the handler
	    				_1,_2)					   /// Stub functors (see boost::bind documentation for details.)
	);

    /* NDS does not yet support software triggering.
     * So we fake it and say that triggering is always enabled.
     */
    _isTriggered = 1;

    char file[256];

    NDS_TRC("ExChannel::initialize");

    _isOutput = !( (channelType == CHANNEL_TYPE_AI) ||
            (channelType == CHANNEL_TYPE_DI) ||
            (channelType == CHANNEL_TYPE_IMAGE)) ||
            (channelType == CHANNEL_TYPE_DIO);

    snprintf(file, sizeof(file), "%s.%s.%i", prefix.c_str(), suffix.c_str(),
            userIndex);

    // All initialization stuff which is not required on instantiation time
    fileName = strdup(file);

    char timerName[80];
    snprintf(timerName, 80, "EventTimer_%s", file);

    eventTimer = nds::Timer::create(timerName, boost::bind(&ExADIOChannel::generateEvent, this, _1, _2) );

    if (!_isOutput)
    {
        snprintf(timerName, sizeof(timerName), "pipe-reader-%s", fileName);

        taskPolling = nds::PollingTask::create(
                timerName,
                epicsThreadGetStackSize(epicsThreadStackSmall),
                epicsThreadPriorityMedium);
    }

    snprintf(timerName, sizeof(timerName), "pipe-sampler-%s", fileName);

    taskPeriodic = nds::PeriodicTask::create(
            timerName,
            epicsThreadGetStackSize(epicsThreadStackSmall),
            epicsThreadPriorityMedium,
            boost::bind(
                    &ExADIOChannel::processSamplingBody,
                    this,
                    _1)
    );
}

ndsStatus ExADIOChannel::registerHandlers(nds::PVContainers* pvContainers)
{
	nds::ADIOChannel::registerHandlers(pvContainers);

	NDS_PV_REGISTER_OCTET("FindChannel", &ExADIOChannel::findChannelByName ,&nds::Base::getOctet, &idFindChannelGroup);
	NDS_PV_REGISTER_OCTET("FindChannelGroup", &ExADIOChannel::findChannelGroup, &nds::Base::getOctet, &idFindChannelGroup);

	NDS_PV_REGISTER_INT32("Event", &nds::Base::setInt32, &ExADIOChannel::getInt32, &idEvent);
	NDS_PV_REGISTER_INT32ARRAY("TimeStamp", &nds::Base::setInt32Array, &ExADIOChannel::getInt32Array, &idTimeStamp);
	NDS_PV_REGISTER_INT32("Reason", &ExADIOChannel::setReason, &ExADIOChannel::getInt32, &idReason);

	return ndsSuccess;
}

/// read value from the channel
ndsStatus ExADIOChannel::getValueFloat64(asynUser* pasynUser, epicsFloat64 *value)
{
	*value=lastValueRead;
	return ndsSuccess;	
}

/*
 * Destroy a channel by freeing all its resources.
 */
ExADIOChannel::~ExADIOChannel()
{

}

ndsStatus ExADIOChannel::setClockFrequency(asynUser* pasynUser, epicsFloat64 value)
{
    NDS_TRC("ExChannel::writeClockFrequency");

    if ( getCurrentState() == nds::CHANNEL_STATE_PROCESSING )
    {
        NDS_DBG("Channel's can't be configured in this state.");
        return ndsError;
    }

    /// calling parents method to provide standard functionality
    Channel::setClockFrequency(pasynUser, value);

    return ndsSuccess;
}

ndsStatus ExADIOChannel::writeToOutput(epicsFloat64 value)
{
    int status;

    /* Make sure we are the only writer on the output file at some time. */
    if(epicsMutexLock(outFileMutex) != epicsMutexLockOK) {
        return ndsError;
    }

    if(outFileHandle == NULL) {
        /* Open the pipe in non-blocking mode.
         * Otherwise, the opening would block until the pipe is opened for
         * reading on the other end. */
        fileFD = open(fileName, O_NONBLOCK | O_WRONLY);
        if(fileFD == -1) {
            if(errno == ENXIO) {
                /* No reader connected on the other end of the pipe yet.
                 * Then there is no sense to try to write. */
                epicsMutexUnlock(outFileMutex);
                return ndsSuccess;
            } else {
                epicsMutexUnlock(outFileMutex);
                return ndsError;
            }
        }

        /* We need the file handle, not the file descriptor,
         * as we'll be using fprintf to write. */
        outFileHandle = fdopen(fileFD, "w");
        if(outFileHandle == NULL) {
            epicsMutexUnlock(outFileMutex);
            return ndsError;
        }
    }

    /* Write the value. */
    status = fprintf(outFileHandle, "%f\n", value);
    if(status < 0) {
        fclose(outFileHandle);
        close(fileFD);
        outFileHandle = NULL;
        epicsMutexUnlock(outFileMutex);
        return ndsError;
    }

    /* Flush the output. */
    fflush(outFileHandle);

    epicsMutexUnlock(outFileMutex);
    return ndsSuccess;
}

ndsStatus ExADIOChannel::createChannelPipeAndThreads()
{
    struct stat fileStat;
    int needNew = 1;

    NDS_TRC("%s\n", fileName);

    /* Remove the file if it already exists. */
    if (stat(fileName, &fileStat) == 0)
    {
        if (S_ISFIFO(fileStat.st_mode))
        {
            needNew = 0;
        }
        else
        {
            if (unlink(fileName) != 0)
            {
                NDS_ERR("Unable to remove file '%s'. Error: '%s'.\n", fileName,
                        strerror(errno));
                return ndsError;
            }
        }
    }

    /* Create a FIFO file / pipe. */
    if (needNew)
    {
        if (mkfifo(fileName, S_IWUSR | S_IRUSR | S_IWGRP | S_IRGRP | S_IROTH)
                != 0)
        {
            NDS_ERR("Can't create FIFO file '%s'. Error: '%s'.\n", fileName,
                    strerror(errno));
            return ndsError;
        }
    }
    return ndsSuccess;
}

void ExADIOChannel::processSamplingBody(nds::TaskServiceBase& service)
{
    /**
     * _TriggerDelay can change at any time, so we use a local variable to ensure
     * that trigger delay is constant during the execution of the while loop.
     */

    /// Period, sampleDelay and ringBufferSize can change only when signal is not being sampled.
    if (state == STATE_NOT_TRIGGERED)
    {
        localTriggerDelay = _TriggerDelay;

        /**
         * Trigger delay translated from seconds to number of samples. clockPeriod() returns the
         * value in nanoseconds, hence 1.0e9.
         */
        sampleDelay = abs(((localTriggerDelay * 1.0e9) / clockPeriod()));

        /// ringBuffer and ringBufferSize are modified only if the new sampleDelay requires a larger ringBuffer.
        if ((localTriggerDelay < 0) && (sampleDelay > oldSampleDelay))
        {
            /// +1 assures, that the newest value in ringBuffer is the last value copied to _BufferFloat32
            ringBufferSize = (abs(sampleDelay) + 1);
            ringBuffer = (epicsFloat32*) realloc(ringBuffer,
                    ringBufferSize * sizeof(epicsFloat32));
            oldSampleDelay = sampleDelay;
            NDS_INF(
                    "ringBuffer memory block reallcoated due to change in ringBufferSize.");
        }
    }

    if (_isOutput)
    {
        /* For output channels, output the next value in the buffer. */
        writeToOutput (_BufferFloat32[bufferPosition]);

        /* Advance position in the channel's buffer. */
        ++bufferPosition;
        /* Have we reached the end of the buffer? */
        if (bufferPosition >= _BufferFloat32Size)
            bufferPosition = 0;
    }
    else
    {
        /**
         * if trigger delay is non-negative, dataAcqusition will use _BufferFloat32 to
         * sample data. If trigger delay is negative, preDataAcquisition uses additional
         * ring buffer to continuously sample data.
         */
        if (localTriggerDelay >= 0)
        {
            dataAcquisition();
        }
        else
        {
            preDataAcquisition();
        }
    }
}


void ExADIOChannel::dataAcquisition()
{
    switch (state)
    {
    case STATE_TRIGGERED:
        /**
         * Until buffer is filled, we acquire data even if trigger condition
         * is not met anymore (checkTrigger()=0)
         */
        if (bufferPosition < _BufferFloat32Size)
        {
            _BufferFloat32[bufferPosition] = lastValueRead;
            bufferPosition++;
        }
        /// When buffer is full, copy it to the waveform record and set the bufferPosition to 0.
        else if (bufferPosition == _BufferFloat32Size)
        {
            _BufferFloat32[bufferPosition] = lastValueRead;
            bufferPosition = 0;
            doCallbacksFloat32Array(_BufferFloat32, _BufferFloat32Size,
                    _interruptIdBufferFloat32, _portAddr);
            /// If trigger condition is no longer met, switch to STATE_NOT_TRIGGERED
            if (!_trigger->checkTrigger())
            {
                state = STATE_NOT_TRIGGERED;
            }
        }
        break;

    case STATE_NOT_TRIGGERED:
        if (_trigger->checkTrigger())
        {
            /// if triggerDelay = 0, start sampling
            if (bufferPosition == sampleDelay)
            {
                _BufferFloat32[bufferPosition] = lastValueRead;
                bufferPosition = 1;
                state = STATE_TRIGGERED;
            }
            /// If sampleDelay > 0, advance position of bufferPosition and go to STATE_TRANSITION
            else if (bufferPosition < sampleDelay)
            {
                state = STATE_TRANSITION;
                bufferPosition++;
            }

        }
        break;

        /**
         * When bufferPosition reaches sampleDelay, we start acquisition (even if
         * checkTrigger() == false)
         */
    case STATE_TRANSITION:
        if (bufferPosition < sampleDelay)
        {
            bufferPosition++;
        }
        else if (bufferPosition == sampleDelay)
        {
            /**
             *	if buffer position has reached sampleDelay, reset it's value,
             *	sample the first element and proceed to STATE_TRIGGERED
             */
            _BufferFloat32[0] = lastValueRead;
            bufferPosition = 1;
            state = STATE_TRIGGERED;
        }
    }
}

void ExADIOChannel::preDataAcquisition(){
	/**
	 * To avoid missing the signal on the input channel, we do not copy the 
	 * entire sampleDelay length of ringBuffer to _BufferFloat32 at once (when 
	 * trigger condition is met), we rather copy the elements one by one.
	*/

	/// signal is continuously sampled into ringBuffer
	ringBuffer[ringBufferPosition] = lastValueRead;
		
	switch(state){
		case STATE_TRIGGERED:							
			/**
			 * If trigger condition is not met anymore we mark the position of
			 * the first value in ringBuffer that broke the condition by assigning it 
			 * to stopTriggerPosition and we proceed to STATE_TRANSITION.
			 */
			if(!_trigger->checkTrigger()){
				stopTriggerPosition = ringBufferPosition;
				state = STATE_TRANSITION;
			}

			/**
			 * We sample signal into the buffer until the buffer is full, then we 
			 * copy the buffer into the waveform record.
			 */
			if(bufferPosition < (_BufferFloat32Size - 1)){
				_BufferFloat32[bufferPosition] = ringBuffer[triggerPosition];
				bufferPosition++;
				triggerPosition++;
			} else if(bufferPosition == (_BufferFloat32Size - 1)){	
				_BufferFloat32[bufferPosition] = ringBuffer[triggerPosition];
				bufferPosition = 0;
				triggerPosition++;

				doCallbacksFloat32Array(
    				_BufferFloat32,
    				_BufferFloat32Size,
                	_interruptIdBufferFloat32,
               		_portAddr);
				}									
			if(abs(triggerPosition) == ringBufferSize){
				triggerPosition = 0;
			}
			break;

		case STATE_TRANSITION:
			/**
			 * When triggerPosition reaches stopTriggerPosition all triggered 
			 * values sampled in ringBuffer have been copied to _BufferFloat32. We can 
			 * switch to STATE_NOT_TRIGGERED after the next doCallBack, unless 
			 * trigger.checkTrigger() = true appears.
			 */
			if(triggerPosition == stopTriggerPosition){
				stopAcquisition = true;
			}

			/*If trigger.checkTrigger() = true appears, switch back to STATE_TRIGGERED*/
			if(_trigger->checkTrigger()){
				state = STATE_TRIGGERED;
				stopAcquisition = false;
			}

			/**
			 * Acquire data until the buffer is full, then copy the buffer to waveform record.
			 */
			if(bufferPosition < (_BufferFloat32Size - 1)){
					_BufferFloat32[bufferPosition] = ringBuffer[triggerPosition];
					bufferPosition++;
					triggerPosition++;
				} else if(bufferPosition == (_BufferFloat32Size - 1)){	
					_BufferFloat32[bufferPosition] = ringBuffer[triggerPosition];
					bufferPosition = 0;
					triggerPosition++;
					doCallbacksFloat32Array(
						_BufferFloat32,
						_BufferFloat32Size,
		            	_interruptIdBufferFloat32,
		           		_portAddr);
					/**
					 * If all triggered values have been copied from ringBuffer to _BufferFloat32, 
					 * switch to STATE_NOT_TRIGGERED and reset stopAcquisition.
					 */
					if(stopAcquisition){
						state=STATE_NOT_TRIGGERED;
						stopAcquisition = false;									
					}												
				}
				if(abs(triggerPosition) == ringBufferSize){
					triggerPosition = 0;
				}
				break;

		case STATE_NOT_TRIGGERED:
			if(_trigger->checkTrigger()){
				 /// Coordinate of the first element from ringBuffer to be copied into _BufferFloat32. 
				triggerPosition = ringBufferPosition - sampleDelay;

				/**
				 * A negative triggerPosition must be added to _BufferFloat32Size 
				 * to determine the real coordinate of the first element to be sampled.
				 */
				if(triggerPosition < 0){
					triggerPosition = ringBufferSize + triggerPosition;
				}
				/// sample the first value and proceed to STATE_TRIGGERED.
				_BufferFloat32[bufferPosition] = ringBuffer[triggerPosition];
				triggerPosition++;
				bufferPosition++;
				state=STATE_TRIGGERED;
			}
			break;
	}
	/**
	 * Advance the position of the ringBuffer. If we have reached the end of the buffer,
	 * reset the position to 0.
	 */
	ringBufferPosition++;
	if(ringBufferPosition == ringBufferSize){
		ringBufferPosition = 0;
	}
}

void ExADIOChannel::processReadBody(nds::TaskServiceBase& service, const struct epoll_event& event)
{
    float oldValue = NAN;
    int j;
    char buf[128];
    int nread;
    float value = 0;
    float factor = 0;
    bool minus;
    minus = false;

    /* Data is ready: read it into a buffer. */
    nread = read(fileFD, buf, sizeof(buf));
    for (j = 0; j < nread; ++j)
    {
        /* Process the buffer. Parse decimal numbers. */
        if (buf[j] >= '0' && buf[j] <= '9')
        {
            if (factor == 0)
            {
                if (isnan(value))
                {
                    value = 0;
                }
                /* Before decimal separator. */
                value = value * 10 + (buf[j] - '0');
            }
            else
            {
                /* After decimal separator. 'factor' is the multiplier to use. */
                value += factor * (buf[j] - '0');
                factor *= 0.1;
            }
        }
        else if (buf[j] == '.' && factor == 0)
        {
            if (isnan(value))
            {
                value = 0;
            }
            /* If decimal separator is encountered, set the multiplier. */
            factor = 0.1;
        }
        else if (buf[j] == '\n' || buf[j] == '\r')
        {
            /* End-of-line means confirm the number. */
            if (!isnan(value))
            {
                if (buf[0] == '-')
                {
                    value = -value;
                }
                lastValueRead = value;
                /* Notify the channel that the value has changed. */
                if (oldValue != lastValueRead)
                {

                    /// Propagate read value to the target PV Record
                    doCallbacksFloat64(lastValueRead, _interruptIdValueFloat64,
                            _portAddr);

                    oldValue = lastValueRead;
                }
            }
            value = NAN;
            factor = 0;
        }
        else
        {
            /* Anything else means invalid input. */
            value = NAN;
            factor = 0;
        }
    }
}

ndsStatus ExADIOChannel::printFrequency(asynUser* pasynUser, const nds::Message& value)
{
	/// Example how the default parameters could be set.

    NDS_STK("getSoftwareVersion");

	nds::Message response;
	response.messageType = value.messageType;
	response.insert( "CODE", "0" );
	nds::insert(response, "FREQ", _ClockFrequency );

	doCallbacksMessage(response);
    return ndsSuccess;
}

ndsStatus ExADIOChannel::findChannelByName(asynUser *pasynUser, const char *value, size_t maxChars, size_t *nActual)
{
	std::string channelName(value);
	nds::Channel* channel=0;
	if ( ndsSuccess == _device->getChannelByName(channelName, &channel) )
	{
		NDS_INF("selected chanel %p", channel);
	}else
	{
		NDS_INF("Chanel %s is not found.", channelName.c_str());
	}
	return ndsSuccess;
}

ndsStatus ExADIOChannel::findChannelGroup(asynUser *pasynUser, const char *value, size_t maxChars, size_t *nActual)
{
	std::string name(value);
	nds::ChannelGroup* group=0;
	if ( ndsSuccess == _device->getChannelGroup(name, &group) )
	{
		NDS_INF("Selected channel group %p", group);
	}else
	{
		NDS_INF("Channel group %s is not found!", name.c_str() );
	}
	return ndsSuccess;
}

ndsStatus ExADIOChannel::setValueFloat64(asynUser* pasynUser, epicsFloat64 value)
{
	writeToOutput(value);
	return ndsSuccess;
}

void ExADIOChannel::destroy()
{
    NDS_DBG("ExADIOChannel::destroy %s.%d", suffix.c_str(), userIndex);

    if( this->getCurrentState() == nds::CHANNEL_STATE_PROCESSING )
    {
        this->stop();
    }

    if (fileName != NULL)
    {
        unlink (fileName);
        free(fileName);
        fileName = NULL;
    }
}

epicsTimerNotify::expireStatus ExADIOChannel::generateEvent(nds::TaskService &service, const epicsTime & currentTime)
{
	epicsInt32 value[2];

	// Calculating timestamp of the event.
	timestamp += 600;
	epicsTimeStamp stamp = (epicsTimeStamp)timestamp;

	value[0] = stamp.secPastEpoch;
	value[1] = stamp.nsec;

	// Updating event timestamp
	doCallbacksInt32Array(value, 2, idTimeStamp, _portAddr, stamp);

	// Generating Event
	++eventCounter;
	doCallbacksInt32(eventCounter, idEvent, _portAddr);

    epicsTimerNotify::expireStatus res(epicsTimerNotify::restart, 5.0);
    return res;
}

ndsStatus ExADIOChannel::setReason(asynUser* pasynUser, epicsInt32 value)
{
	try
	{
	    // Reason's arguments is accessible through DriverCommand class
		nds::DriverCommand *command = (nds::DriverCommand*)pasynUser->userData;
		std::cout << "Arg[0]: "<< command->Args[0] << std::endl;
	}catch(...)
	{
		NDS_ERR("Casting can't be done!" );
	}
	return ndsSuccess;
}

ndsStatus ExADIOChannel::onSwitchOn(nds::ChannelStates prevState,
        nds::ChannelStates currState)
{
    NDS_INF("ExADIOChannel::onSwitchOn");

    if (0 == _ClockFrequency )
    {
        NDS_ERR("Acquisition can't be start with Frequency=0");
        return ndsError;
    }

    bufferPosition = 0;
    ringBufferPosition = 0;
    /**
     * If triggerDelay<0, this is the position of the first value in ringBuffer
     * to be copied to  _BufferFloat32.
     */
    triggerPosition = 0;

    /// used to determin when to stop acquisition in the case of negative delay;
    stopAcquisition = false;

    state = STATE_NOT_TRIGGERED;

    /**
     * value of ringBuffer, size of ringBuffer and sampleDelay
     * are determined when trigger delay is specified.
     */
    ringBuffer = NULL;
    ringBufferSize = 0;
    sampleDelay = 0;

    /// marks the ringBufferPosition of the value that first broke the trigger condition
    stopTriggerPosition = 0;
    localTriggerDelay = 0;
    oldSampleDelay = 0;

    eventTimer->start(5.0);

    if (taskPeriodic)
    {
        taskPeriodic->start( epicsTime::getCurrent()+10, clockPeriod()/1.0e9 );
    }else
        NDS_DBG("Periodic task is not defined.");

    if (!_isOutput)
    {
        if(taskPolling)
        {
            /* Input channel: ensure file is opened. */
            if(fileFD == -1)
            {
                fileFD = open(fileName, O_RDONLY | O_NONBLOCK);
                NDS_DBG("Openning file: %s fd: %d", fileName, fileFD );
                if(fileFD == -1) {
                    NDS_ERR("Can't open '%s' for reading.", fileName);
                    return ndsError;
                }
            }
             if ( taskPolling->addFile(fileFD,  boost::bind(&ExADIOChannel::processReadBody, this, _1, _2)) == ndsSuccess )
                 taskPolling->start();
             else
                 return ndsError;
        }else
            NDS_DBG("Polling task is not defined.");
    }

    return ndsSuccess;
}

ndsStatus ExADIOChannel::onProcessingRequested(nds::ChannelStates prevState,
        nds::ChannelStates currState)
{
    NDS_INF("ExADIOChannel::onProcessingRequested");

    if ( clockPeriod() == 0 )
        return ndsError;

    NDS_INF("Period: %lld", clockPeriod());

    return createChannelPipeAndThreads();
}

ndsStatus ExADIOChannel::stopProcessing(nds::ChannelStates prevState,
        nds::ChannelStates currState)
{
    NDS_INF("ExADIOChannel::stopProcessing");

    eventTimer->cancel();
    if (!_isOutput)
    {
        if(taskPolling)
        {
            taskPolling->cancel();
        }else
            NDS_DBG("Polling task is not defined.");
    }

        if (taskPeriodic)
        {
            taskPeriodic->cancel();
        }else
            NDS_DBG("Periodic task is not defined.");

    return closeFiles();
}

ndsStatus ExADIOChannel::closeFiles()
{
    if (outFileHandle != NULL)
    {
        fclose(outFileHandle);
        outFileHandle = NULL;
    }

    if (fileFD != -1)
    {
        close (fileFD);
        fileFD = -1;
    }

    return ndsSuccess;
}
