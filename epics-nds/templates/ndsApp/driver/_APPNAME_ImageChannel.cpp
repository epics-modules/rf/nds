/*
 * exImageChannel.cpp
 *
 *  Created on: 22. avg. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#include "_APPNAME_ImageChannel.h"

ExImageChannel::ExImageChannel(const std::string& file,
		const std::string& pref,
		int idx)
#ifdef AREA_DETECTOR
	:NDArrayChannel(20)
#endif
{
  // Register available image formats
  addImageFormat("  32x32 8bits 1spp",  32,  32, 8);
  addImageFormat("  64x64 8bits 1spp",  64,  64, 8);
  addImageFormat("128x128 8bits 1spp", 128, 128, 8);

}

ExImageChannel::~ExImageChannel() {
	// TODO Auto-generated destructor stub
}

