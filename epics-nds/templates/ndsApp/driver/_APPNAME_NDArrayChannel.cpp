/*
 * exImageChannel.cpp
 *
 *  Created on: 22. avg. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#include <boost/filesystem.hpp>
#include "_APPNAME_NDArrayChannel.h"
#include "utsBufferTools.h"

ExNDArrayChannel::ExNDArrayChannel(int maxBuffers, size_t maxMemory)
	//Configuring NDArrayChannel
	:NDArrayChannel(maxBuffers, maxMemory
			,NDUInt16 // Raw data format (each pixel represented by the type)
			,2560 // Maximum X size
			,2160 // Maximum Y size
	)
{
  // Register available image formats
  addImageFormat("  32x32 8bits 1spp",  32,  32, 8);
  addImageFormat("  64x64 8bits 1spp",  64,  64, 8);
  addImageFormat("128x128 8bits 1spp", 128, 128, 8);
}

ExNDArrayChannel::~ExNDArrayChannel() {
	// TODO Auto-generated destructor stub
}

ndsStatus ExNDArrayChannel::setLoadFile(asynUser *pasynUser, const char *str, size_t numchars, size_t *nbytesTransfered)
{
  NDS_DBG("setLoadFile");

  *nbytesTransfered = numchars;

  if ( !boost::filesystem::exists(str) )
  {
	  NDS_ERR("File '%s' is not exists.", str);
	  return ndsError;
  }

  // Obtaining image and putting it to image buffer.
  // Raw data format should be set according actual data format is used for images.
  if ( 0 != loadRawFromFile(str, (void**)&_BufferInt8, &_BufferInt8Size) )
  {
	  NDS_ERR("File '%s' can't be loaded.", str);
	  return ndsError;
  }

  // Converting image according to requested parameters (DataFormats, ROI, and etc.)
  // and propagating it to all subscribers
  return propogateImage();
}
