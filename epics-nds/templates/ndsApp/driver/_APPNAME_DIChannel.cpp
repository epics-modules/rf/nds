/*
 * DIChannel.cpp
 *
 *  Created on: 19. jul. 2013
 *      Author: Slava Isaev
 */

#include "_APPNAME_DIChannel.h"

ExDIChannel::ExDIChannel():
    _digValue(0)
{
    registerOnEnterStateHandler(nds::CHANNEL_STATE_ON,
            boost::bind(&ExDIChannel::onSwitchOn, this, _1, _2) );
}

ExDIChannel::~ExDIChannel()
{

}

ndsStatus ExDIChannel::registerHandlers(nds::PVContainers* pvContainers)
{
    ADIOChannel::registerHandlers(pvContainers);

    NDS_PV_REGISTER_UINT32D("digital", &nds::Base::setUInt32Digital ,&nds::Base::getUInt32Digital, &_intIdDigital);

    char buff[255];
    snprintf(buff,255, "UpdateUInt32D_di_%d", _portAddr);

    _timer = nds::Timer::create(buff,
            boost::bind(&ExDIChannel::handleOnTimer, this, _1, _2) );

    return ndsSuccess;
}

epicsTimerNotify::expireStatus ExDIChannel::handleOnTimer(nds::TaskService &service, const epicsTime & currentTime)
{
    ++_digValue;
    doCallbacksUInt32Digital( _digValue, _intIdDigital, currentTime);

    epicsTimerNotify::expireStatus res(epicsTimerNotify::restart, 5.0);
    return res;
}

void ExDIChannel::destroy()
{
    _timer->cancel();
}

ndsStatus ExDIChannel::onSwitchOn(nds::ChannelStates prevState,
        nds::ChannelStates currState)
{
    _timer->start(5.0);

    return ndsSuccess;
}
