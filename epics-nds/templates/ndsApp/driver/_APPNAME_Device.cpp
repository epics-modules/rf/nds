/*
 * ExampleDevice.cpp
 *
 *  Created on: 26.07.2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#include <stdint.h>
#include <boost/filesystem.hpp>

#include "ndsDeviceStates.h"
#include "ndsManager.h"
#include "ndsChannelGroup.h"
#include "ndsChannel.h"

#include "_APPNAME_ADIOChannel.h"
#include "_APPNAME_DIChannel.h"
#include "_APPNAME_.h"
#include "_APPNAME_Device.h"
#include "_APPNAME_ChannelGroup.h"


#ifdef AREA_DETECTOR
#include "_APPNAME_NDArrayChannel.h"
#else
#include "_APPNAME_ImageChannel.h"
#endif


/** Registering new device specific driver
 *
 * This construction accepts 2 arguments:
 *  \lo Specific Device type;
 *  \lo Name of the driver by which it will be known in NDS Manager.
 * This driver name will be used in st.cmd file
 * ndsCreateDevice "testNDSDriver", "DAQDevice", "Parameter=Value"
 */
namespace
{
	// ExDevice      - is a Specific Device type
	// exampleDevice - is auxiliary variable
	// testNDSDriver - is a driver name with which this driver will be populate
	nds::RegisterDriver<ExDevice> reg_APPNAME_Device("_APPNAME_");
}

const char *CHANNEL_TYPE_SUFFIX[CHANNEL_TYPE_COUNT]=
{
        "ai",
        "ao",
        "di",
        "do",
        "dio",
        "c"
};

const char *CHANNEL_TYPE_PARAM_NAME[CHANNEL_TYPE_COUNT]=
{
        "N_AI",
        "N_AO",
        "N_DI",
        "N_DO",
        "N_DIO",
        "N_IMAGE"
};

ExDevice::ExDevice(const std::string& name):nds::Device(name)
,_counter(0)
,_timeRequestCounter(0)
{
	// C-tor of the Device object.
	// It is good place to register event handlers.

	// Registering state handlers...
	// See documentation to get state transfer allowance.

	// onRequest state handlers
	// this handler will be called when actor requests transfer to new state.
	// E.g. onSwitchOn handler will be called when actor calls on() member of the device.
	// this handler should return ndsSuccess to allow device go the ON state.
	// If this handler returns ndsError then device will go to the Error state.
	// (see documentation for details)
//	registerOnRequestStateHandler(
//			nds::DEVICE_STATE_INIT,   // Current state
//			nds::DEVICE_STATE_ON,  // Requested state
//			boost::bind(&ExDevice::onSwitchOn, this, _1, _2) );

	// If you would like to forbid transition from OFF state to ON state uncomment code below.
	// This default request handler returns ndsBlockTransition so device will stay in the same state (OFF).
//    registerOnRequestStateHandler(
//            nds::DEVICE_STATE_OFF,   // Current state
//            nds::DEVICE_STATE_ON,  // Requested state
//            boost::bind(&nds::Device::onWrongStateRequested, this, _1, _2) );


	// onLeave state handlers
	// E.g. onLeaveOn handler will be called when device lives ON state.
	// It means that all request handlers return ndsSuccess and device
	// will definitely leave ON state

	registerOnLeaveStateHandler(nds::DEVICE_STATE_OFF,
			boost::bind(&ExDevice::onLeaveOff, this, _1, _2 ) );

	// onEnter state Handlers
	//E.g. onError handler will be called when device enters to the ERROR state.
	// Note: ERROR state doesn't support request schema it means that transfer to the
	// ERROR state is unconditional

	// If all ChannelGrous should be switched ON automatically when device is switched ON
	// uncomment lines below
//    registerOnEnterStateHandler(nds::DEVICE_STATE_ON,
//            boost::bind(&nds::Device::switchChannelGroupsOn, this, _1, _2 ) );

    // If all ChannelGrous should be switched OFF automatically when device is switched OFF
    // uncomment lines below
//    registerOnEnterStateHandler(nds::DEVICE_STATE_OFF,
//            boost::bind(&nds::Device::switchChannelGroupsOff, this, _1, _2 ) );

	registerOnEnterStateHandler(nds::DEVICE_STATE_ERROR,
			boost::bind(&ExDevice::onError, this, _1, _2 ) );

	registerOnEnterStateHandler(nds::DEVICE_STATE_OFF,
			boost::bind(&ExDevice::onEnterOff, this, _1, _2 ) );

    registerOnEnterStateHandler(nds::DEVICE_STATE_INIT,
            boost::bind(&ExDevice::onEnterInit, this, _1, _2 ) );

    registerOnEnterStateHandler(nds::DEVICE_STATE_RESETTING,
            boost::bind(&ExDevice::onReset, this, _1, _2 ) );

    enableFastInit();

	// Registration of the handlers for the messaging mechanism
	registerMessageWriteHandler(
	    		"TEST2", /// Name of the message type which will be handled by this handler
	    		boost::bind(
	    				&ExDevice::messageHandler, /// Address of the handler
	    				this, 					   /// Object which owns the handler
	    				_1,_2)					   /// Stub functors (see boost::bind documentation for details.)
	);

	// Firmware update demonstration
	_firmwares[0]="2.2.0";
	_firmwares[1]="2.2.1";
	_firmwares[2]="2.2.2";
	_firmwares[3]="2.3.1";


	_hardwareRevision = "rev0";
	_model = "NI6529";
	_firmwareVersion = _firmwares[_counter];

    resetTask =  nds::ThreadTask::create( nds::TaskManager::generateName("Resetting"),
            epicsThreadGetStackSize(epicsThreadStackSmall),
            epicsThreadPriorityMedium,
            boost::bind(&ExDevice::resetProcess, this, _1)
    );

}

ndsStatus ExDevice::registerHandlers(nds::PVContainers* pvContainers)
{
	// Example of additional PV Record handlers' registration

	// IMPORTANT! It is important to call parent register function
	// to register all default handlers.
	nds::Device::registerHandlers(pvContainers);

	// Registration of additional PV reason handlers
	NDS_PV_REGISTER_OCTET(
			"Description",           // The reason. It is from the IN/OUT field of PV's record.
			&ExDevice::setOctet,   // Here is stub for the Octet write operation is used.
									 // This stub should be used when operation doesn't have write
									 // or read operation
			&ExDevice::readDescription, // Real read operation handler.
			&idDescription);           // Address of variable to store interrupt ID of the reason.
										// it used to dispathc interrupt though the
										// doCallbacks... functions.

	NDS_PV_REGISTER_INT32ARRAY(
			"Time",           		 // The reason. It is from the IN/OUT field of PV's record.
			&ExDevice::setInt32Array,   	 // Here is stub for the Octet write operation is used.
									 // This stub should be used when operation doesn't have write
									 // or read operation
			&ExDevice::getTime, // Real read operation handler.
			&idTime);           	 // Address of variable to store interrupt ID of the reason.
									 // it used to dispathc interrupt though the
									 // doCallbacks... functions.

	return ndsSuccess;
}

ExDevice::~ExDevice()
{
	// TODO Auto-generated destructor stub
}


ndsStatus ExDevice::createStructure(const char* portName, const char* params)
{
	// This is replacement for the ndspipeExampleInit function of the C NDS
	// All memory consuming or throws operation should be done
	// from this method.
	// This method should return ndsSuccess to notify NDS MANAGER that driver
	// structure created successfully and instance of the driver could be
	// registered with asyn.

    int channelType;
    int nChannels;
    int bufSize;

    // Device class parses input parameters for initialization line
    // st.cmd and stores it in protected member.
    // Device class has set of predefined functions to read specific
    // parameter and convert it to required type.
    // E.g. reading int value with default value equals 200
    bufSize = getIntParam("BUFSIZE", 200);

    // Getting string parameter file.
    std::string file = getStrParam("FILE", "");
    for(channelType = 0; channelType < 6; ++channelType)
    {
    	//Getting int parameter
        nChannels = getIntParam(CHANNEL_TYPE_PARAM_NAME[channelType], 0);
        if (nChannels > 0 )
        {
        	// Creating Channel Group object.
        	// NDS C++ requires Channel Group object.
        	// For this oject AsynPort will be created to support new
        	// NDS asyn addressing. (see documentation for details)
        	nds::ChannelGroup *channelGroup = new ExChannelGroup(CHANNEL_TYPE_SUFFIX[channelType]);

        	// All Channel groups should be registered.
			registerChannelGroup( channelGroup );
			for(int i=0; i<nChannels; ++i)
			{
				nds::Channel* channel;

				if (channelType == CHANNEL_TYPE_IMAGE)
				{
					// Creating image channel
#ifdef AREA_DETECTOR
					channel = new  ExNDArrayChannel( getIntParam( "MaxBuffers", 6),
													 getIntParam( "MaxMemory", 100000000) );
#else
					channel = new  ExImageChannel(file,
						CHANNEL_TYPE_SUFFIX[channelType],
						i);
#endif
				}else if(channelType == CHANNEL_TYPE_DI)
				{
                    // Creating general input output channel
//                    channel = new  ExDIChannel();

				    channel = new  ExADIOChannel(channelType,
                        file,
                        CHANNEL_TYPE_SUFFIX[channelType],
                        i,
                        bufSize);

				}else
				{
                    // Creating general input output channel
                    channel = new  ExADIOChannel(channelType,
                        file,
                        CHANNEL_TYPE_SUFFIX[channelType],
                        i,
                        bufSize);
				}

				//Registering channel in channel group
				channelGroup->registerChannel(channel);
			}

        }
    }

    return ndsSuccess;
}

void ExDevice::destroy()
{
    NDS_STK("exDevice::destroy");

	/// Deterministic destroy operation.
	/// Good place to put closing of device specific
	/// file handlers.
	/// Proper way is to switch OFF the device from here.
	/// Note: the same member will be called
	/// for each child object (ChannelGroups and Channels)
}

ndsStatus ExDevice::onEnterInit(nds::DeviceStates, nds::DeviceStates)
{
    NDS_INF("Device");

	// This is an example how to set records values from code asynchronously.
	int size = 40;
	// This is one way of buffer initialization for string propogation.
	// Other way is make this buffer a class filed or static.
	char* buff = (char*)malloc(size);
    int32_t version = 123;
	memset(buff, 0,  size);
	snprintf(buff, size, "%d", version);

	// Setting of software version string
	// Note EPCIS limits strings with 40 symbols
	// _interruptIdFirmware - is a specification which records should be updated.
	doCallbacksOctet( (char*) buff, size, ASYN_EOM_END, _interruptIdFirmware );
	free(buff);

	return ndsSuccess;
}

ndsStatus ExDevice::onEnterOff(nds::DeviceStates, nds::DeviceStates)
{
	NDS_TRC("onEnterOff");

    doCallbacksOctetStr(_firmwareVersion, ASYN_EOM_END, _interruptIdFirmware);
    doCallbacksOctetStr(_model, ASYN_EOM_END, _interruptIdModel);
    doCallbacksOctetStr(_softwareVersion, ASYN_EOM_END, _interruptIdSoftware);
    doCallbacksOctetStr(_hardwareRevision, ASYN_EOM_END, _interruptIdHardware);

    nds::Channel *channel;
    if (ndsSuccess == this->getChannelByName("AO1", &channel))
    {
        NDS_ERR("Channel AO%d is found.", channel->getChannelNumber() );
    }else
    {
        NDS_ERR("Channel AO1 is not found.");
    }

    if (ndsSuccess == this->getChannelByName("AI0", &channel))
    {
        NDS_ERR("Channel AI%d is found.", channel->getChannelNumber() );
    }else
    {
        NDS_ERR("Channel AI0 is not found.");
    }

	return ndsSuccess;
}

ndsStatus ExDevice::onError(nds::DeviceStates, nds::DeviceStates)
{
	// Good place to add error logging functionality.
	NDS_TRC("onError");
	return ndsSuccess;
}

ndsStatus ExDevice::onLeaveOff(nds::DeviceStates, nds::DeviceStates)
{
	NDS_TRC("onLeaveOff");
	return ndsSuccess;
}


ndsStatus ExDevice::readDescription (asynUser* pasynUser, char* buff, size_t size, size_t *read, int *end)
{
	// Example of the PV Record's handler
	return ndsSuccess;
}

ndsStatus ExDevice::messageHandler(asynUser* pasynUser, const nds::Message& msg)
{
	// Example of the message handler.
	// This handler receives TEST2 message from the MSGS record and dispatching it
	// to the message receive variable MSGR
	doCallbacksMessage(msg);
	return ndsSuccess;
}

ndsStatus ExDevice::getSoftwareVersion(asynUser *pasynUser, char *buf, size_t size, size_t *pcount, int *eomReason)
{
	// Example of the PV Record's handler
     NDS_STK("getSoftwareVersion");

     uint32_t version = 123456;
     snprintf(buf, size, "%02d.%02d.%02d", version / 100000, (version / 100) % 1000, version % 100);
     *pcount = 8;
     *eomReason = ASYN_EOM_END;

     return ndsSuccess;
}

ndsStatus ExDevice::updateFirmware(const std::string& module, const std::string& image, const std::string& method)
{
	NDS_INF( "Updating firmware.");
	NDS_INF( "Module to update: %s Current firmware version: %s ", module.c_str(), _firmwareVersion.c_str() );
	NDS_INF( "Image file: %s", image.c_str());

	_counter ++;
	_firmwareVersion = _firmwares[_counter];
	doCallbacksOctetStr(_firmwareVersion, ASYN_EOM_END, _interruptIdFirmware);

	if ( !boost::filesystem::exists(image) )
	{
		NDS_ERR( "Update procedure was not able to find : %s", image.c_str());
		return ndsError;
	}

	return ndsSuccess;
}

ndsStatus ExDevice::getTime(asynUser *pasynUser, epicsInt32 *value, size_t nelements, size_t *nIn)
{
	epicsTime time = epicsTime::getCurrent();
	epicsTimeStamp stamp = (epicsTimeStamp)time;
	pasynUser->timestamp = stamp;

	*nIn = 2;
	value[0] = stamp.secPastEpoch;
	value[1] = stamp.nsec;
	return ndsSuccess;
}

ndsStatus ExDevice::getPower(asynUser *pasynUser, epicsInt32* value)
{
    NDS_INF( "getPower is called.");
    *value = 0;
    if ( getCurrentState() == nds::DEVICE_STATE_IOC_INIT )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    return ndsSuccess;
}

ndsStatus ExDevice::setPower(asynUser  *pasynUser, epicsInt32 value)
{
    NDS_INF( "setPower is called : %d", value);
    return ndsSuccess;
}

ndsStatus ExDevice::onReset(nds::DeviceStates, nds::DeviceStates)
{
    NDS_INF("Start resetting.");
    resetTask->start();
    return ndsSuccess;
}

ndsStatus ExDevice::resetProcess(nds::TaskServiceBase &service)
{
    doCallbacksMessage("RESET",0,"Resetting in progress.");
    service.sleep(5.0);
    doCallbacksMessage("RESET",0,"Resetting complete.");
    getCurrentStateObj()->requestState(this, nds::DEVICE_STATE_ON);
    return ndsSuccess;
}


