# -------------------------------- [ Device ] --------------------------------
# Template file: _APPNAME_Device.template
#

record(mbbi, "$(PREFIX)") {
	field(DESC, "The state of the device.")
	field(DTYP, "asynInt32")
	field(INP, "@asyn($(ASYN_PORT), $(ASYN_ADDR))State")
	field(ZRVL, "0")
	field(ZRST, "UNKNOWN")
	field(ONVL, "1")
	field(ONST, "IOCINIT")
	field(TWVL, "2")
	field(TWST, "OFF")
	field(THVL, "3")
	field(THST, "INIT")
	field(FRVL, "4")
	field(FRST, "ON")
	field(FVVL, "5")
	field(FVST, "ERROR")
	field(SXVL, "6")
	field(SXST, "FAULT")	
	field(SVVL, "7")
	field(SVST, "RESETTING")	
	field(SCAN, "I/O Intr")
}

record(waveform, "$(PREFIX)-MSGS") {
	field(DESC, "Send message to device driver.")
	field(DTYP, "asynOctetWrite")
	field(INP, "@asyn($(ASYN_PORT), $(ASYN_ADDR))Command")
	field(FTVL, "UCHAR")
	field(NELM, "255")
}

record(waveform, "$(PREFIX)-MSGR") {
	field(DESC, "Receive message from device driver.")
	field(DTYP, "asynOctetRead")
	field(INP, "@asyn($(ASYN_PORT), $(ASYN_ADDR))Command")
	field(SCAN, "I/O Intr")	
	field(FTVL, "UCHAR")
	field(NELM, "255")
}

record(stringin, "$(PREFIX)-IMDL") {
	field(DESC, "Model of the device.")
	field(DTYP, "asynOctetRead")
	field(INP, "@asyn($(ASYN_PORT), $(ASYN_ADDR))Model")
	field(SCAN, "I/O Intr")	
}

record(stringin, "$(PREFIX)-ISN") {
	field(DESC, "Serial number of the device.")
	field(DTYP, "asynOctetRead")
	field(INP, "@asyn($(ASYN_PORT), $(ASYN_ADDR))Serial")
	field(SCAN, "I/O Intr")	
}

record(stringin, "$(PREFIX)-IHW") {
	field(DESC, "Hardware revision of the device.")
	field(DTYP, "asynOctetRead")
	field(INP, "@asyn($(ASYN_PORT), $(ASYN_ADDR))HardwareRevision")
	field(SCAN, "I/O Intr")	
}

record(stringin, "$(PREFIX)-IFW") {
	field(DESC, "Firmware version of the device.")
	field(DTYP, "asynOctetRead")
	field(INP, "@asyn($(ASYN_PORT), $(ASYN_ADDR))FirmwareVersion")
	field(SCAN, "I/O Intr")	
}

record(stringin, "$(PREFIX)-ISW") {
	field(DESC, "Software version of the device driver.")
	field(DTYP, "asynOctetRead")
	field(INP, "@asyn($(ASYN_PORT), $(ASYN_ADDR))SoftwareVersion")
	field(SCAN, "I/O Intr")	
}

record(waveform, "$(PREFIX)-FWUP") {
	field(DESC, "Link to the new firmware to load.")
	field(DTYP, "asynOctetWrite")
	field(INP, "@asyn($(ASYN_PORT), $(ASYN_ADDR))UpdateFirmware")
	field(FTVL, "UCHAR")
	field(NELM, "255")	
}

record(mbbi, "$(PREFIX)-PWR") {
	field(DESC, "The state of the device.")
	field(DTYP, "asynInt32")
	field(INP, "@asyn($(ASYN_PORT), $(ASYN_ADDR))Power")
	field(ZRVL, "0")
	field(ZRST, "OFF")
	field(ONVL, "1")
	field(ONST, "ON")
	field(SCAN, "I/O Intr")
}

record(mbbo, "$(PREFIX)-PWRS") {
	field(DESC, "The state of the device.")
	field(DTYP, "asynInt32")
	field(OUT, "@asyn($(ASYN_PORT), $(ASYN_ADDR))Power")
	field(ZRVL, "0")
	field(ZRST, "OFF")
	field(ONVL, "1")
	field(ONST, "ON")
	field(PINI, "YES")
	field(VAL, "1")
}

record(ai, "$(PREFIX)-PWRV") {
	field(DESC, "Power value.")
	field(DTYP, "asynFloat64")
	field(INP, "@asyn($(ASYN_PORT), $(ASYN_ADDR))PowerValue")
	field(SCAN, "Passive")
}

record(ai, "$(PREFIX)-TMP") {
	field(DESC, "Temperature.")
	field(DTYP, "asynFloat64")
	field(INP, "@asyn($(ASYN_PORT), $(ASYN_ADDR))Temperature")
	field(SCAN, "Passive")
}

record(mbbo, "$(PREFIX)-RST") {
	field(DESC, "Resetting device")
	field(DTYP, "asynInt32")
	field(OUT, "@asyn($(ASYN_PORT), $(ASYN_ADDR))Reset")
	field(ZRVL, "0")
	field(ZRST, "Do nothing")
	field(ONVL, "1")
	field(ONST, "Soft reset")
	field(TWVL, "2")
	field(TWST, "Master reset")
	field(THVL, "3")
	field(THST, "Power-cycle")
}

record(waveform, "$(PREFIX)-TIME") {
	field(DESC, "System time.")
	field(DTYP, "asynInt32ArrayIn")
	field(INP,  "@asyn($(ASYN_PORT), $(ASYN_ADDR))Time")
	field(FTVL, "ULONG")
	field(NELM, "2")
	field(SCAN, "1 second")	
	field(TSE, "-2")
}

record(mbbi, "$(PREFIX)-STS") {
	field(DESC, "The status of the device.")
	field(DTYP, "asynInt32")
	field(INP, "@asyn($(ASYN_PORT), $(ASYN_ADDR))ErrorCode")
	field(SCAN, "I/O Intr")	
	
	field(ZRVL, "0")
	field(ONVL, "1")
	field(TWVL, "2")
	field(THVL, "3")
	field(FRVL, "4")
	field(FVVL, "5")
	field(SXVL, "6")
	field(SVVL, "7")
	field(EIVL, "8")
	field(NIVL, "9")
	field(TEVL, "10")
	field(ELVL, "11")
	field(TVVL, "12")
	field(TTVL, "13")
	field(FTVL, "14")
	field(FFVL, "15")
	field(ZRST, "No error")
	field(ONST, "Initializing")
	field(TWST, "Resetting")
	field(THST, "PBIT test failure")
	field(FRST, "No board detected")
	field(FVST, "Static cfg. error")
	field(SXST, "Dynamic cfg. error")
	field(SVST, "Reserved")
	field(EIST, "Reserved")
	field(NIST, "Reserved")
	field(TEST, "Reserved")
	field(ELST, "Reserved")
	field(TVST, "Reserved")
	field(TTST, "Reserved")
	field(FTST, "Reserved")
	field(FFST, "Miscellaneous errors")
}

record(bo, "$(PREFIX)-ENBL")
{
	field(DTYP, "asynInt32")
	field(OUT,"@asyn( $(ASYN_PORT), $(ASYN_ADDR))Enabled")
	field(ZNAM	, "DISABLED")
	field(ONAM	, "ENABLED")
	field(VAL,  "$(ENABLED)")
	field(PINI, "YES")
}
