/*
 * ndsPollingThread.h
 *
 *  Created on: 23.08.2013
 *      Author: Slava
 */

#ifndef NDSPOLLINGTHREAD_H_
#define NDSPOLLINGTHREAD_H_

#include <map>
#include <boost/function.hpp>

#include "ndsTypes.h"
#include "ndsFileDescriptor.h"
#include "ndsBaseTask.h"
#include "ndsThread.h"

namespace nds
{

typedef std::map<int, FileDescriptor*> FDContainer;

class PollingTask : public Thread
{
private:
    PollingTask();
	PollingTask(const PollingTask&);
	PollingTask* operator =(const PollingTask&);

protected:
    int _fdCancelEvent;
    int _fdCancelAckEvent;
    int _epollFD;

    struct epoll_event _eventCancel;
    struct epoll_event _eventCancelAck;

    FDContainer _events;

    virtual ndsStatus cancelPrv();

    void createPollFD();

    PollingTask(const std::string& name,
            unsigned int priority,
            unsigned int stackSize);
public:

    static PollingTask* create(
            const std::string& name,
            unsigned int priority,
            unsigned int stackSize);

    ~PollingTask();

    /// Common initialization.
    void init();

    ndsStatus addFile(int fileFd, FileEventHandler, uint32_t events = EPOLLIN);

    virtual void run(TaskServiceBase& service);
};


} /* namespace nds */
#endif /* NDSPOLLINGTHREAD_H_ */
