/*
 * ndsTaskService.cpp
 *
 *  Created on: 27. avg. 2013
 *      Author: Slava Isaev
 */

#include "ndsTaskService.h"

namespace nds
{

TaskService::TaskService():
        _cancelled(false)
{

}

volatile bool TaskService::isCancelled() const
{
    return _cancelled;
}

WaitStatus TaskService::sleep(double seconds)
{
    return (_event.wait(seconds))? ndsWaitStatusEvent : ndsWaitStatusTimeout;
}

void TaskService::cancel()
{
    _cancelled = true;
    _event.signal();
}

epicsTime TaskService::currentTime()
{
    return epicsTime::getCurrent();
}

void TaskService::restore()
{
    _cancelled = false;
}


} /* namespace nds */
