/*
 * ndsFileDescriptor.h
 *
 *  Created on: 23.08.2013
 *      Author: Slava
 */

#ifndef NDSFILEDESCRIPTOR_H_
#define NDSFILEDESCRIPTOR_H_

#include <sys/epoll.h>
#include <boost/function.hpp>

#include "ndsTaskService.h"

namespace nds
{

typedef boost::function<void (TaskServiceBase&, const struct epoll_event&)> FileEventHandler;

class FileDescriptor
{
protected:
    TaskServiceBase &_service;
    int _fd;
    FileEventHandler _handler;
    struct epoll_event _epollEvent;


    void init();
public:
    FileDescriptor(TaskServiceBase&_service, int fd, uint32_t events, FileEventHandler&);
    virtual ~FileDescriptor();
    struct epoll_event* getEpollEvent();
    int getFD();
    void notify(const struct epoll_event&);
};


} /* namespace nds */
#endif /* NDSFILEDESCRIPTOR_H_ */
