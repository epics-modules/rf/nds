/*
 * ndsTrigger.cpp
 *
 *  Created on: 15. mar. 2013
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#include "ndsDevice.h"

#include "ndsTrigger.h"
#include "ndsTriggerCondition.h"

namespace nds
{

Trigger::~Trigger()
{
	/// deallocate the memory pointed to by elements of vector condition
	cleanUp();
}

bool Trigger::checkTrigger() const
{
	/**
	 * For each element of trigger condition vector, call verifyTriggerCondition.
	 * If any condition is not met, set _isTrigger=0 and break from loop.
	 * If string is empty, return true.
	 */
	int _isTriggered;
	if(emptyString){
		_isTriggered = 1;
	} else{
		_isTriggered = 1;
		unsigned int i = 0;
		/// TO DO: use iterator
		while(i < condition.size()){
			if(!condition[i]->verifyTriggerCondition()){
				_isTriggered = 0;
				break;
			}
			i++;
		}
	}
	return _isTriggered;
}


ndsStatus Trigger::parseTriggerCondition(const char* data){
	///	Enumerating states of the parser state machine
	enum states {
		STATE_INIT,
		STATE_CHANNEL_ID,
		STATE_OPERATION,
		STATE_SIGNUM,
		STATE_VALUE_INTEGER,
		STATE_VALUE_FRACTION
	};

	if ( !_device )
	    return ndsError;

	/// Remove the parsing results of the previous trigger condition;
	cleanUp();
	condition.clear();

	/// pointer to parsing results of individual trigger conditions
	ITriggerCondition* current = NULL;


	/// ParseTriggerCondition will return success unless something goes wrong during the parsing.
	ndsStatus res = ndsSuccess;

	/// Arguments of function setValue, used to store the value of the current trigger condition.
	epicsFloat64 parseValueFloat = 0;
	epicsInt32 parseValueInt = 0;

	int i = 0; /// Used to fill the channel_id strings
	int n = 10; /// Used to calculate fractional part of the value

	/**
	 * Condition can begin with a channel_id or negation. To avoid allocating memory in
	 * both cases, we introduce additional boolean variable which is set to true,
	 * if condition starts with negation
	 */
	bool currentNegated = false;

	/// if string is empty, channel is triggered
	emptyString = true;

	/// data0 is used to calculate syntax error position in condition string
	const char* data0 = data;

	int state = STATE_INIT;
	while((*data != 0) && (res == ndsSuccess)){
		switch (state){
			case STATE_INIT:
				if (((*data >= 'a')&&(*data <= 'z')) ||
					((*data >= 'A')&&(*data <= 'Z')))
				{
					/// at the begining of a new trigger condition, check if vector condition is full
					if(condition.size() < condition.capacity()){
						/// allocate memory for the next trigger condition
						current = new TriggerCondition<epicsFloat64>();
						/// string is clearly not empty
						emptyString = false;
						current->negated = currentNegated;
						currentNegated = false;
						state = STATE_CHANNEL_ID;
					} else {
						res = ndsError;
						NDS_ERR("Too many trigger conditions. Maximum number of conditions is %d", (int)condition.capacity());
						/// free allocated memory and clear vector condition of previous, valid conditions
						cleanUp();
						condition.clear();
					}
				} else if ((*data == '!') && (!currentNegated)){
					/// Only one negation per condition is allowed
					currentNegated = true;
					emptyString = false;
					data++;
					if(*data == 0){
						res = ndsError;
						NDS_ERR("Error at character number %ld, negation must be followed by a channel name.\n",data-data0+1);
						/// clear vector condition of previous, valid conditions
						cleanUp();
						condition.clear();
					}
				} else if ((*data == ' ') || (*data == '	')) {
					data++;
				} else {
					res = ndsError;
					emptyString = false;
					NDS_ERR("Error at character number %ld, %c received where channel id is expected.\n",data-data0+1, *data);
					/// clear vector condition of previous, valid conditions
					cleanUp();
					condition.clear();
				}
				break;

			case STATE_CHANNEL_ID:
				if (((*data >= '0') && (*data <= '9')) ||
					((*data >= 'a') && (*data <= 'z')) ||
					((*data >= 'A') && (*data <= 'Z')) )
				{
					current->channel_id[i] = *data;
					/// set the next value in array to 0
					i++;
					current->channel_id[i] = 0;
					data++;
				} else if ((*data == '<') || (*data == '>') || (*data == '\\') || (*data == '/')){
					state = STATE_OPERATION;
				} else{
					res = ndsError;
					NDS_ERR("Error at character number %ld, %c received where channel id, operation or space are expected.\n",data-data0+1, *data);
					/// free allocated memory and clear vector condition of previous, valid conditions
					delete current;
					cleanUp();
					condition.clear();
				}
				/**
				 * If channel_id isn't followed by an operator, channel is considered
				 * to be digital. Push back the current condition in the vector
				 */
				if ((*data==' ') || (*data=='	') || (*data == 0)){
					/// Identify channel and store a pointer to it in pchannel
					if ( ndsSuccess != _device -> getChannelByName(current->channel_id, &current->pchannel) )
					{
					    NDS_ERR("Can't find cahnnel: '%s'", current->channel_id);
					    return ndsError;
					}
					condition.push_back(current);
					/// reset position in channel_id string and go to state_init
					i=0;
					state=STATE_INIT;
				}
				break;

			case STATE_OPERATION:
				/*Only one operator per condition is allowed, except the combination /\ */
				if(*data == '<'){
					current->operation = NDS_TC_OP_LT;
					data++;
					state = STATE_VALUE_INTEGER;
				} else if(*data == '>'){
					current->operation = NDS_TC_OP_GT;
					data++;
					state = STATE_VALUE_INTEGER;
				} else if(*data == '/'){
					current->operation = NDS_TC_OP_RISING;
					data++;
					/// if rising edge is followed by falling edge, trigger on rising and falling edge
					if(*data == '\\'){
						current->operation = NDS_TC_OP_RISING_FALLING;
						data++;
					}
					state = STATE_VALUE_INTEGER;
				} else if(*data == '\\'){
					current->operation = NDS_TC_OP_FALLING;
					data++;
					state = STATE_VALUE_INTEGER;
				}

				if((*data >= '0') && (*data <= '9')){
					state = STATE_VALUE_INTEGER;
				} else if (*data == '-'){
					current->minus = true;
					state = STATE_SIGNUM;
				} else if (*data == '+'){
					state = STATE_SIGNUM;
				} else {
					res = ndsError;
					NDS_ERR("Error at character %ld, operator must be followed by value.\n",data-data0+1);
					/// free allocated memory and clear vector condition of previous, valid conditions
					delete current;
					cleanUp();
					condition.clear();
				}
				break;

			case STATE_SIGNUM:
				/// Sign must be followed by a value, else error.
				data++;
				if((*data >= '0') && (*data <= '9')){
					state = STATE_VALUE_INTEGER;
				} else {
					res = ndsError;
					NDS_ERR("Error at character %ld, sign must be followed by a value.\n",data-data0+1);
					/// free allocated memory and clear vector condition of previous, valid conditions
					delete current;
					cleanUp();
					condition.clear();

				}
				break;

			case STATE_VALUE_INTEGER:
				if((*data >= '0') && (*data <= '9')){
					parseValueFloat = parseValueFloat*10 + (int)(*data-'0');
					data++;
				} else if(*data == '.'){
					state = STATE_VALUE_FRACTION;
					data++;
				} else{
					res = ndsError;
					NDS_ERR("Error at character %ld, %c received where value, decimal point or space are expected.\n",data-data0+1, *data);
					/// free allocated memory and clear vector condition of previous, valid conditions
					delete current;
					cleanUp();
					condition.clear();
				}

				 /// If value has no fractional part, put the current condition in the vector
				if ((*data == ' ') || (*data == '	') || (*data == 0)){
					if(current->minus){
						parseValueFloat = (-1)*parseValueFloat;
					}
					/// cast the value to integer and store it in current.
					parseValueInt = (epicsInt32)parseValueFloat;
					current->setValue(parseValueInt);
					/// reset the additional values
					parseValueFloat = 0;
					parseValueInt = 0;
					/// identify the trigger and store its addres in pchannel
					if ( ndsSuccess != _device -> getChannelByName(current->channel_id, &current->pchannel) )
					{
                        NDS_ERR("Can't find cahnnel: '%s'", current->channel_id);
					    return ndsError;
					}

					condition.push_back(current);
					/// reset position in channel_id string and return to state init
					i = 0;
					state = STATE_INIT;
				}
				break;

			case STATE_VALUE_FRACTION:
				if((*data >= '0') && (*data <= '9')){
					parseValueFloat = parseValueFloat+float(*data-'0')/n;
					n*=10;
					data++;
				} else {
					res = ndsError;
					NDS_ERR("Error at character %ld, %c received where value or space are expected.\n",data-data0+1, *data);
					/// free allocated memory and clear vector condition of previous, valid conditions
					delete current;
					cleanUp();
					condition.clear();
				}
				/// If calculation of value is complete, push_back the current condition
				if ((*data == ' ') || (*data == '	') || (*data == 0)){
					if(current->minus){
						parseValueFloat = (-1)*parseValueFloat;
					}

					/// set the value of the current trigger condition
					current->setValue(parseValueFloat);
					/// reset the additional value
					parseValueFloat = 0;
					/// identify the trigger and store it's addres in pchannel
					if (ndsSuccess != _device -> getChannelByName(current->channel_id, &current->pchannel) )
					{
                        NDS_ERR("Can't find cahnnel: '%s'", current->channel_id);
					    return ndsError;
					}
					condition.push_back(current);
					/// reset position in channel_id and denominator and return to STATE_INIT
					n = 10;
					i = 0;
					state = STATE_INIT;
				}
				break;
		}
	}

	return res;
}


void Trigger::cleanUp(){
	for (ConditionVector::iterator itr = condition.begin() ; itr != condition.end(); ++itr){
		delete *itr;
	}
}

void Trigger::setDevice(nds::Device *dev) {
	_device = dev;
}

Trigger::Trigger(nds::Device* device): _device(device)
{
	/// Setting the capacity of condition vector which stores the trigger conditions.
	condition.reserve(100);
}

ITriggerCondition* Trigger::getFirst() const
{
    return condition.front();
}

bool Trigger::isEmpty() const
{
    return condition.empty();
}


} /* namespace nds */
