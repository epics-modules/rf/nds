/*
 * ndsIOC.c
 *
 *  Created on: 26.07.2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdint.h>

#include <signal.h>

/* epics */
#include <cantProceed.h>
#include <iocsh.h>
#include <epicsExport.h>
#include <epicsThread.h>
#include <epicsString.h>
#include <epicsExit.h>

/* asyn */
#include <asynDriver.h>

#include "ndsDebug.h"
#include "ndsManager.h"
#include "ndsTaskManager.h"
#include "ndsIOC.h"

epicsShareFunc void epicsInitHookFunction(initHookState state)
{
	nds::Manager::getInstance().epicsInit(state);
}

/**
 * \brief Exit handler which executes when the process is shutting down.
 *
 * The exit handler invokes the closeDevice method of all devices created
 * with ndsCreateDevice.
 */

epicsShareFunc void ndsSigIntHandler(int signum)
{
    epicsExit(signum);
}

epicsShareFunc void ndsAtExit(void *param)
{
	nds::Manager::getInstance().ndsAtExit(param);
}

epicsShareFunc int ndsCreateDevice(
    const char *driverName,
    const char *portNameParam,
    const char *params)
{
	return (int) nds::Manager::getInstance().createDevice(
			driverName,
		    portNameParam,
		    params);
}

/* ----------------------- [ IOC Shell Functions ] ----------------------- */

static const iocshArg ndsCreateDeviceArg0 = { "driverName", iocshArgString }; ///< Driver name defined in startup script.
static const iocshArg ndsCreateDeviceArg1 = { "portName", iocshArgString }; ///< Port name defined in startup script.
static const iocshArg ndsCreateDeviceArg2 = { "params", iocshArgString }; ///< Parameters defined in startup script.
static const iocshArg *ndsCreateDeviceArgs[] = {
    &ndsCreateDeviceArg0, &ndsCreateDeviceArg1, &ndsCreateDeviceArg2}; ///< Structure of IOC Shell parameters.
static const iocshFuncDef ndsCreateDeviceFuncDef = {
    "ndsCreateDevice", 3, ndsCreateDeviceArgs }; ///< Structure for IOC Shell function definition.

/**
 * \brief IOC Shell initialization function.
 */
epicsShareFunc void ndsCreateDeviceIocsh(const iocshArgBuf *args)
{
    ndsCreateDevice(args[0].sval, args[1].sval, args[2].sval);
}

static const iocshArg ndsSetTraceLevelArg0 = { "traceLevel", iocshArgInt }; ///< Driver name defined in startup script.
static const iocshArg *ndsSetTraceLevelArgs[] = {
    &ndsSetTraceLevelArg0}; ///< Structure of IOC Shell parameters.
static const iocshFuncDef ndsSetTraceLevelFuncDef = {
    "ndsSetTraceLevel", 1, ndsSetTraceLevelArgs }; ///< Structure for IOC Shell function definition.

/**
 * \brief IOC Shell function to set trace level.
 */
epicsShareFunc void ndsSetTraceLevelIocsh(const iocshArgBuf *args)
{
    utsSetTraceLevel(args[0].ival);
}

static const iocshArg *ndsListDriversArgs[] = {}; ///< Structure of IOC Shell parameters.
static const iocshFuncDef ndsListDriversFuncDef = {
    "ndsListDrivers", 0, ndsListDriversArgs }; ///< Structure for IOC Shell function definition.

/**
 * \brief IOC Shell function to set trace level.
 */
epicsShareFunc void ndsListDriversIocsh(const iocshArgBuf *args)
{
    nds::Manager::getInstance().listDrivers();
}

static const iocshArg *ndsListDevicesArgs[] = {}; ///< Structure of IOC Shell parameters.
static const iocshFuncDef ndsListDevicesFuncDef = {
    "ndsListDevices", 0, ndsListDevicesArgs }; ///< Structure for IOC Shell function definition.

/**
 * \brief IOC Shell function to set trace level.
 */
epicsShareFunc void ndsListDevicesIocsh(const iocshArgBuf *args)
{
    nds::Manager::getInstance().listDevices();
}


static const iocshArg *ndsTraceLevelsArgs[] = {}; ///< Structure of IOC Shell parameters.
static const iocshFuncDef ndsTraceLevelsFuncDef = {
    "ndsTraceLevels", 0, ndsTraceLevelsArgs }; ///< Structure for IOC Shell function definition.

/**
 * \brief IOC Shell function to set trace level.
 */
epicsShareFunc void ndsTraceLevelsIocsh(const iocshArgBuf *args)
{
    int i=1;
    for (;i<8; ++i)
    {
        printf("%d: %s\n", i, strDebugLevels[i] );
    }
}

static const iocshArg *ndsListTasksArgs[] = {}; ///< Structure of IOC Shell parameters.
static const iocshFuncDef ndsListTasksFuncDef = {
    "ndsListTasks", 0, ndsListTasksArgs }; ///< Structure for IOC Shell function definition.

/**
 * \brief IOC Shell function to set trace level.
 */
epicsShareFunc void ndsListTasksIocsh(const iocshArgBuf *args)
{
    nds::TaskManager::getInstance().listTasks();
}


static const iocshArg ndsStartTaskArg0 = { "taskName", iocshArgString }; ///< Task name
static const iocshArg *ndsStartTaskArgs[] = {&ndsStartTaskArg0}; ///< Structure of IOC Shell parameters.
static const iocshFuncDef ndsStartTaskFuncDef = {
    "ndsStartTask", 1, ndsStartTaskArgs }; ///< Structure for IOC Shell function definition.

/**
 * \brief IOC Shell function to set trace level.
 */
epicsShareFunc void ndsStartTaskIocsh(const iocshArgBuf *args)
{
    nds::TaskManager::getInstance().startTask(args[0].sval);
}

static const iocshArg ndsCancelTaskArg0 = { "taskName", iocshArgString }; ///< Task name
static const iocshArg *ndsCancelTaskArgs[] = {&ndsCancelTaskArg0}; ///< Structure of IOC Shell parameters.
static const iocshFuncDef ndsCancelTaskFuncDef = {
    "ndsCancelTask", 1, ndsCancelTaskArgs }; ///< Structure for IOC Shell function definition.

/**
 * \brief IOC Shell function to set trace level.
 */
epicsShareFunc void ndsCancelTaskIocsh(const iocshArgBuf *args)
{
    nds::TaskManager::getInstance().cancelTask(args[0].sval);
}

static const iocshArg ndsPrintStructureArg0 = { "DeviceName", iocshArgString }; ///< Task name
static const iocshArg *ndsPrintStructureArgs[] = {&ndsPrintStructureArg0}; ///< Structure of IOC Shell parameters.
static const iocshFuncDef ndsPrintStructureFuncDef = {
    "ndsPrintStructure", 1, ndsPrintStructureArgs }; ///< Structure for IOC Shell function definition.

/**
 * \brief IOC Shell function to set trace level.
 */
epicsShareFunc void ndsPrintStructureIocsh(const iocshArgBuf *args)
{
    if (args[0].sval)
    {
        nds::Manager::getInstance().printDeviceStructure(args[0].sval);
    }
    else
    {
        printf("Device name is not specified. Use ndsListDevices.\n" );
    }
}

epicsShareFunc void ndsRegister (void)
{
  static int firstTime = 1;
  if (!firstTime)
	  return ;
  firstTime = 0;

  epicsAtExit(ndsAtExit, NULL);
  initHookRegister(epicsInitHookFunction);
  iocshRegister(&ndsCreateDeviceFuncDef,  ndsCreateDeviceIocsh);
  iocshRegister(&ndsSetTraceLevelFuncDef, ndsSetTraceLevelIocsh);
  iocshRegister(&ndsListDriversFuncDef,   ndsListDriversIocsh);
  iocshRegister(&ndsListDevicesFuncDef,   ndsListDevicesIocsh);
  iocshRegister(&ndsTraceLevelsFuncDef,   ndsTraceLevelsIocsh);
  iocshRegister(&ndsListTasksFuncDef,     ndsListTasksIocsh);
  iocshRegister(&ndsStartTaskFuncDef,     ndsStartTaskIocsh);
  iocshRegister(&ndsCancelTaskFuncDef,    ndsCancelTaskIocsh);
  iocshRegister(&ndsPrintStructureFuncDef,    ndsPrintStructureIocsh);

  signal(SIGINT, ndsSigIntHandler);
}

epicsExportRegistrar (ndsRegister);

