/*
 * ndsTypes.h
 *
 *  Created on: 22.07.2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDSTYPES_H_
#define NDSTYPES_H_

#include <stddef.h>
#include <shareLib.h>
#include <cantProceed.h>
#include <epicsTime.h>
#include <epicsTypes.h>
#include <epicsExport.h>
#include <asynStandardInterfaces.h>

#define NDS_VERSION "2.3"

#define NDS_CALLOC(object, count, size) object = callocMustSucceed(count, size, "Unable to allocate memory for " #object)

/// NDS statuses
typedef enum
{
    ndsSuccess   = asynSuccess
    ,ndsTimeout  = asynTimeout
    ,ndsOverflow = asynOverflow
    ,ndsError    = asynError
    ,ndsDisconnected = asynDisconnected
    ,ndsDisabled     = asynDisabled

    ,ndsBlockTransition = asynDisabled + 1 ///< It is reserved for onRequestState handlers. In this case instance will stay in the same state.
    ,ndsDefunct                            ///<  Specifies that object should go to degraded state.
    ,ndsFault   = ndsDefunct
} ndsStatus;

typedef int reason_type;


#endif /* NDSTYPES_H_ */
