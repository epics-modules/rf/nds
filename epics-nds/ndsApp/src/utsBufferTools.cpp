/*
 * utsBufferTools.cpp
 *
 *  Created on: 6. dec. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#include "utsBufferTools.h"

int saveRawToFile(const char* filename, const void* data, size_t  elemSize, size_t elemNumber )
{
	FILE* f;
	f=fopen(filename,"w");
	if(f == NULL)
   {
      return -1;
    }
	fwrite( (void*)data, elemSize, elemNumber, f );
	fclose(f);
	return 0;
}

int loadRawFromFile(const char* filename, void** data, size_t* size )
{
	FILE * pFile;
	size_t lSize;
	size_t result;

	*size = 0;

	pFile = fopen ( filename , "rb" );
	if (pFile==NULL)
	{
	  return -1;
	}

	// obtain file size:
	  fseek (pFile , 0 , SEEK_END);
	  lSize = ftell (pFile);
	  rewind (pFile);

	  // allocate memory to contain the whole file:
	  *data = (void*) malloc (sizeof(char)*lSize);
	  if (*data == NULL)
	  {
		  return -2;
	  }

	  // copy the file into the buffer:
	  result = fread (*data,1,lSize,pFile);
	  if (result != lSize)
	  {
		  return -3;
	  }

	  *size = lSize;
	  // terminate
	  fclose (pFile);
	  return 0;
}
