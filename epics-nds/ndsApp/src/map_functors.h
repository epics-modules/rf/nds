/*
 * map_functors.h
 *
 *  Created on: 30. avg. 2012
 *      Author: Slava Isaev (C) Cosylab. Slovenia.
 */

#ifndef IVA_MAP_FUNCTORS_H_
#define IVA_MAP_FUNCTORS_H_

#include <map>
#include <algorithm>
#include <sstream>

/// @cond NDS_INTERNALS

template<typename T>
class PrintPairToStr
{
	std::stringstream _os;
	bool _first;
	std::string _delimeter;
public:
	PrintPairToStr(const std::string& delimeter):
		_os(std::stringstream::in | std::stringstream::out),
		_first(true),
		_delimeter(delimeter)
	{ }

	void operator()(const T& obj)
	{
		if(_first)
			_first = false;
		else
			_os << _delimeter;
		_os << obj.first<<"="<<obj.second;
	}

	const std::string& str()
	{
		return _os.str();
	}
};

template<typename T>
class PrintPairToStream
{
	std::ostream& _os;
	bool _first;
	std::string _delimeter;
public:
	PrintPairToStream(std::ostream& os, const std::string& delimeter):
		_os(os), _first(true), _delimeter(delimeter)
	{ }

	void operator()(const T& obj)
	{
		if(_first)
			_first = false;
		else
			_os << _delimeter;
		_os << obj.first<<"="<<obj.second;
	}
};

template<typename T>
class PrintMapToStr
{
	typedef T map_type;
	typedef typename T::value_type value_type;
	std::stringstream _os;
	bool _first;
	std::string _delimeter;
public:
	PrintMapToStr(const map_type& values, const std::string& delimeter):
		_os(std::stringstream::in | std::stringstream::out),
		_first(true),
		_delimeter(delimeter)
	{
	    PrintPairToStream<value_type> printer(_os, ",");
	    std::for_each(values.begin(),
	    		values.end(),
	    		printer
	    );
	}

	const std::string str()
	{
		return _os.str();
	}
};


template<typename T>
class PrintMapTo
{
	typedef T map_type;
	typedef typename T::value_type value_type;
	std::ostream _os;
	bool _first;
	std::string _delimeter;
public:
	PrintMapTo(std::ostream& os,const map_type& values, const std::string& delimeter):
		_os( os ),
		_first(true),
		_delimeter(delimeter)
	{
	    PrintPairToStream<value_type> printer(_os, _delimeter);
	    std::for_each(values.begin(),
	    		values.end(),
	    		printer
	    );
	}
};

template<typename T>
void printStrKey(const T& obj)
{
    printf("%s\n", obj.first.c_str() );
};

/*
 * Functor to delete
 */
template <class T>
void deleteMapObj(T& obj)
{
    if (obj.second)
        delete obj.second;
	obj.second = 0;
}

/// @endcond

#endif /* MAP_FUNCTORS_H_ */
