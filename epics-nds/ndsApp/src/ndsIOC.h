/*
 * ndsIOC.h
 *
 *  Created on: 26.07.2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDSIOC_H_
#define NDSIOC_H_

#include <initHooks.h>

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

/// IOC specific function to handle exit signal
epicsShareFunc void ndsAtExit(void *param);

/// IOC specific function to handle initialization signals
epicsShareFunc void epicsInitHookFunction(initHookState state);

/// IOC specific function to handle initialization signals
epicsShareFunc int  ndsCreateDevice(  const char *driverName, const char *portNameParam, const char *params);

/// IOC specific functions registrator
epicsShareFunc void ndsRegister (void);

/// IOC specific function
epicsShareFunc void ndsCreateDeviceIocsh(const iocshArgBuf *args);


#ifdef __cplusplus
}
#endif  /* __cplusplus */


#endif /* NDSIOC_H_ */
