/*
 * ndsMessaging.cpp
 *
 *  Created on: 28. avg. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#include <macLib.h>

#include "ndsMessage.h"
#include "map_functors.h"
using namespace nds;

ndsStatus Message::load(const std::string& msg)
{
	if (msg.length()<=0)
		return ndsError;

	int idx = msg.find(" ");
	if (-1 == idx)
	{
		messageType = msg;
		return ndsSuccess;
	}

	messageType = msg.substr (0,idx);

	MAC_HANDLE *macHandle; ///< Handle for reading the parameters.
	char **macPairs; 		///< Parameters read from st.cmd file.
	long nParams; 			///< Number of read parameters.

	macHandle = NULL;

	if(macCreateHandle(&macHandle, NULL))
		return ndsError;

	nParams = macParseDefns(
	    		macHandle,
	    		msg.substr(idx+1).c_str(),
	    		&macPairs);

	for(int i = 2 * nParams - 2; i >= 0; i -= 2)
	{
		parameters.insert(
				MessageParameters::value_type(macPairs[i], macPairs[i + 1])
		);
	}

	return ndsSuccess;
}

ndsStatus Message::load(const char *data, size_t numchars, size_t *nbytesTransfered)
{
	if (!data || numchars<=0 )
		return ndsError;
	std::string in(data);
	*nbytesTransfered = (numchars > in.length())?numchars:in.length();

	return load(in);
}

const std::string Message::str() const
{

    std::stringstream ss (std::stringstream::in | std::stringstream::out);
    ss << messageType << " ";
    PrintPairToStream<MessageParameters::value_type> params(ss,",");

    std::for_each(parameters.begin(),
       			parameters.end(),
    	params
    );

	return ss.str();
}

void Message::insert(const std::string& name, const std::string& value)
{
	parameters.insert(
			MessageParameters::value_type(name, value)
	);
}

const std::string& Message::getStrParam(const std::string& name, const std::string& def) const
{
  MessageParameters::const_iterator itr =  parameters.find(name);
  if ( parameters.end() == itr )
          return def;
  return itr->second;
}

int Message::getIntParam(const std::string& name, int def) const
{
  MessageParameters::const_iterator itr =  parameters.find(name);
  if ( parameters.end() == itr )
          return def;
  return atoi(itr->second.c_str());
}

