/*
 * ndsTriggerCondition.cpp
 *
 *  Created on: 15. mar. 2013
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#include "ndsTriggerCondition.h"

namespace nds
{

ITriggerCondition::ITriggerCondition()
{
	channel_id[0] = 0;
	negated = false;
	minus=false;
	operation = NDS_TC_OP_NONE;
}

std::string ITriggerCondition::getChannelId()
{
    return channel_id;
}

bool ITriggerCondition::isNegated()
{
    return negated;
}

bool ITriggerCondition::isMinus()
{
    return minus;
}

Operations ITriggerCondition::getOperation()
{
    return operation;
}

nds::Channel* ITriggerCondition::getChannel()
{
    return pchannel;
}


} /* namespace nds */
