/*
 * ndsTriggerCondition.h
 *
 *  Created on: 15. mar. 2013
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDSTRIGGERCONDITION_H_
#define NDSTRIGGERCONDITION_H_

#include "ndsChannel.h"

enum Operations {
    NDS_TC_OP_GT,            // greater then
    NDS_TC_OP_LT,            // less then
    NDS_TC_OP_NONE,          // no operation
    NDS_TC_OP_RISING,        // rising edge
    NDS_TC_OP_FALLING,       // falling edge
    NDS_TC_OP_RISING_FALLING // rising edge and falling edge
};

namespace nds
{

/// enumerated operations that can occur in trigger conditions

class ITriggerCondition
{
public:
	/// constructor
	ITriggerCondition();

	/// destructor
	virtual ~ITriggerCondition(){};

	/// read the trigger condition value
	virtual void getValue(epicsFloat64 &output) = 0;

    /// read the trigger condition value
	virtual void getValue(epicsInt32 &output) = 0;

    /// set the trigger condition value
    virtual void setValue(epicsFloat64 output) = 0;

    /// set the trigger condition value
    virtual void setValue(epicsInt32 output) = 0;

	///	verify if trigger condition is met on a particular channel
	virtual bool verifyTriggerCondition() = 0;

	/// channel names are limited to 20 chars
	char channel_id[20];

	/// true if channel is negated (e.g., !DI1)
	bool negated;

 	/// true if value is negative
	bool minus;

	/// type of operation in the trigger condition
	Operations operation;

	/// pointer to a particular channel of the device
	nds::Channel *pchannel;

	std::string getChannelId();
	bool isNegated();
	bool isMinus();
	Operations getOperation();
	nds::Channel* getChannel();

};

template <class T>
class TriggerCondition: public ITriggerCondition
{
protected:

public:
	/// trigger condition value
	T value;

	/// constructor
	TriggerCondition();

	///	verify if trigger condition is met on a particular channel
	bool verifyTriggerCondition();

	/// read the trigger condition value
	void getValue(epicsFloat64 &output);

    /// read the trigger condition value
	void getValue(epicsInt32 &output);

    /// set the trigger condition value
    virtual void setValue(epicsFloat64 output);

    /// set the trigger condition value
    virtual void setValue(epicsInt32 output);

};
//-----------------Implementation of TriggerCondition Class ---------------
template <class T>
TriggerCondition<T>::TriggerCondition(){
	channel_id[0] = 0;
	negated = false;
	minus=false;
	operation = NDS_TC_OP_NONE;
	value = 0;
	pchannel = NULL;
}

template <class T>
bool TriggerCondition<T>::verifyTriggerCondition()
{
	bool result = true;
	asynUser *pasynUser=NULL;
	T currentValue;

	/// read current value on the channel and store it in currentValue
	pchannel->getValueFloat64(pasynUser, &currentValue);

	/// verify if condition is met
	if(operation == NDS_TC_OP_NONE){
		/// for a digitial channel result is true if currentValue is not zero
		result = currentValue;
	} else if (operation == NDS_TC_OP_GT){
		result = currentValue > value;
	} else {
		result = currentValue < value;
	}

	return (negated)?!result:result;
}

template <class T>
void TriggerCondition<T>::getValue(epicsFloat64 &output){
	output = value;
}

template <class T>
void TriggerCondition<T>::getValue(epicsInt32 &output){
	output = value;
}

/// set the trigger condition value
template <class T>
void TriggerCondition<T>::setValue(epicsFloat64 output){
    value = output;
}

/// set the trigger condition value
template <class T>
void TriggerCondition<T>::setValue(epicsInt32 output){
    value = output;
}


} /* namespace nds */
#endif /* NDSTRIGGERCONDITION_H_ */
