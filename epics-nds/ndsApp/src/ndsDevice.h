/*
 * ndsDevice.h
 *
 *  Created on: 22.07.2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDSDEVICE_H_
#define NDSDEVICE_H_

#include <string>
#include <map>
#include <list>

#include <boost/function.hpp>

#include "ndsTypes.h"
#include "ndsPVContainer.h"
#include "ndsBase.h"
#include "ndsMacro.h"
#include "ndsDeviceStates.h"
#include "ndsAbstractStateMachine.h"
#include "ndsDeviceStateMachine.h"
#include "ndsChannelStates.h"
#include "ndsFirmware.h"

namespace nds
{

//struct ThreadPvt
//{
//      int exitEventFd;
//};

class ChannelGroup;
class AsynDriver;
class DeviceBaseState;
class Channel;

/**
 *  Device class
 *
 *  Device instance is a key point from
 *  It is responsible to create whole device structure and register device components
 *  (channel groups, channels).
 *
 *  Device instance is used to provide access to device specific functions.
 *  (E.g. power management, firmware update)
 *
 *
 */
class Device : public Base, public AbstractStateMachine<DeviceStates>
{
private:
	/// @cond NDS_INTERNALS
    DeviceBaseState* _state;
	/// @endcond

protected:
	/// @cond NDS_INTERNALS
	typedef std::map<std::string, AsynDriver*> ChannelGroupContainer;
	typedef std::map<std::string, std::string> ParameterContainer;

    /// @cond NDS_INTERNALS
    static DeviceStateUnknown   stateUnknown;
    static DeviceStateIOCInit   stateIOCInit;
    static DeviceStateOn        stateOn;
    static DeviceStateOff       stateOff;
    static DeviceStateInit      stateInit;
    static DeviceStateDefunct   stateDefunct;
    static DeviceStateError     stateError;
    static DeviceStateReset     stateReset;
    /// @endcond

	/**
	 * Fuctor to call registerHandler functions
	 */
	template<typename T>
	struct RegisterHandlerCallerMap
	{
		PVContainers* _pvContainers;
		RegisterHandlerCallerMap(PVContainers* pvContainers):_pvContainers(pvContainers){}
		void operator ()(T obj)
		{
			obj.second->getBase()->registerHandlers(_pvContainers);
			obj.second->registerHandlers();
		}
	};

	/// @endcond

	int _interruptIdFirmware;  		///< Interrupt ID to dispatch Firmware event.
	int _interruptIdHardware;  		///< Interrupt ID to dispatch Hardware event.
	int _interruptIdModel;     		///< Interrupt ID to dispatch Model event.
	int _interruptIdSerial;    		///< Interrupt ID to dispatch Serial event.
	int _interruptIdSoftware;  		///< Interrupt ID to dispatch Software event.
	int _interruptIdUpdateSoftware; ///< Interrupt ID to dispatch UpdateSoftware event.
	int _interruptIdPower;          ///< Interrupt ID to dispatch Power event.
	int _interruptIdReset;       	///< Interrupt ID to dispatch Reset event.
	int _interruptIdPowerDevice;	///< Interrupt ID to dispatch Power Device event.
	int _interruptIdTemperature;    ///< Interrupt ID to dispatch Temperature event.
    int _interruptIdErrorCode;      ///< Interrupt ID to dispatch Error Code event.

	/// Parameter list from nds registering string.
    ParameterContainer _params;

	/// Registered channel groups.
	ChannelGroupContainer _nodes;

	/*** String representation of firmware version.
	 *
	 *   Default handler uses this string to set PV value from it.
	 *   It is also used in default check firmware compatibility version,
	 *   during firmware update procedure.
	 */
	std::string _firmwareVersion;

	/*** String representation of hardware revision.
	 *
	 *   Default handler uses this string to set PV value from it.
	 *   It is also used in default check firmware compatibility version,
	 *   during firmware update procedure.
	 */
	std::string _hardwareRevision;

	/*** String representation of device model.
	 *
	 *   Default handler uses this string to set PV value.
	 *   It is also used in default check firmware compatibility version,
	 *   during firmware update procedure.
	 */
	std::string _model;

	/*** String representation of software version.
	 *
	 *   Default handler uses this string to set PV value from it.
	 */
	std::string _softwareVersion;

	/*** String representation of hardware serial number.
	 *
	 *   Default handler uses this string to set PV value from it.
	 */
	std::string _serial;

	/// @cond NDS_INTERNALS
	Device(const Device&);
	Device& operator = (const Device&);

	DeviceBaseState* getStateObject(DeviceStates state);
	virtual void     setMachineState(DeviceStates state);
	/// @endcond

	/*** Stub. It is used to check hardware compatibility with provided firmware.
	 *
	 *	This method is called to check firmware compatibility.
	 *	Default implementation compares _model, _hardwareRevision, _firmwareVersion with
	 *	info from target parameter.
	 *
	 * @param target is an instance of firmware description.
	 */
	virtual ndsStatus checkFirmwareCompatibility(const Target& target);

	/*** Stub. It should include business logic for firmware update.
	 *
	 *	This stub should be overloaded to implement firmware update process.
	 *
	 *@param module is a module name where this target should be applied.
	 *@param image  is a full path to downloaded image file.
	 *@param method is a description of update method. It is taken from XML file (target@update_method).
	 */
	virtual ndsStatus updateFirmware(const std::string& module, const std::string& image, const std::string& method);

	/*** It parses firmware meta file.
	 *
	 * @param filepath is a path to file to parse.
	 */
    virtual ndsStatus parseFirmwareMetafile(const std::string& filepath);

	/*** It parses firmware meta info from provided XML.
	 *
	 *  @param doc is a pointer to xmlDoc instance.
	 *  @param cur is a pointer to current XmlNode.
	 */
    virtual int parseFirmware(xmlDocPtr doc, xmlNodePtr cur);

    /*** Handler to process FWUP message.
     *	 FWUP expects URL as an argument of the message.
     *
     */
    virtual ndsStatus handleFirmwareUpdateMsg(asynUser *pasynUser, const Message& value);

    /** Registers handlers which provides automatic state update in EPICS DB.
     *
     *  This function uses onEnterState handlers.
     *  If user cleans onEnterState handlers then this method should be call to
     *  restore automatic updating of device state.
     */
    void registerStateNotificationHandlers();

    /**
     * Register OnRequestState handler
     */
    ndsStatus registerOnRequestStateHandlerForAll(nds::DeviceStates requestedState,
            StateHandler handler);

public:

    Device(const std::string& name);

    virtual ~Device();

	/// @cond NDS_INTERNALS
    ndsStatus createStructurePrivate(const char* portName, const char* params);
    void destroyPrivate();
    virtual ndsStatus setIOCInitialization();
    virtual ndsStatus setIOCInitialized();
    virtual ndsStatus propagateRegisterHandlers(PVContainers* pvContainers);

	/// @endcond

    DeviceStates getCurrentState();
    DeviceBaseState* getCurrentStateObj();

    virtual void printStructure(const std::string&);

    /**
     *	This is replacements for the deviceInit function.
     *	All device structure should be created from this one function.
     *
     */
    virtual ndsStatus createStructure(const char* portName, const char* params);

    /** Get string parameter.
     *
     *  If parameter is not set default value will be returned
     *
     *  @param name - parameter's name
     *  @param def - parameter's default value
     */
    const std::string& getStrParam(const std::string& name, const std::string& def) const;

    /** Get integer parameter.
     *
     *  If parameter is not set default value will be returned
     *
     *  @param name - parameter's name
     *  @param def - parameter's default value
     */
    int getIntParam(const std::string&, int def) const;

    // State specific functions

    /// Request to transfer device to state ON
    virtual ndsStatus on();

    /// Request to transfer device to state OFF
    virtual ndsStatus off();

    /// Unconditional transition state ERROR
    virtual ndsStatus error();

    /// Unconditional transition state DEFUNCT
    virtual ndsStatus defunct();

    /// Unconditional transition to state RESETING
    virtual ndsStatus reset();

    /// Register channel group
    ndsStatus registerChannelGroup(ChannelGroup* channelGroup);

    /*** Returns channel group instance by its name.
     *
     */
    ndsStatus getChannelGroup(const std::string& groupName, ChannelGroup**);

    /*** Returns channel instance by its name.
     *
     */
    ndsStatus getChannelByName(const std::string& channelName, Channel**);

    /*** Returns channel instance by group name and channel index.
     *
     */
    ndsStatus getChannel(const std::string& groupName, int idx, Channel**);

    /// Register PV record's handlers
    virtual ndsStatus registerHandlers(PVContainers* pvContainers);

    // HW device specific functions. Contract: HW Device
    /** PV record getter
	 *  Asyn Reason:
	 *  PV Record suffix: IMDL
	 *  This function will be called when EPICS record is read.
	 */
    virtual ndsStatus getModel   (asynUser *pasynUser, char *buf, size_t size, size_t *pcount, int *eomReason);

    /** PV record getter
	 *  Asyn Reason:
	 *  PV Record suffix: ISN
	 *  This function will be called when EPICS record is read.
	 */
    virtual ndsStatus getSerial  (asynUser *pasynUser, char *buf, size_t size, size_t *pcount, int *eomReason);

    /** PV record getter
	 *  Asyn Reason:
	 *  PV Record suffix: IHW
	 *  This function will be called when EPICS record is read.
	 */
    virtual ndsStatus getHardwareRevision(asynUser *pasynUser, char *buf, size_t size, size_t *pcount, int *eomReason);

    /** PV record getter
	 *  Asyn Reason:
	 *  PV Record suffix: IFM
	 *  This function will be called when EPICS record is read.
	 */
    virtual ndsStatus getFirmwareVersion(asynUser *pasynUser, char *buf, size_t size, size_t *pcount, int *eomReason);

    /** PV record getter
	 *  Asyn Reason:
	 *  PV Record suffix: ISW
	 *  This function will be called when EPICS record is read.
	 */
    virtual ndsStatus getSoftwareVersion(asynUser *pasynUser, char *buf, size_t size, size_t *pcount, int *eomReason);

    /** PV record setter
	 *  Asyn Reason:
	 *  PV Record suffix:
	 *  This function will be called when EPICS record is updated.
	 */
    virtual ndsStatus setFirmware(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransfered);

    /** PV record getter
	 *  Asyn Reason:
	 *  PV Record suffix:
	 *  This function will be called when EPICS record is read.
	 */
    virtual ndsStatus getPower(asynUser *pasynUser, epicsInt32* value);

    /** PV record setter
	 *  Asyn Reason:
	 *  PV Record suffix:
	 *  This function will be called when EPICS record is updated.
	 */
    virtual ndsStatus setPower(asynUser  *pasynUser, epicsInt32 value);

    /** PV record setter
	 *  Asyn Reason:
	 *  PV Record suffix:
	 *  This function will be called when EPICS record is updated.
	 */
    virtual ndsStatus setReset(asynUser  *pasynUser, epicsInt32 value);

    /** PV record getter
	 *  Asyn Reason:
	 *  PV Record suffix:
	 *  This function will be called when EPICS record is read.
	 */
    virtual ndsStatus getPowerDevice(asynUser *pasynUser, epicsFloat64* value);

    /** PV record getter
	 *  Asyn Reason:
	 *  PV Record suffix:
	 *  This function will be called when EPICS record is read.
	 */
    virtual ndsStatus getTemperature(asynUser *pasynUser, epicsFloat64* value);

    /*** State PV's record setter
     *
     */
    virtual ndsStatus writeState (asynUser *pasynUser, epicsInt32 value);

    /*** State PV's record getter
     *
     */
    virtual ndsStatus readState  (asynUser *pasynUser, epicsInt32 *value);

    /*** Device state changed handler
     *
     */
    ndsStatus stateChangeHandler (nds::DeviceStates, nds::DeviceStates);

    ndsStatus onIOCInit(nds::DeviceStates, nds::DeviceStates);

    ndsStatus onEnterOff(nds::DeviceStates, nds::DeviceStates);

    /** PV record setter
     *  Asyn Reason: ErrorCode
     *  PV Record suffix: ERR
     *  This function will be called when EPICS record update is requested.
     *
     *  Following error codes are applicable:
     *  0 - Fully operational status (no error)
     *  1 - Initializing: During initialization phase board statues shall go to the initialization status
     *  2 - Resetting: During resetting phase board status shall go to this phase
     *  3 - PBIT test failure. (Firmware or Hardware Self-test failure)
     *  4 - No board detected.
     *  5 - Static configuration error during initialization phase.
     *  6 - Dynamic configuration error during operational phase.
     *  7..14 - Device specific
     *  15 - Miscellaneous error during operational phase.
     */
    virtual ndsStatus getErrorCode(asynUser  *pasynUser, epicsInt32* value);

    /**
     *  Channel request state handler.
     *  It will prevent child object to be switched on if parent object
     *  is not yet switched on.
     */
    ndsStatus onRequestChildSwitchOn(ChannelStates, ChannelStates);
    ndsStatus onRequestChildProcessing(nds::ChannelStates, nds::ChannelStates requestedState);

    virtual ndsStatus getInt32(asynUser *pasynUser, epicsInt32* value);
    virtual ndsStatus getFloat64(asynUser *pasynUser, epicsFloat64* value);

    virtual ndsStatus getOctet(asynUser *pasynUser, char *data, size_t maxchars, size_t *nbytesTransfered, int *eomReason);

    virtual ndsStatus getInt8Array(asynUser* pasynUser, epicsInt8 *outArray, size_t nelem,  size_t *nIn);
    virtual ndsStatus getInt16Array(asynUser* pasynUser, epicsInt16 *outArray, size_t nelem,  size_t *nIn);
    virtual ndsStatus getInt32Array(asynUser* pasynUser, epicsInt32 *outArray, size_t nelem,  size_t *nIn);
    virtual ndsStatus getFloat32Array(asynUser* pasynUser, epicsFloat32 *outArray, size_t nelem,  size_t *nIn);
    virtual ndsStatus getFloat64Array(asynUser* pasynUser, epicsFloat64 *outArray, size_t nelem,  size_t *nIn);
    virtual ndsStatus getUInt32Digital(asynUser *pasynUser, epicsUInt32 *value, epicsUInt32 mask);

    /**
     *  Switching all registered groups to ON state
     */
    ndsStatus switchChannelGroupsOn() __attribute__ ((deprecated));

    /**
     *  Switching all registered groups to ON state
     */
    ndsStatus switchChannelGroupsOff();

    ndsStatus startChannelGroups();

    /** Allows to call method on all registered ChannelGroups
     *
     */
    ndsStatus callOnAllChannelGroups(boost::function<ndsStatus(ChannelGroup*)>);

    /** Set the enabled status of the entity. [PV record setter]
     *
     */
    virtual ndsStatus setEnabled(asynUser* pasynUser, epicsInt32 value);

    /** Get the enabled status of the entity. [PV record getter]
     *
     */
    virtual ndsStatus getEnabled(asynUser* pasynUser, epicsInt32 *value);

    /** Enables fastInit
     *
     */
    virtual void enableFastInit();

    ndsStatus checkAutoEnable(nds::DeviceStates prev, nds::DeviceStates);
};


}

#endif /* NDSDEVICE_H_ */
