/*
 * ndsThreadTask.cpp

 *
 *  Created on: 2. sep. 2013
 *      Author: Slava Isaev
 */

#include "ndsDebug.h"
#include "ndsThreadTask.h"
#include "ndsTaskManager.h"

namespace nds
{

ThreadTask* ThreadTask::create(
        const std::string& name,
        unsigned int     stackSize,
        unsigned int     priority,
        TaskBody  body)
{
    ThreadTask* _task = new ThreadTask(
            name,
            stackSize,
            priority,
            new TaskService(),
            body
           );
    TaskManager::getInstance().registerTask(name, _task);
    return _task;
}

ThreadTask::ThreadTask(
        const std::string& name,
        unsigned int       stackSize,
        unsigned int       priority,
        TaskServiceBase*   service,
        TaskBody           body
       ):
    Thread(name, stackSize, priority, service),
    _body(body)
{
}

ThreadTask::~ThreadTask()
{
}

void ThreadTask::run(TaskServiceBase&  service)
{
    try
    {
        _body(service);
    }catch(...)
    {
        NDS_ERR("ThreadTask '%s' throws exception.", _name.c_str());
    }
}


} /* namespace nds */
