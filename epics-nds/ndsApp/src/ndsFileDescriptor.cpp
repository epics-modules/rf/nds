/*
 * ndsFileDescriptor.cpp
 *
 *  Created on: 23.08.2013
 *      Author: Slava
 */

#include "ndsFileDescriptor.h"

#include <unistd.h>

namespace nds
{

FileDescriptor::FileDescriptor(TaskServiceBase &service, int fd, uint32_t events, FileEventHandler& handler):
        _service(service), _fd(fd), _handler(handler)
{
    memset(&_epollEvent, 0, sizeof(struct epoll_event));
    _epollEvent.events  = events;
    _epollEvent.data.fd = _fd;
}


FileDescriptor::~FileDescriptor()
{
}

struct epoll_event* FileDescriptor::getEpollEvent()
{
    return &_epollEvent;
}

int FileDescriptor::getFD()
{
    return _fd;
}

void FileDescriptor::notify(const struct epoll_event& event)
{
    _handler(_service, event);
}


} /* namespace nds */
