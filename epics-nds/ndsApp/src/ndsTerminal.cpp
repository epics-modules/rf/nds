/*
 * ndsTerminal.cpp
 *
 *  Created on: 24. apr. 2013
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#include "ndsTerminal.h"

namespace nds
{

Terminal::Terminal()
{
    // TODO Auto-generated constructor stub

}

Terminal::~Terminal()
{
    // TODO Auto-generated destructor stub
}

ndsStatus Terminal::registerHandlers(nds::PVContainers* pvContainers)
{

    Channel::registerHandlers(pvContainers);

    NDS_PV_REGISTER_INT32("Inverted", &Terminal::setInverted, &Terminal::getInverted, &_interruptIdInverted);
    return ndsSuccess;
}

ndsStatus Terminal::setInverted(asynUser* pasynUser, epicsInt32 value)
{
    _Inverted = value;
    return ndsSuccess;
}


ndsStatus Terminal::getInverted(asynUser* pasynUser, epicsInt32* value)
{
    *value = _Inverted;
    return ndsSuccess;
}


} /* namespace nds */
