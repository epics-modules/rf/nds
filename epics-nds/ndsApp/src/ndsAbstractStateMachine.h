/*
 * ndsAbstractStateMachine.h
 *
 *  Created on: 20. avg. 2012
 *      Author: Slava Isaev (C) Cosylab. Slovenia.
 */

#ifndef NDSABSTRACTSTATEMACHINE_H_
#define NDSABSTRACTSTATEMACHINE_H_

#include <map>
#include <vector>
#include <boost/function.hpp>
#include <boost/bind.hpp>

#include "ndsTypes.h"
#include "ndsDebug.h"

namespace nds
{

/// @cond NDS_INTERNALS

template<typename T>
class AbstractStateHandlersHolder;

template<typename T>
class AbstractStateMachine;

template<typename T>
class StateMachineDescriptor
{
public:
    typedef T StatesEnum;

    virtual ~StateMachineDescriptor(){}
    virtual std::string getStateName(T state)=0;
    virtual bool isTransitionAlowed(T currentState, T requestedState)=0;
    virtual void configureSateMachine( AbstractStateMachine<T>* )=0;

    virtual void error( AbstractStateMachine<T>* )=0;
    virtual void defunct( AbstractStateMachine<T>* )=0;

};


/**
 *	Class to store handlers of state machine processing
 */
template<typename T>
class AbstractStateHandlersHolder
{
public:
	typedef boost::function<ndsStatus(T, T)> StateHandler;

private:
	typedef std::vector<StateHandler> StateHandlerVector;
	typedef std::map<T, StateHandlerVector> StateHandlerMap;

	struct HandlerKeeper
	{
	public:
		StateHandlerMap OnRequest;
		StateHandlerVector OnLeave;
		StateHandlerVector OnEnter;

		size_t backedRequestHandlers;
        size_t backedLeaveHandlers;
        size_t backedEnterHandlers;

        HandlerKeeper():
            backedRequestHandlers(0),
            backedLeaveHandlers(0),
            backedEnterHandlers(0)
        {

        }
	};

	typedef std::map<T, HandlerKeeper> HandlerMap;
	HandlerMap _handlers;
public:
	virtual ~AbstractStateHandlersHolder()
	{
	}

	ndsStatus registerRequestHandler(T state, T requestedState,
			StateHandler handler)
	{
	    StateHandlerVector &vector = _handlers[state].OnRequest[requestedState];
		vector.push_back(handler);
		return ndsSuccess;
	}

	ndsStatus registerLeaveHandler(T state, StateHandler handler)
	{
	    StateHandlerVector &vector = _handlers[state].OnLeave;
		vector.insert(vector.end()-_handlers[state].backedLeaveHandlers, handler);
		return ndsSuccess;
	}

	ndsStatus registerEntertHandler(T state, StateHandler handler)
	{
	    StateHandlerVector &vector = _handlers[state].OnEnter;
	    vector.insert(vector.end()-_handlers[state].backedEnterHandlers, handler);
		return ndsSuccess;
	}

    ndsStatus pushbackRequestHandler(T state, T requestedState,
            StateHandler handler)
    {
        StateHandlerVector &vector = _handlers[state].OnRequest[requestedState];
        vector.push_back(handler);
        return ndsSuccess;
    }

    ndsStatus pushbackLeaveHandler(T state, StateHandler handler)
    {
        StateHandlerVector &vector = _handlers[state].OnLeave;
        vector.push_back(handler);
        ++_handlers[state].backedLeaveHandlers;
        return ndsSuccess;
    }

    ndsStatus pushbackEntertHandler(T state, StateHandler handler)
    {
        StateHandlerVector &vector = _handlers[state].OnEnter;
        vector.push_back(handler);
        ++_handlers[state].backedEnterHandlers;
        return ndsSuccess;
    }

    ndsStatus cleanRequestHandler(T state, T requestedState)
    {
        _handlers[state].OnRequest[requestedState].clear();
        _handlers[state].backedRequestHandlers = 0;
        return ndsSuccess;
    }

    ndsStatus cleanLeaveHandler(T state)
    {
        _handlers[state].OnLeave.clear();
        _handlers[state].backedLeaveHandlers=0;
        return ndsSuccess;
    }

    ndsStatus cleanEntertHandler(T state)
    {
        _handlers[state].OnEnter.clear();
        _handlers[state].backedEnterHandlers = 0;
        return ndsSuccess;
    }

	ndsStatus doOnRequestStateHandlers(T state, T requestedState)
	{
		NDS_STK("doOnRequestStateHandlers");
		typename HandlerMap::const_iterator itr = _handlers.find(state);
		if (_handlers.end() != itr)
		{
			typename StateHandlerMap::const_iterator itr2 =
					itr->second.OnRequest.find(requestedState);
			if (itr->second.OnRequest.end() != itr2)
			{
				for (typename StateHandlerVector::const_iterator itr3 =
						itr2->second.begin();
						itr3 != itr2->second.end();
						++itr3)
				{
					NDS_TRC("Calling onRequest handler");
					try
					{
					    ndsStatus st = (*itr3)(state, requestedState);
						if (ndsSuccess != st)
						{
							NDS_WRN("RequestStateHandler: Error!");
							return st;
						}
					} catch (...)
					{
						NDS_ERR("On request state handler throws exception.");
						return ndsError;
					}
				}
			}else
			{
				NDS_TRC("doOnRequestStateHandlers: empty");
			}
		}
		return ndsSuccess;
	}

	ndsStatus doOnLeaveStateHandlers(T state, T requestedState)
	{
		NDS_STK("doOnLeaveStateHandlers");
		typename HandlerMap::const_iterator itr = _handlers.find(state);
		if (_handlers.end() != itr)
		{
			for (typename StateHandlerVector::const_iterator itr3 =
					itr->second.OnLeave.begin();
					itr3 != itr->second.OnLeave.end(); ++itr3)
			{
				NDS_TRC("Calling onLeave handler");
				try
				{
					(*itr3)(state, requestedState);
				} catch (...)
				{
					NDS_ERR("On leave state handler throws exception.");
				}
			}
		}else
		{
			NDS_TRC("doOnLeaveStateHandlers: empty");
		}
		return ndsSuccess;
	}

	ndsStatus doOnEnterStateHandlers(T state, T requestedState)
	{
		NDS_STK("doOnEnterStateHandlers");

		typename HandlerMap::const_iterator itr = _handlers.find(requestedState);
		if (_handlers.end() != itr)
		{
			for (typename StateHandlerVector::const_iterator itr3 =
					itr->second.OnEnter.begin();
					itr3 != itr->second.OnEnter.end(); ++itr3)
			{
				NDS_TRC("Calling onEnter handler");
				try
				{
					(*itr3)(state, requestedState);
				} catch (...)
				{
					NDS_ERR("On enter state handler throws exception.");
				}
			}
		}else
		{
			NDS_TRC("doOnEnterStateHandlers: empty");
		}
		return ndsSuccess;
	}


    ndsStatus blockTransition(T state, T requestedState)
    {
        NDS_WRN("Transition prohibited");
        return ndsBlockTransition;
    }

    ndsStatus alowTransition(T state, T requestedState)
    {
        return ndsSuccess;
    }

};
/// @endcond

template<typename T>
class AbstractState;

/*
 * State machine abstract class
 */
template<typename T>
class AbstractStateMachine
{
	/// @cond NDS_INTERNALS
	template<typename A>
	friend class AbstractState;
	/// @endcond

public:
	/// @cond NDS_INTERNALS
	virtual ~AbstractStateMachine()
	{
	}

	typedef AbstractStateHandlersHolder<T> StateHandlersHolder;
	typedef typename AbstractStateHandlersHolder<T>::StateHandler StateHandler;
	/// @endcond

	/**
	 * Register OnRequestState handler
	 */
	ndsStatus registerOnRequestStateHandler(T state, T requestedState,
			StateHandler handler)
	{
		return _handlerHolder.registerRequestHandler(state, requestedState,
				handler);
	}

	/**
	 * Regster OnLeaveState handler
	 */
	ndsStatus registerOnLeaveStateHandler(T state, StateHandler handler)
	{
		return _handlerHolder.registerLeaveHandler(state, handler);
	}

	/**
	 * Register OnEnterState handler
	 */
	ndsStatus registerOnEnterStateHandler(T state, StateHandler handler)
	{
		return _handlerHolder.registerEntertHandler(state, handler);
	}

    /**
     * Regster OnLeaveState handler
     */
    ndsStatus pushbackOnLeaveStateHandler(T state, StateHandler handler)
    {
        return _handlerHolder.pushbackLeaveHandler(state, handler);
    }

    /**
     * Register OnEnterState handler
     */
    ndsStatus pushbackOnEnterStateHandler(T state, StateHandler handler)
    {
        return _handlerHolder.pushbackEntertHandler(state, handler);
    }

    void cleanOnRequestStateHandlers(T state, T requestedState)
    {
        _handlerHolder.cleanRequestHandler(state, requestedState);
    }

    void cleanOnLeaveStateHandlers(T state)
    {
        _handlerHolder.cleanLeaveHandler(state);
    }

    void cleanOnEnterStateHandler(T state)
    {
        _handlerHolder.cleanEntertHandler(state);
    }

    void prohibitTransition(T state, T requestedState)
    {
        _handlerHolder.registerRequestHandler(state, requestedState,
                boost::bind(&AbstractStateMachine<T>::onWrongStateRequested, this, _1, _2) );
    }

    ndsStatus onWrongStateRequested(T currentState , T requestedState)
    {
        NDS_WRN( "Wrong state requested." );
        return ndsBlockTransition;
    }

protected:
	virtual void
	setMachineState(T requestedState)=0;

	ndsStatus doOnRequestStateHandlers(T state, T requestedState)
	{
		return _handlerHolder.doOnRequestStateHandlers(state, requestedState);
	}

	ndsStatus doOnLeaveStateHandlers(T state, T requestedState)
	{
		return _handlerHolder.doOnLeaveStateHandlers(state, requestedState);
	}

	ndsStatus doOnEnterStateHandlers(T state, T requestedState)
	{
		return _handlerHolder.doOnEnterStateHandlers(state, requestedState);
	}
private:
	StateHandlersHolder _handlerHolder;
	T _currentState;
    T _previousState;
};


/*
 * Abstract state class
 */
template<typename T>
class AbstractState
{
public:

    AbstractState()
    {
    }

	virtual ~AbstractState()
	{
	}

	/// Get state object
	virtual T getState()=0;

	/// Function to handle errors.
	virtual ndsStatus error(AbstractStateMachine<T>*)=0;

    /// Function to handle errors.
    virtual ndsStatus defunct(AbstractStateMachine<T>*)=0;

	/// Set state
	ndsStatus setMachineState(AbstractStateMachine<T>* obj, T requestedState)
	{
		NDS_STK("setMachineState");
		obj->doOnLeaveStateHandlers(getState(), requestedState);
		obj->setMachineState(requestedState);
		obj->doOnEnterStateHandlers(getState(), requestedState);
		return ndsSuccess;
	}

	/// Request transition to new state
	ndsStatus requestState(AbstractStateMachine<T>* obj, T requestedState)
	{
		NDS_STK("requestState");
		ndsStatus st = obj->doOnRequestStateHandlers(getState(), requestedState);
		switch (st)
		{
		case ndsSuccess:
			setMachineState(obj, requestedState);
			return ndsSuccess;
		case ndsBlockTransition:
		    return ndsSuccess;
		case ndsDefunct:
		    defunct(obj);
		    break;
		default :
	        error(obj);
		    break;
		};
        return ndsError;
	}
};

}

#endif /* NDSABSTRACTSTATEMACHINE_H_ */
