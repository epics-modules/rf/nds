/*
 * utsBufferTools.h
 *
 *  Created on: 6. dec. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef PCOTOOLS_H_
#define PCOTOOLS_H_

#include <stdio.h>
#include <stdlib.h>

int saveRawToFile(const char* filename, const void* data, size_t elemSize, size_t elemNumber );

int loadRawFromFile(const char* filename, void** data, size_t* size );

#endif /* PCOTOOLS_H_ */
