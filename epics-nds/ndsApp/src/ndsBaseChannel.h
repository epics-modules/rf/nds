/*
 * ndsBaseChannel.h
 *
 *  Created on: 17. avg. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDSBASECHANNEL_H_
#define NDSBASECHANNEL_H_

#include "ndsBase.h"
#include "ndsChannelStates.h"
#include "ndsAbstractStateMachine.h"
#include "ndsChannelStateMachine.h"
#include "ndsTrigger.h"

namespace nds
{

class ActiveChannelState;
class FunctionalChannelState;

/// Map to store sources/targets
typedef std::map<std::string, int> SourceMap;

/**
 * Base class for channel specific objects
 */
class BaseChannel : public Base, public AbstractStateMachine<ChannelStates>
{
	/// @cond NDS_INTERNALS
	friend class FunctionalChannelState;

protected:
    // Business Logic states
    static ChannelStateUnknown stateUnknown;
    static ChannelStateIOCInitialization stateIOCInitialization;
    static ChannelStateOff stateOff;
    static ChannelStateOn stateOn;
    static ChannelStateDefunct stateDefunct;
    static ChannelStateError stateError;
    static ChannelStateProcessing stateProcessing;
    static ChannelStateReady stateReady;
    static ChannelStateReset stateReset;
    /// @endcond

	Device* _device;

	Trigger* _trigger;
	Trigger* _pauseTrigger;
	Trigger* _referenceTrigger;

    /// Interrupt ID to dispatch DIODirection event
    int _interruptIdDIODirection;
    epicsInt32 _DIODirection;

    /// Interrupt ID to dispatch Differential event
    int _interruptIdDifferential;
    epicsInt32 _Differential;

    /// Interrupt ID to dispatch Ground event
    int _interruptIdGround;
    epicsInt32 _Ground;

	SourceMap _TriggerSourceMap; ///< Map of trigger sources
	SourceMap _TriggerTargetMap; ///< Map of targets for trigger routing
	SourceMap _PauseTriggerSourceMap; ///< Map of pause trigger's sources
	SourceMap _PauseTriggerTargetMap; ///< Map of targets for pause trigger routing
	SourceMap _ReferenceTriggerSourceMap; ///< Map of reference trigger's sources
	SourceMap _ReferenceTriggerTargetMap;///< Map of targets for reference trigger routing
	SourceMap _ClockSourceMap; ///< Map of clock's sources
	SourceMap _ClockTargetMap; ///< Map of targets for clock routing
	SourceMap _ConvertClockBaseMap; ///< Map of convert clock's sources
	SourceMap _ConvertClockTargetMap; ///< Map of targets for convert clock routing

    /// 0 if the channel is not triggeread.
    int _isTriggered;

	int _interruptIdTrigger;	///< Interrupt ID to dispatch Trigger event
	int _interruptIdTriggerCondition; ///< Interrupt ID to dispatch TriggerChannel event

	char* _Trigger; ///< Buffer to store trigger
	char* _TriggerChannel; ///< Buffer to store trigger channel

	epicsInt32 _TriggerRepeat;      ///< Internal variable to store TriggerRepeat
	epicsInt32 _TriggerType;        ///< Internal variable to store TriggerType
	epicsInt32 _TriggerDelay;     ///< Internal variable to store TriggerDelay
	int _interruptIdTriggerRepeat;  ///< Interrupt ID to dispatch TriggerRepeat event
	int _interruptIdTriggerDelay;   ///< Interrupt ID to dispatch TriggerDelay event
	int _interruptIdTriggerRouting; ///< Interrupt ID to dispatch TriggerDelay event
	int _interruptIdTriggerTarget;
	int _interruptIdTriggerType;
	int _interruptIdTriggerPolarity;
    int _interruptIdPauseTriggerTarget;
    int _interruptIdPauseTriggerType;
    int _interruptIdPauseTriggerPolarity;
    int _interruptIdReferenceTriggerTarget;
    int _interruptIdReferenceTriggerType;
    int _interruptIdReferenceTriggerPolarity;

	int _interruptIdPauseTrigger; ///< Interrupt ID to dispatch PauseTrigger event
	int _interruptIdPauseTriggerCondition; ///< Interrupt ID to dispatch PauseTriggerChannel event

	char* _PauseTrigger; ///< Buffer to store PauseTrigger
	char* _PauseTriggerChannel; ///< Buffer to store PauseTrigger channel

	epicsInt32 _PauseTriggerRepeat; ///< Internal variable to store PauseTriggerRepeat
	epicsInt32 _PauseTriggerType; ///< Internal variable to store PauseTriggerType
	epicsInt32 _PauseTriggerDelay; ///< Internal variable to store PauseTriggerDelay

	int _interruptIdPauseTriggerRepeat; ///< Interrupt ID to dispatch PauseTriggerRepeat event
	int _interruptIdPauseTriggerDelay;  ///< Interrupt ID to dispatch PauseTriggerDelay event
	int _interruptIdPauseTriggerRouting;     ///< Interrupt ID to dispatch PauseTriggerDelay event
	int _interruptIdReferenceTrigger;        ///< Interrupt ID to dispatch ReferenceTrigger event
	int _interruptIdReferenceTriggerCondition; ///< Interrupt ID to dispatch ReferenceTriggerChannel event


	char* _ReferenceTrigger;        ///< Buffer to store ReferenceTrigger
	char* _ReferenceTriggerCondition; ///< Buffer to store ReferenceTrigger channel

	epicsInt32 _ReferenceTriggerRepeat;      ///< Internal variable to store ReferenceTriggerRepeat
	epicsInt32 _ReferenceTriggerType;        ///< Internal variable to store ReferenceTriggerType
	epicsInt32 _ReferenceTriggerDelay;       ///< Internal variable to store ReferenceTriggerDelay
	int _interruptIdReferenceTriggerRepeat;  ///< Interrupt ID to dispatch ReferenceTriggerRepeat event
	int _interruptIdReferenceTriggerDelay; 	 ///< Interrupt ID to dispatch ReferenceTriggerDelay event
	int _interruptIdReferenceTriggerRouting; ///< Interrupt ID to dispatch ReferenceTriggerDelay event

	int _interruptIdConvertClockBase;     ///< Interrupt ID to dispatch ConvertClockBase event
	int _interruptIdConvertClockTarget;   ///< Interrupt ID to dispatch ConvertClockTarget event
	int _interruptIdConvertClockPolarity; ///< Interrupt ID to dispatch ConvertClockPolarity event
	int _interruptIdConvertClockDelay;    ///< Interrupt ID to dispatch ConvertClockDelay event
	int _interruptIdConvertClockRouting;  ///< Interrupt ID to dispatch ConvertClockRouting event

	int _interruptIdClockSource;     ///< Interrupt ID to dispatch ClockSource event
	int _interruptIdClockFrequency;  ///< Interrupt ID to dispatch ClockFrequency event
	int _interruptIdClockMultiplier; ///< Interrupt ID to dispatch ClockMultiplier event
	int _interruptIdClockTarget;     ///< Interrupt ID to dispatch ClockTarget changed event
	int _interruptIdClockRouting;    ///< Interrupt ID to dispatch ClockRouting event
	int _interruptIdClockPolarity;   ///< Interrupt ID to dispatch ClockPolarity event
    int _interruptIdClockDivisor;   ///< Interrupt ID to dispatch ClockPolarity event

	epicsInt32   _ClockSource;      ///< Internal variable to store ClockSource
    epicsInt32   _ClockPolarity;    ///< Internal variable to store ClockPolarity
    epicsFloat64 _ClockFrequency;   ///< Internal variable to store ClockFrequency
	epicsInt32   _ClockMultiplier;  ///< Internal variable to store ClockMultiplier
    epicsInt32   _ClockDivisor;     ///< Internal variable to store ClockDevider

	int _interruptIdProcessingMode; ///< Interrupt ID to dispatch ProcessingMode event
	int _interruptIdSamplesCount;   ///< Interrupt ID to dispatch SamplesCount event
    int _interruptIdRange;          ///< Interrupt ID to dispatch Range event

	epicsInt32 _ProcessingMode; ///< Internal variable to store ProcessingMode
	epicsInt32 _SamplesCount;   ///< Internal variable to store SamplesCount
	epicsInt32 _Range;          ///< Internal variable to store Range

    /// Interrupt ID to dispatch ValueFloat64 event
    int _interruptIdValueUInt32Digital;

    /// @cond NDS_INTERNALS
    FunctionalChannelState* getMachineState(ChannelStates state);
    void setMachineState(ChannelStates state);

    ndsStatus logLeaveState(ChannelStates state, ChannelStates requestedState);

    ndsStatus findSource(const SourceMap& map,
            const std::string& source,
            boost::function<ndsStatus (BaseChannel*, epicsInt32)> handler );

    /// @endcond

    /** Registers handler which provide automatic state update in EPCIS DB.
     *
     *  This methods is using onEnterState handlers.
     *  If developer cleans onEnterState handlers, then this method should be called
     *  to restore automatic state updates.
     */
    void registerStateNotificationHandlers();

    /**
     * Register OnRequestState handler
     */
    ndsStatus registerOnRequestStateHandlerForAll(nds::ChannelStates requestedState,
            StateHandler handler);

public:

	/// Constructor
	BaseChannel();

	/// Destructor
	virtual ~BaseChannel();

    /// Get channel state.
    ChannelStates getCurrentState();
    FunctionalChannelState* getCurrentStateObj();

	/// Request ON state
	ndsStatus on() __attribute__ ((deprecated));

	/// Request OFF state
	ndsStatus off() __attribute__ ((deprecated));

	/// Request starting processing
	ndsStatus start();

    /// Channel is configured and ready to process
    ndsStatus ready() __attribute__ ((deprecated));

	/// Request stopping processing
	ndsStatus stop();

	/// Transition to degrading state
	ndsStatus degrading() __attribute__ ((deprecated));

	/// Transition to normal functioning state
	ndsStatus restoring() __attribute__ ((deprecated));

	/**
	 *	Non conditional transition to ERROR state
	 */
	ndsStatus error();

    /// @cond NDS_INTERNALS
	virtual ndsStatus setState(asynUser* pasynUser, epicsInt32 value);
	virtual ndsStatus getState(asynUser* pasynUser, epicsInt32 *value);
    ndsStatus stateChangeHandler(nds::ChannelStates, nds::ChannelStates);

    //State related members
    ndsStatus setIOCInitialization();
    ndsStatus setIOCInitialized();

	ndsStatus enable();

	ndsStatus disable();

    ndsStatus reset();
    /// @endcond

	/// Register PV record's handlers
    virtual ndsStatus registerHandlers(PVContainers* pvContainers);

    /** \brief Set processing mode. [PV record setter]
     *
 	 *  Asyn Reason: ProcessingMode\n
	 *  PV Record suffix: PM\n
	 *
	 *	Processing mode allows to set type of signal generation or digitazing
	 *	Available values:
	 *	 Single-point
	 *   Finite
	 *   Continuous
	 *
	 *  This function will be called when EPICS record value is set.
     */
	virtual ndsStatus setProcessingMode(asynUser *pasynUser, epicsInt32 value);

    virtual ndsStatus getProcessingMode(asynUser *pasynUser, epicsInt32 *value);

    /** \brief Set samples count to read/write in finite processing mode.  [PV record setter]
     *
 	 *  Asyn Reason: SamplesCount\n
	 *  PV Record suffix: SMNM\n
	 *
	 *  This function will be called when EPICS record value is set.
     */
	virtual ndsStatus setSamplesCount(asynUser *pasynUser, epicsInt32 value);

	virtual ndsStatus getSamplesCount(asynUser *pasynUser, epicsInt32 *value);

    /** \brief Trigger data acquisition immediately or at specified time.  [PV record setter]
     *
 	 *  Asyn Reason: Trigger\n
	 *  PV Record suffix: TRG\n
	 *
	 *  This function will be called when EPICS record value is set.
     */
	virtual ndsStatus setTrigger(asynUser *pasynUser, epicsInt32 value);

    /** \brief  Get trigger parameters[PV record getter]
     *
	 *  Asyn Reason: \n
	 *  PV Record suffix: - \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
	virtual ndsStatus getTrigger(asynUser *pasynUser, epicsInt32 *value);

    /** \brief Set source for the trigger. [PV record setter]
     *
 	 *  Asyn Reason: TriggerChannel\n
	 *  PV Record suffix: TRGC\n
	 *
	 *	Please refer to section 4.1.2 for syntax of this record.
	 *
	 *  This function will be called when EPICS record value is set.
     */
	virtual ndsStatus setTriggerCondition(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransferead);

    virtual ndsStatus onTriggerConditionParsed(asynUser *pasynUser, const Trigger&);

    /** \brief Get source for the trigger. [PV record getter]
     *
	 *  Asyn Reason: TriggerChannel\n
	 *  PV Record suffix: \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
	virtual ndsStatus getTriggerCondition(asynUser *pasynUser, char *data, size_t maxchars, size_t *nbytesTransferead, int *eomReason);

    /** \brief Set the delay for the trigger. [PV record setter]
     *
	 *  Asyn Reason: TriggerDelay\n
	 *  PV Record suffix: TRGD\n
	 *
	 *  The delay is given as the number of seconds past the trigger event.
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setTriggerDelay(asynUser* pasynUser, epicsInt32 value);

    /** \brief  [PV record getter]
     *
	 *  Asyn Reason: TriggerDelay\n
	 *  PV Record suffix: - \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getTriggerDelay(asynUser* pasynUser, epicsInt32 *value);

    /** \brief Set the number of times that the trigger should be triggered. [PV record setter]
     *
	 *  Asyn Reason: TriggerRepeat\n
	 *  PV Record suffix: TRGR\n
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setTriggerRepeat(asynUser* pasynUser, epicsInt32 value);

    /** \brief  [PV record getter]
     *
	 *  Asyn Reason: TriggerRepeat\n
	 *  PV Record suffix: TRGR\n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getTriggerRepeat(asynUser* pasynUser, epicsInt32 *value);

    /** \brief Set trigger type software/hardware.  [PV record setter]
     *
 	 *  Asyn Reason: TriggerType\n
	 *  PV Record suffix: TRGT\n
	 *
	 *  This function will be called when EPICS record value is set.
     */
	virtual ndsStatus setTriggerType(asynUser *pasynUser, epicsInt32 value);

    /** \brief Get configured trigger type software/hardware[PV record getter]
     *
	 *  Asyn Reason: TriggerType\n
	 *  PV Record suffix: - \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
	virtual ndsStatus getTriggerType(asynUser *pasynUser, epicsInt32 *value);

    /** \brief Enable/Disable trigger routing  [PV record setter]
     *
 	 *  Asyn Reason: TriggerRouting\n
	 *  PV Record suffix: TRGO\n
	 *
	 *  This function will be called when EPICS record value is set.
     */
	virtual ndsStatus setTriggerRouting(asynUser *pasynUser, epicsInt32 value);

    /** \brief Get status of trigger routing [PV record getter]
     *
	 *  Asyn Reason: TriggerRouting\n
	 *  PV Record suffix: - \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
	virtual ndsStatus getTriggerRouting(asynUser *pasynUser, epicsInt32 *value);

    /** \brief Set trigger routing target [PV record setter]
     *
 	 *  Asyn Reason: TriggerTarget\n
	 *  PV Record suffix: TRGA\n
	 *
	 *  This function will be called when EPICS record value is set.
     */
	virtual ndsStatus setTriggerTarget(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransferead);

    /** \brief Get trigger routing target [PV record getter]
     *
	 *  Asyn Reason: TriggerTarget\n
	 *  PV Record suffix: - \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
	virtual ndsStatus getTriggerTarget(asynUser *pasynUser, char *data, size_t maxchars, size_t *nbytesTransferead, int *eomReason);

    /** \brief Set trigger polarity rising/falling [PV record setter]
     *
 	 *  Asyn Reason: TriggerPolarity\n
	 *  PV Record suffix: TRGP\n
	 *
	 *  This function will be called when EPICS record value is set.
     */
	virtual ndsStatus setTriggerPolarity(asynUser *pasynUser, epicsInt32 value);

    /** \brief Get trigger polarity rising/falling[PV record getter]
     *
	 *  Asyn Reason: TriggerPolarity\n
	 *  PV Record suffix: - \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
	virtual ndsStatus getTriggerPolarity(asynUser *pasynUser, epicsInt32 *value);

    /** \brief Set source for the convert clock.[PV record setter]
     *
 	 *  Asyn Reason: ConvertClockBase\n
	 *  PV Record suffix: CCLB\n
	 *
	 *  This function will be called when EPICS record value is set.
     */
	virtual ndsStatus setConvertClockBase(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransferead);

    /** \brief Get source which is used as a base for convert clock [PV record getter]
     *
	 *  Asyn Reason: ConvertClockBase\n
	 *  PV Record suffix: - \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
	virtual ndsStatus getConvertClockBase(asynUser *pasynUser, char *data, size_t maxchars, size_t *nbytesTransferead, int *eomReason);

    /** \brief Set polarity of convert clock to start sampling rising/falling. [PV record setter]
     *
 	 *  Asyn Reason: ConvertClockPolarity\n
	 *  PV Record suffix: CCLP\n
	 *
	 *  This function will be called when EPICS record value is set.
     */
	virtual ndsStatus setConvertClockPolarity(asynUser *pasynUser, epicsInt32 value);

    /** \brief Get polarity of convert clock. [PV record getter]
     *
	 *  Asyn Reason: ConvertClockPolarity\n
	 *  PV Record suffix: - \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
	virtual ndsStatus getConvertClockPolarity(asynUser *pasynUser, epicsInt32 *value);

    /** \brief Set convert clock delay related to start trigger. [PV record setter]
     *
 	 *  Asyn Reason: ConvertClockDelay\n
	 *  PV Record suffix: CCLD\n
	 *
	 *  This function will be called when EPICS record value is set.
     */
	virtual ndsStatus setConvertClockDelay(asynUser *pasynUser, epicsInt32 value);

    /** \brief Get convert clock delay related to start trigger. [PV record getter]
     *
	 *  Asyn Reason: ConvertClockDelay\n
	 *  PV Record suffix: - \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
	virtual ndsStatus getConvertClockDelay(asynUser *pasynUser, epicsInt32 *value);

    /** \brief Set target for routing convert clock.[PV record setter]
     *
 	 *  Asyn Reason: ConvertClockTarget\n
	 *  PV Record suffix: CCLA\n
	 *
	 *  This function will be called when EPICS record value is set.
     */
	virtual ndsStatus setConvertClockTarget(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransferead);

    /** \brief Get convert clock routing target [PV record getter]
     *
	 *  Asyn Reason: ConvertClockTarget\n
	 *  PV Record suffix: - \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
	virtual ndsStatus getConvertClockTarget(asynUser *pasynUser, char *data, size_t maxchars, size_t *nbytesTransferead, int *eomReason);

    /** \brief Enable/disable convert clock routing to target. [PV record setter]
     *
 	 *  Asyn Reason: ConvertClockRouting\n
	 *  PV Record suffix: CCLO\n
	 *
	 *  This function will be called when EPICS record value is set.
     */
	virtual ndsStatus setConvertClockRouting(asynUser *pasynUser, epicsInt32 value);

    /** \brief Get status of convert clock routing. [PV record getter]
     *
	 *  Asyn Reason: ConvertClockRouting\n
	 *  PV Record suffix: - \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
	virtual ndsStatus getConvertClockRouting(asynUser *pasynUser, epicsInt32 *value);

	/** \brief Set the clock source. [PV record setter]
	 *
	 *  Asyn Reason: ClockSource\n
	 *  PV Record suffix: CLKS\n
	 *  This function will be called when EPICS record value set.
	 *
	 *  Set the clock source for sampling (input channel), frame acquisition (camera) or clocking digital-to-analog converter (output channel).
     */
    virtual ndsStatus setClockSource(asynUser* pasynUser, epicsInt32 value);

    /** \brief Get clock source. [PV record getter]
     *
	 *  Asyn Reason: ClockSource \n
	 *  PV Record suffix: \n
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getClockSource(asynUser* pasynUser, epicsInt32 *value);

    /** \brief Set the frequency of the internal clock selected with CLKSRC record. The frequency is given in Hz. [PV record setter]
     *
	 *  Asyn Reason: ClockFrequency\n
	 *  PV Record suffix: CLKF\n
	 *  This function will be called when EPICS record value is set.
	 *
	 *  For camera channels, this parameter specifies the number of frames per second. \n
	 *
	 *  If a clock source other than an internal clock source is selected, or if the selected
	 *  clock source doesn’t support the requested frequency,
	 *  alarm severity (SEVR field) of this record is set to MINOR and alarm state (STAT) is set to WRITE.
	 *
	 *  If internal clock source is selected, the frequency is set to the nearest smaller frequency.
	 *
     */
    virtual ndsStatus setClockFrequency(asynUser* pasynUser, epicsFloat64 value);

    /** \brief Frequency of the internal clock. [PV record getter]
     *
	 *  Asyn Reason: ClockFrequency\n
	 *  PV Record suffix: CLKF\n
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getClockFrequency(asynUser* pasynUser, epicsFloat64 *value);

    /** \brief Set the clock multiplier of the external clock selected with CLKSRC record.  [PV record setter]
     *
	 *  Asyn Reason: ClockMultiplier\n
	 *  PV Record suffix: CLKM\n
	 *
	 *  The multiplier is given as a factor, e.g., 1.0 to keep the external source’s frequency,
	 *   0.25 to divide it by 4, and 4 to multiply it with 4, etc.\n
	 *
	 *   If a clock source other than an external clock source is selected,
	 *   or if the selected clock source doesn’t support the requested multiplier factor,
	 *   alarm severity (SEVR field) of this record is set to MINOR and alarm state (STAT) is set to WRITE.
	 *
	 *   If external clock source is selected, the multiplier is set to the nearest smaller multiplier.
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setClockMultiplier(asynUser* pasynUser, epicsInt32 value);

    /** \brief  [PV record getter]
     *
	 *  Asyn Reason: ClockMultiplier\n
	 *  PV Record suffix: - \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getClockMultiplier(asynUser* pasynUser, epicsInt32 *value);

    virtual ndsStatus setClockDivisor(asynUser* pasynUser, epicsInt32 value);

    virtual ndsStatus getClockDivisor(asynUser* pasynUser, epicsInt32 *value);

    /** \brief [PV record setter]
     *
 	 *  Asyn Reason: ClockRouting\n
	 *  PV Record suffix: CLKO\n
	 *
	 *  This function will be called when EPICS record value is set.
     */
	virtual ndsStatus setClockRouting(asynUser *pasynUser, epicsInt32 value);

    /** \brief Get trigger routing target [PV record getter]
     *
	 *  Asyn Reason: ClockRouting\n
	 *  PV Record suffix: - \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
	virtual ndsStatus getClockRouting(asynUser *pasynUser, epicsInt32 *value);

    /** \brief [PV record setter]
     *
 	 *  Asyn Reason: ClockTarget\n
	 *  PV Record suffix: CLKA\n
	 *
	 *  This function will be called when EPICS record value is set.
     */
	virtual ndsStatus setClockTarget(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransferead);

    /** \brief Get trigger routing target [PV record getter]
     *
	 *  Asyn Reason: ClockTarget\n
	 *  PV Record suffix: - \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
	virtual ndsStatus getClockTarget(asynUser *pasynUser, char *data, size_t maxchars, size_t *nbytesTransferead, int *eomReason);

    /** \brief Set polarity (rising/falling) of the clock to start sampling. [PV record setter]
     *
 	 *  Asyn Reason: ClockPolarity\n
	 *  PV Record suffix: CLKP\n
	 *
	 *  This function will be called when EPICS record value is set.
     */
	virtual ndsStatus setClockPolarity(asynUser *pasynUser, epicsInt32 value);
    /** \brief Get polarity of the clock. [PV record getter]
     *
	 *  Asyn Reason: ClockPolarity\n
	 *  PV Record suffix: - \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
	virtual ndsStatus getClockPolarity(asynUser *pasynUser, epicsInt32 *value);


    /** \brief Trigger data acquisition immediately or at specified time.  [PV record setter]
     *
 	 *  Asyn Reason: ReferenceTrigger\n
	 *  PV Record suffix: RTRG\n
	 *
	 *  This function will be called when EPICS record value is set.
     */
	virtual ndsStatus setReferenceTrigger(asynUser *pasynUser, epicsInt32 value);

    /** \brief  [PV record getter]
     *
	 *  Asyn Reason: ReferenceTrigger\n
	 *  PV Record suffix: - \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
	virtual ndsStatus getReferenceTrigger(asynUser *pasynUser, epicsInt32 *value);

    /** \brief Set another channel as the trigger. [PV record setter]
     *
 	 *  Asyn Reason: ReferenceTriggerChannel\n
	 *  PV Record suffix: RTGC\n
	 *
	 *	Please refer to section 4.1.2 for syntax of this record.
	 *
	 *  This function will be called when EPICS record value is set.
     */
	virtual ndsStatus setReferenceTriggerCondition(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransferead);

	virtual ndsStatus onReferenceTriggerConditionParsed(asynUser *pasynUser, const Trigger& trigger);

    /** \brief  [PV record getter]
     *
	 *  Asyn Reason: ReferenceTriggerChannel\n
	 *  PV Record suffix: \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
	virtual ndsStatus getReferenceTriggerCondition(asynUser *pasynUser, char *data, size_t maxchars, size_t *nbytesTransferead, int *eomReason);

    /** \brief Set the delay for the trigger. [PV record setter]
     *
	 *  Asyn Reason: ReferenceTriggerDelay\n
	 *  PV Record suffix: RTGD\n
	 *
	 *  The delay is given as the number of seconds past the trigger event.
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setReferenceTriggerDelay(asynUser* pasynUser, epicsInt32 value);

    /** \brief  [PV record getter]
     *
	 *  Asyn Reason: ReferenceTriggerDelay\n
	 *  PV Record suffix: - \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getReferenceTriggerDelay(asynUser* pasynUser, epicsInt32 *value);

    /** \brief Stop data acquisition immediately or at specified time.  [PV record setter]
     *
 	 *  Asyn Reason: ReferenceTriggerType\n
	 *  PV Record suffix: RTGT\n
	 *
	 *  This function will be called when EPICS record value is set.
     */
	virtual ndsStatus setReferenceTriggerType(asynUser *pasynUser, epicsInt32 value);

    /** \brief Get configured reference trigger type [PV record getter]
     *
	 *  Asyn Reason: ReferenceTriggerType\n
	 *  PV Record suffix: - \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
	virtual ndsStatus getReferenceTriggerType(asynUser *pasynUser, epicsInt32 *value);

    /** \brief Enable/Disable reference trigger routing  [PV record setter]
     *
 	 *  Asyn Reason: ReferenceTriggerRouting\n
	 *  PV Record suffix: RTGR\n
	 *
	 *  This function will be called when EPICS record value is set.
     */
	virtual ndsStatus setReferenceTriggerRouting(asynUser *pasynUser, epicsInt32 value);

    /** \brief Get status of reference trigger routing [PV record getter]
     *
	 *  Asyn Reason: ReferenceTriggerRouting\n
	 *  PV Record suffix: - \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
	virtual ndsStatus getReferenceTriggerRouting(asynUser *pasynUser, epicsInt32 *value);

    /** \brief Set reference trigger routing target [PV record setter]
     *
 	 *  Asyn Reason: ReferenceTriggerTarget\n
	 *  PV Record suffix: RTGA\n
	 *
	 *  This function will be called when EPICS record value is set.
     */
	virtual ndsStatus setReferenceTriggerTarget(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransferead);

    /** \brief Get reference trigger routing target [PV record getter]
     *
	 *  Asyn Reason: ReferenceTriggerTarget\n
	 *  PV Record suffix: - \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
	virtual ndsStatus getReferenceTriggerTarget(asynUser *pasynUser, char *data, size_t maxchars, size_t *nbytesTransferead, int *eomReason);

    /** \brief Set reference trigger polarity [PV record setter]
     *
 	 *  Asyn Reason: ReferenceTriggerPolarity\n
	 *  PV Record suffix: RTGP\n
	 *
	 *  This function will be called when EPICS record value is set.
     */
	virtual ndsStatus setReferenceTriggerPolarity(asynUser *pasynUser, epicsInt32 value);

    /** \brief Get reference trigger polarity [PV record getter]
     *
	 *  Asyn Reason: ReferenceTriggerPolarity\n
	 *  PV Record suffix: - \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
	virtual ndsStatus getReferenceTriggerPolarity(asynUser *pasynUser, epicsInt32 *value);

    /** \brief Trigger data acquisition immediately or at specified time.  [PV record setter]
     *
 	 *  Asyn Reason: PauseTrigger\n
	 *  PV Record suffix: PTG\n
	 *
	 *  This function will be called when EPICS record value is set.
     */
	virtual ndsStatus setPauseTrigger(asynUser *pasynUser, epicsInt32 value);

    /** \brief  [PV record getter]
     *
	 *  Asyn Reason: PauseTrigger\n
	 *  PV Record suffix: - \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
	virtual ndsStatus getPauseTrigger(asynUser *pasynUser, epicsInt32 *value);

	virtual ndsStatus onPauseTriggerConditionParsed(asynUser *pasynUser, const Trigger& trigger);

    /** \brief Set another channel as the trigger. [PV record setter]
     *
 	 *  Asyn Reason: PauseTriggerChannel\n
	 *  PV Record suffix: PTGC\n
	 *
	 *	Please refer to section 4.1.2 for syntax of this record.
	 *
	 *  This function will be called when EPICS record value is set.
     */
	virtual ndsStatus setPauseTriggerCondition(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransferead);

    /** \brief  [PV record getter]
     *
	 *  Asyn Reason: PauseTriggerChannel\n
	 *  PV Record suffix: \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
	virtual ndsStatus getPauseTriggerCondition(asynUser *pasynUser, char *data, size_t maxchars, size_t *nbytesTransferead, int *eomReason);

    /** \brief Set the delay for the trigger. [PV record setter]
     *
	 *  Asyn Reason: PauseTriggerDelay\n
	 *  PV Record suffix: PTGD\n
	 *
	 *  The delay is given as the number of seconds past the trigger event.
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setPauseTriggerDelay(asynUser* pasynUser, epicsInt32 value);

    /** \brief  [PV record getter]
     *
	 *  Asyn Reason: PauseTriggerDelay\n
	 *  PV Record suffix: - \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getPauseTriggerDelay(asynUser* pasynUser, epicsInt32 *value);

    /** \brief Trigger data acquisition immediately or at specified time.  [PV record setter]
     *
 	 *  Asyn Reason: PauseTriggerType\n
	 *  PV Record suffix: PTGT\n
	 *
	 *  This function will be called when EPICS record value is set.
     */
	virtual ndsStatus setPauseTriggerType(asynUser *pasynUser, epicsInt32 value);

    /** \brief Get configured trigger type [PV record getter]
     *
	 *  Asyn Reason: PauseTriggerType\n
	 *  PV Record suffix: - \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
	virtual ndsStatus getPauseTriggerType(asynUser *pasynUser, epicsInt32 *value);

    /** \brief Enable/Disable trigger routing  [PV record setter]
     *
 	 *  Asyn Reason: PauseTriggerRouting\n
	 *  PV Record suffix: PTGO\n
	 *
	 *  This function will be called when EPICS record value is set.
     */
	virtual ndsStatus setPauseTriggerRouting(asynUser *pasynUser, epicsInt32 value);

    /** \brief Get status of trigger routing [PV record getter]
     *
	 *  Asyn Reason: PauseTriggerRouting\n
	 *  PV Record suffix: - \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
	virtual ndsStatus getPauseTriggerRouting(asynUser *pasynUser, epicsInt32 *value);

    /** \brief Set trigger routing target [PV record setter]
     *
 	 *  Asyn Reason: PauseTriggerTarget\n
	 *  PV Record suffix: PTGA\n
	 *
	 *  This function will be called when EPICS record value is set.
     */
	virtual ndsStatus setPauseTriggerTarget(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransferead);

    /** \brief Get trigger routing target [PV record getter]
     *
	 *  Asyn Reason: PauseTriggerTarget\n
	 *  PV Record suffix: - \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
	virtual ndsStatus getPauseTriggerTarget(asynUser *pasynUser, char *data, size_t maxchars, size_t *nbytesTransferead, int *eomReason);

    /** \brief Set trigger polarity [PV record setter]
     *
 	 *  Asyn Reason: PauseTriggerPolarity\n
	 *  PV Record suffix: PTGP\n
	 *
	 *  This function will be called when EPICS record value is set.
     */
	virtual ndsStatus setPauseTriggerPolarity(asynUser *pasynUser, epicsInt32 value);

    /** \brief Get trigger polarity [PV record getter]
     *
	 *  Asyn Reason: PauseTriggerPolarity\n
	 *  PV Record suffix: - \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
	virtual ndsStatus getPauseTriggerPolarity(asynUser *pasynUser, epicsInt32 *value);

	/** Add trigger source key-value pair.
	 *
	 *  Key is string representation of the trigger source
	 *  E.g. PFI0, RTSI1, INTERNAL_CLOCK
	 *
	 *	Value is an integer value representing port, reference in register map and etc.
	 */
	void addTriggerSource(const std::string& source, int value);

	/** Add key-value pair.
	 *
	 *  Key is string representation of the trigger source
	 *  E.g. PFI0, RTSI1, INTERNAL_CLOCK
	 *
	 *	Value is an integer value representing port, reference in register map and etc.
	 */
	void addTriggerTarget(const std::string& source, int value);

	/** Add key-value pair.
	 *
	 *  Key is string representation of the trigger source
	 *  E.g. PFI0, RTSI1, INTERNAL_CLOCK
	 *
	 *	Value is an integer value representing port, reference in register map and etc.
	 */
	void addPauseTriggerSource(const std::string& source, int value);

	/** Add key-value pair.
	 *
	 *  Key is string representation of the trigger source
	 *  E.g. PFI0, RTSI1, INTERNAL_CLOCK
	 *
	 *	Value is an integer value representing port, reference in register map and etc.
	 */
	void addPauseTriggerTarget(const std::string& source, int value);

	/** Add key-value pair.
	 *
	 *  Key is string representation of the trigger source
	 *  E.g. PFI0, RTSI1, INTERNAL_CLOCK
	 *
	 *	Value is an integer value representing port, reference in register map and etc.
	 */
	void addReferenceTriggerSource(const std::string& source, int value);

	/** Add key-value pair.
	 *
	 *  Key is string representation of the trigger source
	 *  E.g. PFI0, RTSI1, INTERNAL_CLOCK
	 *
	 *	Value is an integer value representing port, reference in register map and etc.
	 */
	void addReferenceTriggerTarget(const std::string& source, int value);

	/** Add key-value pair.
	 *
	 *  Key is string representation of the trigger source
	 *  E.g. PFI0, RTSI1, INTERNAL_CLOCK
	 *
	 *	Value is an integer value representing port, reference in register map and etc.
	 */
	void addClockSource(const std::string& source, int value);

	/** Add key-value pair.
	 *
	 *  Key is string representation of the trigger source
	 *  E.g. PFI0, RTSI1, INTERNAL_CLOCK
	 *
	 *	Value is an integer value representing port, reference in register map and etc.
	 */
	void addClockTarget(const std::string& source, int value);

	/** Add key-value pair.
	 *
	 *  Key is string representation of the trigger source
	 *  E.g. PFI0, RTSI1, INTERNAL_CLOCK
	 *
	 *	Value is an integer value representing port, reference in register map and etc.
	 */
	void addConvertClockBase(const std::string& source, int value);

	/** Add key-value pair.
	 *
	 *  Key is string representation of the trigger source
	 *  E.g. PFI0, RTSI1, INTERNAL_CLOCK
	 *
	 *	Value is an integer value representing port, reference in register map and etc.
	 */
	void addConvertClockTarget(const std::string& source, int value);

	/** User stub function to select a source/target by Id.
	 *
	 *  NDS provides parsing string value of PV record and provides relevant Id
	 *  to simplify settings.
	 *
	 *  @param value - Id of requested source/target
	 */
	virtual ndsStatus setClockSourceId(epicsInt32 value);

	/** User stub function to select a source/target by Id.
	 *
	 *  NDS provides parsing string value of PV record and provides relevant Id
	 *  to simplify settings.
	 *
	 *  @param value - Id of requested source/target
	 */
	virtual ndsStatus setClockTargetId(epicsInt32 value);

	/** User stub function to select a source/target by Id.
	 *
	 *  NDS provides parsing string value of PV record and provides relevant Id
	 *  to simplify settings.
	 *
	 *  @param value - Id of requested source/target
	 */
	virtual ndsStatus setConvertClockBaseId(epicsInt32 value);

	/** User stub function to select a source/target by Id.
	 *
	 *  NDS provides parsing string value of PV record and provides relevant Id
	 *  to simplify settings.
	 *
	 *  @param value - Id of requested source/target
	 */
	virtual ndsStatus setConvertClockTargetId(epicsInt32 value);

	/** User stub function to select a source/target by Id.
	 *
	 *  NDS provides parsing string value of PV record and provides relevant Id
	 *  to simplify settings.
	 *
	 *  @param value - Id of requested source/target
	 */
	virtual ndsStatus setTriggerSourceId(epicsInt32 value);

	/** User stub function to select a source/target by Id.
	 *
	 *  NDS provides parsing string value of PV record and provides relevant Id
	 *  to simplify settings.
	 *
	 *  @param value - Id of requested source/target
	 */
	virtual ndsStatus setTriggerTargetId(epicsInt32 value);

	/** User stub function to select a source/target by Id.
	 *
	 *  NDS provides parsing string value of PV record and provides relevant Id
	 *  to simplify settings.
	 *
	 *  @param value - Id of requested source/target
	 */
	virtual ndsStatus setReferenceTriggerSourceId(epicsInt32 value);

	/** User stub function to select a source/target by Id.
	 *
	 *  NDS provides parsing string value of PV record and provides relevant Id
	 *  to simplify settings.
	 *
	 *  @param value - Id of requested source/target
	 */
	virtual ndsStatus setReferenceTriggerTargetId(epicsInt32 value);

	/** User stub function to select a source/target by Id.
	 *
	 *  NDS provides parsing string value of PV record and provides relevant Id
	 *  to simplify settings.
	 *
	 *  @param value - Id of requested source/target
	 */
	virtual ndsStatus setPauseTriggerSourceId(epicsInt32 value);

	/** User stub function to select a source/target by Id.
	 *
	 *  NDS provides parsing string value of PV record and provides relevant Id
	 *  to simplify settings.
	 *
	 *  @param value - Id of requested source/target
	 */
	virtual ndsStatus setPauseTriggerTargetId(epicsInt32 value);

    /** \brief Set the number of times that the trigger should be triggered. [PV record setter]
     *
	 *  Asyn Reason: TriggerRepeat\n
	 *  PV Record suffix: TRGR\n
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setPauseTriggerRepeat(asynUser* pasynUser, epicsInt32 value);

    /** \brief  [PV record getter]
     *
	 *  Asyn Reason: TriggerRepeat\n
	 *  PV Record suffix: TRGR\n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getPauseTriggerRepeat(asynUser* pasynUser, epicsInt32 *value);

    /** \brief Set the number of times that the trigger should be triggered. [PV record setter]
     *
	 *  Asyn Reason: TriggerRepeat\n
	 *  PV Record suffix: TRGR\n
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setReferenceTriggerRepeat(asynUser* pasynUser, epicsInt32 value);

    /** \brief  [PV record getter]
     *
	 *  Asyn Reason: TriggerRepeat\n
	 *  PV Record suffix: TRGR\n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getReferenceTriggerRepeat(asynUser* pasynUser, epicsInt32 *value);

    /** Set the enabled status of the entity. [PV record setter]
     *
     */
    virtual ndsStatus setEnabled(asynUser* pasynUser, epicsInt32 value);

    /** \brief [PV record setter]
     *
     *  Asyn Reason: Enabled\n
     *  PV Record suffix: ENBL\n
     *
     *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus getEnabled(asynUser* pasynUser, epicsInt32 *value);

    /** \brief [PV record setter]
     *
     *  Asyn Reason: Range\n
     *  PV Record suffix: RNG\n
     *
     *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setRange(asynUser* pasynUser, epicsInt32 value);

    /** \brief Range [PV record getter]
     *
     *  Asyn Reason: Range\n
     *  PV Record suffix: RNG\n
     *
     *  This function will be called when EPICS record's is read.
     */
    virtual ndsStatus getRange(asynUser* pasynUser, epicsInt32 *value);

    /** Stub implementation to handler states correctly
     *
     */
    virtual ndsStatus getInt32(asynUser *pasynUser, epicsInt32* value);
    /** Stub implementation to handler states correctly
     *
     */
    virtual ndsStatus getFloat64(asynUser *pasynUser, epicsFloat64* value);
    /** Stub implementation to handler states correctly
     *
     */
    virtual ndsStatus getOctet(asynUser *pasynUser, char *data, size_t maxchars, size_t *nbytesTransfered, int *eomReason);
    /** Stub implementation to handler states correctly
     *
     */
    virtual ndsStatus getInt8Array(asynUser* pasynUser, epicsInt8 *outArray, size_t nelem,  size_t *nIn);
    /** Stub implementation to handler states correctly
     *
     */
    virtual ndsStatus getInt16Array(asynUser* pasynUser, epicsInt16 *outArray, size_t nelem,  size_t *nIn);
    /** Stub implementation to handler states correctly
     *
     */
    virtual ndsStatus getInt32Array(asynUser* pasynUser, epicsInt32 *outArray, size_t nelem,  size_t *nIn);
    /** Stub implementation to handler states correctly
     *
     */
    virtual ndsStatus getFloat32Array(asynUser* pasynUser, epicsFloat32 *outArray, size_t nelem,  size_t *nIn);
    /** Stub implementation to handler states correctly
     *
     */
    virtual ndsStatus getFloat64Array(asynUser* pasynUser, epicsFloat64 *outArray, size_t nelem,  size_t *nIn);

    /** Stub implementation to handler states correctly
     *
     */
    virtual ndsStatus getUInt32Digital(asynUser *pasynUser, epicsUInt32 *value, epicsUInt32 mask);

    virtual ndsStatus handleStartMsg(asynUser *pasynUser, const nds::Message& value);

    virtual ndsStatus handleStopMsg(asynUser *pasynUser, const nds::Message& value);


    /** \brief Range [PV record getter]
     *
     *  Asyn Reason: ValueUInt32D\n
     *  PV Record suffix: RNG\n
     *
     *  This function will be called when EPICS record's is read.
     */
    virtual ndsStatus getValueUInt32Digital(asynUser *pasynUser, epicsUInt32 *value, epicsUInt32 mask);

    /** \brief [PV record setter]
     *
     *  Asyn Reason: ValueUInt32D\n
     *  PV Record suffix: RNG\n
     *
     *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setValueUInt32Digital(asynUser *pasynUser, epicsUInt32 value, epicsUInt32 mask);

    /** [PV record setter]
      *  Asyn Reason: DIODirection
      *  PV Record: DIR
      *
      *  For general purpose input/output digital channels,
      *  specifies whether the channel is an input or an output channel.
      *  Values are:
      *  0   IN  Input channel.
      *  1   OUT Output channel.
      *
      */
     virtual ndsStatus setDIODirection(asynUser* pasynUser, epicsInt32 value);

     /** [PV record getter]
      *  Asyn Reason: DIODirection
      *  PV Record:
      */
     virtual ndsStatus getDIODirection(asynUser* pasynUser, epicsInt32 *value);

     /** \brief Set the type of input: differential or single-ended. [PV record setter]
      *  Asyn Reason: Differential \n
      *  PV Record: DIFF \n
      */
     virtual ndsStatus setDifferential(asynUser* pasynUser, epicsInt32 value);

     /** \brief Get the type of input [PV record getter]
      *  Asyn Reason: Differential \n
      *  PV Record: - \n
      */
     virtual ndsStatus getDifferential(asynUser* pasynUser, epicsInt32 *value);

     /** \brief Specify whether or not to ground the input. [PV record setter]
      *
      *  Asyn Reason: Ground\n
      *  PV Record: GND\n
      */
     virtual ndsStatus setGround(asynUser* pasynUser, epicsInt32 value);

     /** \brief Get current input state. [PV record getter]
      *
      *  Asyn Reason: Ground \n
      *  PV Record: -\n
      */
     virtual ndsStatus getGround(asynUser* pasynUser, epicsInt32 *value);

private:
	/// @cond NDS_INTERNALS

	FunctionalChannelState* _funcState;
	/// @endcond
};

}

#endif /* NDSBASECHANNEL_H_ */
