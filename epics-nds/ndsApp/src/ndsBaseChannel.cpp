/*
 * ndsBaseChannel.cpp
 *
 *  Created on: 17. avg. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#include "ndsBaseChannel.h"
#include "ndsChannelStates.h"
#include "ndsChannelStateMachine.h"

using namespace nds;

ChannelStateUnknown BaseChannel::stateUnknown;
ChannelStateIOCInitialization BaseChannel::stateIOCInitialization;
ChannelStateOff BaseChannel::stateOff;
ChannelStateOn BaseChannel::stateOn;
ChannelStateDefunct BaseChannel::stateDefunct;
ChannelStateError BaseChannel::stateError;
ChannelStateProcessing BaseChannel::stateProcessing;
ChannelStateReady BaseChannel::stateReady;
ChannelStateReset BaseChannel::stateReset;


void BaseChannel::registerStateNotificationHandlers()
{
    registerOnEnterStateHandler(CHANNEL_STATE_UNKNOWN,
            boost::bind(&BaseChannel::stateChangeHandler, this, _1, _2));
    registerOnEnterStateHandler(CHANNEL_STATE_IOC_INITIALIZATION,
            boost::bind(&BaseChannel::stateChangeHandler, this, _1, _2));
    registerOnEnterStateHandler(CHANNEL_STATE_OFF,
            boost::bind(&BaseChannel::stateChangeHandler, this, _1, _2));
    registerOnEnterStateHandler(CHANNEL_STATE_ON,
            boost::bind(&BaseChannel::stateChangeHandler, this, _1, _2));
    registerOnEnterStateHandler(CHANNEL_STATE_PROCESSING,
            boost::bind(&BaseChannel::stateChangeHandler, this, _1, _2));
    registerOnEnterStateHandler(CHANNEL_STATE_ERROR,
            boost::bind(&BaseChannel::stateChangeHandler, this, _1, _2));
    registerOnEnterStateHandler(CHANNEL_STATE_DEFUNCT,
            boost::bind(&BaseChannel::stateChangeHandler, this, _1, _2));
    registerOnEnterStateHandler(CHANNEL_STATE_READY,
            boost::bind(&BaseChannel::stateChangeHandler, this, _1, _2));
    registerOnEnterStateHandler(CHANNEL_STATE_RESETTING,
            boost::bind(&BaseChannel::stateChangeHandler, this, _1, _2));
}

BaseChannel::BaseChannel():
 _device(0)
, _trigger(new nds::Trigger(_device))
,_pauseTrigger(new nds::Trigger(_device))
,_referenceTrigger(new nds::Trigger(_device))
{
	// TODO Auto-generated constructor stub
	setMachineState( CHANNEL_STATE_UNKNOWN );
//	_activeState= &stateEnabled;

	registerOnLeaveStateHandler(CHANNEL_STATE_UNKNOWN, boost::bind(&BaseChannel::logLeaveState, this, _1, _2) );
	registerOnLeaveStateHandler(CHANNEL_STATE_IOC_INITIALIZATION, boost::bind(&BaseChannel::logLeaveState, this, _1, _2) );
	registerOnLeaveStateHandler(CHANNEL_STATE_OFF, boost::bind(&BaseChannel::logLeaveState, this, _1, _2) );
	registerOnLeaveStateHandler(CHANNEL_STATE_ON, boost::bind(&BaseChannel::logLeaveState, this, _1, _2) );
	registerOnLeaveStateHandler(CHANNEL_STATE_PROCESSING, boost::bind(&BaseChannel::logLeaveState, this, _1, _2) );
	registerOnLeaveStateHandler(CHANNEL_STATE_DEGRADED, boost::bind(&BaseChannel::logLeaveState, this, _1, _2) );
	registerOnLeaveStateHandler(CHANNEL_STATE_ERROR, boost::bind(&BaseChannel::logLeaveState, this, _1, _2) );
    registerOnLeaveStateHandler(CHANNEL_STATE_READY, boost::bind(&BaseChannel::logLeaveState, this, _1, _2) );
    registerOnLeaveStateHandler(CHANNEL_STATE_RESETTING, boost::bind(&BaseChannel::logLeaveState, this, _1, _2) );


    // Registration of the handlers for the messaging mechanism
    registerMessageWriteHandler(
        "START",                             /// Name of the message type which will be handled by this handler
        boost::bind(
            &BaseChannel::handleStartMsg, /// Address of the handler
            this,                            /// Object which owns the handler
            _1,_2)                           /// Stub functors (see boost::bind documentation for details.)
    );

    // Registration of the handlers for the messaging mechanism
    registerMessageWriteHandler(
        "STOP",                             /// Name of the message type which will be handled by this handler
        boost::bind(
            &BaseChannel::handleStopMsg, /// Address of the handler
            this,                            /// Object which owns the handler
            _1,_2)                           /// Stub functors (see boost::bind documentation for details.)
    );

    registerStateNotificationHandlers();
}

BaseChannel::~BaseChannel()
{
    if ( _trigger )
        delete  _trigger;
    if ( _pauseTrigger )
        delete _pauseTrigger;
    if ( _referenceTrigger )
        delete _referenceTrigger;
}


FunctionalChannelState* BaseChannel::getMachineState(ChannelStates state)
{
	switch( state )
	{
	case CHANNEL_STATE_IOC_INITIALIZATION:
		return &stateIOCInitialization;
		break;
	case CHANNEL_STATE_OFF:
		return &stateOff;
	case CHANNEL_STATE_ON:
		return &stateOn;
	case CHANNEL_STATE_PROCESSING:
		return &stateProcessing;
	case CHANNEL_STATE_ERROR:
		return &stateError;
    case CHANNEL_STATE_RESETTING:
        return &stateReset;
	case CHANNEL_STATE_DEFUNCT:
		return &stateDefunct;
	case CHANNEL_STATE_UNKNOWN:
	default:
		return &stateUnknown;
	}

}

void BaseChannel::setMachineState(ChannelStates state)
{
	_funcState = getMachineState( state );
}

ndsStatus BaseChannel::on()
{
    NDS_WRN("ON request for channel object is deprecated.");
    _funcState->on(this);
	return ndsError;
}

ndsStatus BaseChannel::off()
{
    _funcState->off(this);
    return ndsError;
}

ndsStatus BaseChannel::start()
{
	return _funcState->start(this);
}

ndsStatus BaseChannel::stop()
{
	return _funcState->off(this);
}

ndsStatus BaseChannel::error()
{
	return _funcState->error(this);
}

ndsStatus BaseChannel::ready()
{
	return _funcState->ready(this);
}

ndsStatus BaseChannel::enable()
{
    return _funcState->on(this);
}

ndsStatus BaseChannel::disable()
{
    return _funcState->off(this);
}

ndsStatus BaseChannel::reset()
{
    return _funcState->reset(this);
}

ChannelStates BaseChannel::getCurrentState()
{
	return _funcState->getState();
}

ndsStatus BaseChannel::logLeaveState(ChannelStates state, ChannelStates requestedState)
{
//	NDS_TRC("%s: Leaving state: %s",
//			toString().c_str(),
//			ChannelState::toString(state).c_str() );
	return ndsSuccess;
}

ndsStatus BaseChannel::setIOCInitialization()
{
	NDS_TRC("BaseChannel::setIOCInitialization");
	return _funcState->startIOCinit(this);
}

ndsStatus BaseChannel::setIOCInitialized()
{
	NDS_TRC("BaseChannel::setIOCInitialized");
    return _funcState->off(this);
}

ndsStatus BaseChannel::setState(asynUser* pasynUser, epicsInt32 value)
{
	return ndsError;
}

ndsStatus BaseChannel::getState(asynUser* pasynUser, epicsInt32 *value)
{
	*value = (epicsInt32)getCurrentState();
	return ndsSuccess;
}

ndsStatus BaseChannel::stateChangeHandler(nds::ChannelStates, nds::ChannelStates requestedState)
{
	NDS_DBG("BaseChannel::stateChangeHandler %s: Entering to state: %s",
			toString().c_str(),
			ChannelBaseState::toString(requestedState).c_str() );

	doCallbacksInt32(getCurrentState(), _interruptIdState, _portAddr);
	return ndsSuccess;
}

ndsStatus BaseChannel::registerHandlers(PVContainers* pvContainers)
{
	Base::registerHandlers(pvContainers);

	//NDS_PV_REGISTER_("", &BaseChannel::set, &BaseChannel::get, &_interruptId);
	NDS_PV_REGISTER_INT32("Trigger", &BaseChannel::setTrigger, &BaseChannel::getTrigger, &_interruptIdTrigger);
	NDS_PV_REGISTER_OCTET("TriggerCondition", &BaseChannel::setTriggerCondition, &BaseChannel::getTriggerCondition, &_interruptIdTriggerCondition);
	NDS_PV_REGISTER_OCTET("TriggerTarget",  &BaseChannel::setTriggerTarget, &BaseChannel::getTriggerTarget, &_interruptIdTriggerTarget);
	NDS_PV_REGISTER_INT32("TriggerDelay", &BaseChannel::setTriggerDelay, &BaseChannel::getTriggerDelay, &_interruptIdTriggerDelay);
	NDS_PV_REGISTER_INT32("TriggerRepeat",  &BaseChannel::setTriggerRepeat, &BaseChannel::getTriggerRepeat, &_interruptIdTriggerRepeat);
	NDS_PV_REGISTER_INT32("TriggerType",    &BaseChannel::setTriggerType, &BaseChannel::getTriggerType, &_interruptIdTriggerType);
	NDS_PV_REGISTER_INT32("TriggerRouting", &BaseChannel::setTriggerRouting, &BaseChannel::getTriggerRouting, &_interruptIdTriggerRouting);
	NDS_PV_REGISTER_INT32("TriggerPolarity", &BaseChannel::setTriggerPolarity, &BaseChannel::getTriggerPolarity, &_interruptIdTriggerPolarity);

	NDS_PV_REGISTER_INT32("ReferenceTrigger", &BaseChannel::setReferenceTrigger, &BaseChannel::getReferenceTrigger, &_interruptIdReferenceTrigger);
	NDS_PV_REGISTER_OCTET("ReferenceTriggerCondition", &BaseChannel::setReferenceTriggerCondition, &BaseChannel::getReferenceTriggerCondition, &_interruptIdReferenceTriggerCondition);
	NDS_PV_REGISTER_OCTET("ReferenceTriggerTarget", &BaseChannel::setReferenceTriggerTarget, &BaseChannel::getReferenceTriggerTarget, &_interruptIdReferenceTriggerTarget);
	NDS_PV_REGISTER_INT32("ReferenceTriggerDelay", &BaseChannel::setReferenceTriggerDelay, &BaseChannel::getReferenceTriggerDelay, &_interruptIdReferenceTriggerDelay);
	NDS_PV_REGISTER_INT32("ReferenceTriggerRepeat", &BaseChannel::setReferenceTriggerRepeat, &BaseChannel::getReferenceTriggerRepeat, &_interruptIdReferenceTriggerRepeat);
	NDS_PV_REGISTER_INT32("ReferenceTriggerType", &BaseChannel::setReferenceTriggerType, &BaseChannel::getReferenceTriggerType, &_interruptIdReferenceTriggerType);
	NDS_PV_REGISTER_INT32("ReferenceTriggerRouting", &BaseChannel::setReferenceTriggerRouting, &BaseChannel::getReferenceTriggerRouting, &_interruptIdReferenceTriggerRouting);
	NDS_PV_REGISTER_INT32("ReferenceTriggerPolarity", &BaseChannel::setReferenceTriggerPolarity, &BaseChannel::getReferenceTriggerPolarity, &_interruptIdReferenceTriggerPolarity);

	NDS_PV_REGISTER_INT32("PauseTrigger", &BaseChannel::setPauseTrigger, &BaseChannel::getPauseTrigger, &_interruptIdPauseTrigger);
	NDS_PV_REGISTER_OCTET("PauseTriggerCondition", &BaseChannel::setPauseTriggerCondition, &BaseChannel::getPauseTriggerCondition, &_interruptIdPauseTriggerCondition);
	NDS_PV_REGISTER_OCTET("PauseTriggerTarget", &BaseChannel::setPauseTriggerTarget, &BaseChannel::getPauseTriggerTarget, &_interruptIdPauseTriggerTarget);
	NDS_PV_REGISTER_INT32("PauseTriggerDelay", &BaseChannel::setPauseTriggerDelay, &BaseChannel::getPauseTriggerDelay, &_interruptIdPauseTriggerDelay);
	NDS_PV_REGISTER_INT32("PauseTriggerRepeat", &BaseChannel::setPauseTriggerRepeat, &BaseChannel::getPauseTriggerRepeat, &_interruptIdPauseTriggerRepeat);
	NDS_PV_REGISTER_INT32("PauseTriggerType", &BaseChannel::setPauseTriggerType, &BaseChannel::getPauseTriggerType, &_interruptIdPauseTriggerType);
	NDS_PV_REGISTER_INT32("PauseTriggerRouting", &BaseChannel::setPauseTriggerRouting, &BaseChannel::getPauseTriggerRouting, &_interruptIdPauseTriggerRouting);
	NDS_PV_REGISTER_INT32("PauseTriggerPolarity", &BaseChannel::setPauseTriggerPolarity, &BaseChannel::getPauseTriggerPolarity, &_interruptIdPauseTriggerPolarity);

	NDS_PV_REGISTER_OCTET("ConvertClockBase",     &BaseChannel::setConvertClockBase,     &BaseChannel::getConvertClockBase, &_interruptIdConvertClockBase);
	NDS_PV_REGISTER_OCTET("ConvertClockTarget",   &BaseChannel::setConvertClockTarget,   &BaseChannel::getConvertClockTarget, &_interruptIdConvertClockTarget);
	NDS_PV_REGISTER_INT32("ConvertClockPolarity", &BaseChannel::setConvertClockPolarity, &BaseChannel::getConvertClockPolarity, &_interruptIdConvertClockPolarity);
	NDS_PV_REGISTER_INT32("ConvertClockDelay",    &BaseChannel::setConvertClockDelay,    &BaseChannel::getConvertClockDelay, &_interruptIdConvertClockDelay);
	NDS_PV_REGISTER_INT32("ConvertClockRouting",  &BaseChannel::setConvertClockRouting,  &BaseChannel::getConvertClockRouting, &_interruptIdConvertClockRouting);

	NDS_PV_REGISTER_INT32("ClockSource", &BaseChannel::setClockSource, &BaseChannel::getClockSource, &_interruptIdClockSource);
	NDS_PV_REGISTER_FLOAT64("ClockFrequency", &BaseChannel::setClockFrequency, &BaseChannel::getClockFrequency, &_interruptIdClockFrequency);
	NDS_PV_REGISTER_INT32("ClockMultiplier", &BaseChannel::setClockMultiplier, &BaseChannel::getClockMultiplier, &_interruptIdClockMultiplier);
	NDS_PV_REGISTER_OCTET("ClockTarget",  &BaseChannel::setClockTarget,  &BaseChannel::getClockTarget,  &_interruptIdClockTarget);
	NDS_PV_REGISTER_INT32("ClockRouting", &BaseChannel::setClockRouting, &BaseChannel::getClockRouting, &_interruptIdClockRouting);
	NDS_PV_REGISTER_INT32("ClockPolarity", &BaseChannel::setClockPolarity, &BaseChannel::getClockPolarity, &_interruptIdClockPolarity);

	NDS_PV_REGISTER_INT32("ProcessingMode", &BaseChannel::setProcessingMode, &BaseChannel::getProcessingMode, &_interruptIdProcessingMode);
	NDS_PV_REGISTER_INT32("SamplesCount", &BaseChannel::setSamplesCount,   &BaseChannel::getSamplesCount, &_interruptIdSamplesCount);

    NDS_PV_REGISTER_INT32("ClockDivisor", &BaseChannel::setClockDivisor,   &BaseChannel::getClockDivisor, &_interruptIdClockDivisor);

    NDS_PV_REGISTER_INT32("Range", &BaseChannel::setRange, &BaseChannel::getRange, &_interruptIdRange);

    NDS_PV_REGISTER_UINT32D("ValueUInt32Digital", &BaseChannel::setValueUInt32Digital, &BaseChannel::getValueUInt32Digital, &_interruptIdValueUInt32Digital);

    NDS_PV_REGISTER_INT32("DIODirection", &BaseChannel::setDIODirection, &BaseChannel::getDIODirection, &_interruptIdDIODirection);
    NDS_PV_REGISTER_INT32("Differential", &BaseChannel::setDifferential, &BaseChannel::getDifferential, &_interruptIdDifferential);
    NDS_PV_REGISTER_INT32("Ground", &BaseChannel::setGround, &BaseChannel::getGround, &_interruptIdGround);


	return ndsSuccess;
}


ndsStatus BaseChannel::setTrigger(asynUser *pasynUser, epicsInt32 value)
{
	_isTriggered=1;
	return ndsSuccess;
}

ndsStatus BaseChannel::getTrigger(asynUser *pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	return ndsSuccess;
}

ndsStatus BaseChannel::onTriggerConditionParsed(asynUser *pasynUser, const Trigger& trigger)
{
    return ndsSuccess;
}

ndsStatus BaseChannel::setTriggerCondition(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransferead)
{
    nds::Trigger *trigger = new nds::Trigger(_device);
	*nbytesTransferead = numchars;
	std::string str(data);
	if (trigger->parseTriggerCondition(data) == ndsSuccess)
	{
	    if ( onTriggerConditionParsed(pasynUser, *trigger) == ndsSuccess )
	    {
	        if( _trigger )
	            delete _trigger;
	        _trigger = trigger;
	        doCallbacksOctetStr(str, ASYN_EOM_END, _interruptIdTriggerCondition );
	        return ndsSuccess;
	    }else
	    {
	        NDS_ERR("Trigger's condition handler error.");
	    }
	}else
	{
	    NDS_ERR("Trigger's condition parsing error.");
	}
	return ndsError;
}

ndsStatus BaseChannel::getTriggerCondition(asynUser *pasynUser, char *data, size_t maxchars, size_t *nbytesTransferead, int *eomReason)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	return ndsError;
}

ndsStatus BaseChannel::setTriggerDelay(asynUser* pasynUser, epicsInt32 value)
{
	_TriggerDelay = value;
	return ndsSuccess;
}

ndsStatus BaseChannel::getTriggerDelay(asynUser* pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	*value = _TriggerDelay;
	return ndsSuccess;
}

ndsStatus BaseChannel::setTriggerRepeat(asynUser* pasynUser, epicsInt32 value)
{
	_TriggerRepeat = value;
	return ndsSuccess;
}

ndsStatus BaseChannel::getTriggerRepeat(asynUser* pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	*value = _TriggerRepeat;
	return ndsSuccess;
}

ndsStatus BaseChannel::setTriggerType(asynUser *pasynUser, epicsInt32 value)
{
	_TriggerType = value;
	return ndsSuccess;
}

ndsStatus BaseChannel::getTriggerType(asynUser *pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	*value = _TriggerRepeat;
	return ndsSuccess;
}

ndsStatus BaseChannel::setTriggerRouting(asynUser *pasynUser, epicsInt32 value)
{
	return ndsSuccess;
}

ndsStatus BaseChannel::getTriggerRouting(asynUser *pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	return ndsSuccess;
}

ndsStatus BaseChannel::setTriggerTarget(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransferead)
{
	return findSource(_TriggerTargetMap, data, &BaseChannel::setTriggerTargetId );
}

ndsStatus BaseChannel::getTriggerTarget(asynUser *pasynUser, char *data, size_t maxchars, size_t *nbytesTransferead, int *eomReason)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	return ndsSuccess;
}

ndsStatus BaseChannel::setTriggerPolarity(asynUser *pasynUser, epicsInt32 value)
{
	return ndsSuccess;
}

ndsStatus BaseChannel::getTriggerPolarity(asynUser *pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	return ndsSuccess;
}

ndsStatus BaseChannel::setConvertClockBase(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransferead)
{
	return findSource(_ConvertClockBaseMap, data, &BaseChannel::setConvertClockBaseId );
}

ndsStatus BaseChannel::getConvertClockBase(asynUser *pasynUser, char *data, size_t maxchars, size_t *nbytesTransferead, int *eomReason)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	return ndsSuccess;
}

ndsStatus BaseChannel::setConvertClockPolarity(asynUser *pasynUser, epicsInt32 value)
{
	return ndsSuccess;
}

ndsStatus BaseChannel::getConvertClockPolarity(asynUser *pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	return ndsSuccess;
}

ndsStatus BaseChannel::setConvertClockDelay(asynUser *pasynUser, epicsInt32 value)
{
	return ndsSuccess;
}

ndsStatus BaseChannel::getConvertClockDelay(asynUser *pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	return ndsSuccess;
}

ndsStatus BaseChannel::setConvertClockTarget(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransferead)
{
	return findSource(_ConvertClockTargetMap, data, &BaseChannel::setConvertClockTargetId );
}

ndsStatus BaseChannel::getConvertClockTarget(asynUser *pasynUser, char *data, size_t maxchars, size_t *nbytesTransferead, int *eomReason)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	return ndsSuccess;
}

ndsStatus BaseChannel::setConvertClockRouting(asynUser *pasynUser, epicsInt32 value)
{
	return ndsSuccess;
}

ndsStatus BaseChannel::getConvertClockRouting(asynUser *pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	return ndsSuccess;
}

ndsStatus BaseChannel::setClockRouting(asynUser *pasynUser, epicsInt32 value)
{
	return ndsSuccess;
}

ndsStatus BaseChannel::getClockRouting(asynUser *pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	return ndsSuccess;
}

ndsStatus BaseChannel::setClockTarget(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransferead)
{
	return findSource(_ClockTargetMap, data, &BaseChannel::setClockTargetId );
}

ndsStatus BaseChannel::getClockTarget(asynUser *pasynUser, char *data, size_t maxchars, size_t *nbytesTransferead, int *eomReason)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	return ndsSuccess;
}

ndsStatus BaseChannel::setPauseTrigger(asynUser *pasynUser, epicsInt32 value)
{
	return ndsSuccess;
}

ndsStatus BaseChannel::getPauseTrigger(asynUser *pasynUser, epicsInt32* value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	return ndsSuccess;
}

ndsStatus BaseChannel::onPauseTriggerConditionParsed(asynUser *pasynUser, const nds::Trigger& trigger)
{
    return ndsSuccess;
}

ndsStatus BaseChannel::setPauseTriggerCondition(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransferead)
{

	nds::Trigger *trigger = new nds::Trigger(_device);
    *nbytesTransferead = numchars;
    std::string str(data);
    if (trigger->parseTriggerCondition(data) == ndsSuccess)
    {
        if ( onPauseTriggerConditionParsed(pasynUser, *trigger) == ndsSuccess )
        {
            if (_pauseTrigger)
                delete _pauseTrigger;
            _pauseTrigger = trigger;
            doCallbacksOctetStr(str, ASYN_EOM_END, _interruptIdPauseTriggerCondition );
            return ndsSuccess;
        }else
        {
            NDS_ERR("Trigger's condition handler error.");
        }
    }else
    {
        NDS_ERR("Trigger's condition parsing error.");
    }
    return ndsError;
}

ndsStatus BaseChannel::getPauseTriggerCondition(asynUser *pasynUser, char *data, size_t maxchars, size_t *nbytesTransferead, int *eomReason)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	return ndsSuccess;
}

ndsStatus BaseChannel::setPauseTriggerDelay(asynUser* pasynUser, epicsInt32 value)
{
	_TriggerDelay = value;
	return ndsSuccess;
}

ndsStatus BaseChannel::getPauseTriggerDelay(asynUser* pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	*value = _TriggerDelay;
	return ndsSuccess;
}

ndsStatus BaseChannel::setPauseTriggerRepeat(asynUser* pasynUser, epicsInt32 value)
{
	_TriggerRepeat = value;
	return ndsSuccess;
}

ndsStatus BaseChannel::getPauseTriggerRepeat(asynUser* pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	*value = _TriggerRepeat;
	return ndsSuccess;
}

ndsStatus BaseChannel::setPauseTriggerType(asynUser *pasynUser, epicsInt32 value)
{
	_TriggerType = value;
	return ndsSuccess;
}

ndsStatus BaseChannel::getPauseTriggerType(asynUser *pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	*value = _TriggerRepeat;
	return ndsSuccess;
}

ndsStatus BaseChannel::setPauseTriggerRouting(asynUser *pasynUser, epicsInt32 value)
{
	return ndsSuccess;
}

ndsStatus BaseChannel::getPauseTriggerRouting(asynUser *pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	return ndsSuccess;
}

ndsStatus BaseChannel::setPauseTriggerTarget(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransferead)
{
	return ndsSuccess;
}

ndsStatus BaseChannel::getPauseTriggerTarget(asynUser *pasynUser, char *data, size_t maxchars, size_t *nbytesTransferead, int *eomReason)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	return ndsSuccess;
}

ndsStatus BaseChannel::setPauseTriggerPolarity(asynUser *pasynUser, epicsInt32 value)
{
	return ndsSuccess;
}

ndsStatus BaseChannel::getPauseTriggerPolarity(asynUser *pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	return ndsSuccess;
}

ndsStatus BaseChannel::setReferenceTrigger(asynUser *pasynUser, epicsInt32 value)
{
	return ndsSuccess;
}
ndsStatus BaseChannel::getReferenceTrigger(asynUser *pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	return ndsSuccess;
}

ndsStatus BaseChannel::onReferenceTriggerConditionParsed(asynUser *pasynUser, const Trigger& trigger)
{
    return ndsSuccess;
}

ndsStatus BaseChannel::setReferenceTriggerCondition(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransferead)
{
	nds::Trigger *trigger= new nds::Trigger(_device);
    *nbytesTransferead = numchars;
    std::string str(data);
    if (trigger->parseTriggerCondition(data) == ndsSuccess)
    {
        if ( onReferenceTriggerConditionParsed(pasynUser, *trigger) == ndsSuccess )
        {
            if ( _referenceTrigger )
                delete _referenceTrigger;
            _referenceTrigger = trigger;
            doCallbacksOctetStr(str, ASYN_EOM_END, _interruptIdReferenceTriggerCondition );
            return ndsSuccess;
        }else
        {
            NDS_ERR("Trigger's condition handler error.");
        }
    }else
    {
        NDS_ERR("Trigger's condition parsing error.");
    }
    return ndsError;
}

ndsStatus BaseChannel::getReferenceTriggerCondition(asynUser *pasynUser, char *data, size_t maxchars, size_t *nbytesTransferead, int *eomReason)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	return ndsSuccess;
}

ndsStatus BaseChannel::setReferenceTriggerDelay(asynUser* pasynUser, epicsInt32 value)
{
	_TriggerDelay = value;
	return ndsSuccess;
}

ndsStatus BaseChannel::getReferenceTriggerDelay(asynUser* pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	*value = _TriggerDelay;
	return ndsSuccess;
}

ndsStatus BaseChannel::setReferenceTriggerRepeat(asynUser* pasynUser, epicsInt32 value)
{
	_TriggerRepeat = value;
	return ndsSuccess;
}
ndsStatus BaseChannel::getReferenceTriggerRepeat(asynUser* pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	*value = _TriggerRepeat;
	return ndsSuccess;
}

ndsStatus BaseChannel::setReferenceTriggerType(asynUser *pasynUser, epicsInt32 value)
{
	_TriggerType = value;
	return ndsSuccess;
}

ndsStatus BaseChannel::getReferenceTriggerType(asynUser *pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	*value = _TriggerRepeat;
	return ndsSuccess;
}

ndsStatus BaseChannel::setReferenceTriggerRouting(asynUser *pasynUser, epicsInt32 value)
{
	return ndsSuccess;
}

ndsStatus BaseChannel::getReferenceTriggerRouting(asynUser *pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	return ndsSuccess;
}

ndsStatus BaseChannel::setReferenceTriggerTarget(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransferead)
{
	return findSource(_ReferenceTriggerTargetMap, data, &BaseChannel::setReferenceTriggerTargetId );
}

ndsStatus BaseChannel::getReferenceTriggerTarget(asynUser *pasynUser, char *data, size_t maxchars, size_t *nbytesTransferead, int *eomReason)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	return ndsSuccess;
}

ndsStatus BaseChannel::setReferenceTriggerPolarity(asynUser *pasynUser, epicsInt32 value)
{
	return ndsSuccess;
}
ndsStatus BaseChannel::getReferenceTriggerPolarity(asynUser *pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	return ndsSuccess;
}

ndsStatus BaseChannel::setClockSource(asynUser* pasynUser, epicsInt32 value)
{
	_ClockSource = value;
	return ndsSuccess;
}
ndsStatus BaseChannel::getClockSource(asynUser* pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	*value = _ClockSource;
	return ndsSuccess;
}

ndsStatus BaseChannel::setClockFrequency(asynUser* pasynUser, epicsFloat64 value)
{
	_ClockFrequency =  value;
	return ndsSuccess;
}
ndsStatus BaseChannel::getClockFrequency(asynUser* pasynUser, epicsFloat64 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	*value =  _ClockFrequency;
	return ndsSuccess;
}

ndsStatus BaseChannel::setClockMultiplier(asynUser* pasynUser, epicsInt32 value)
{
	_ClockMultiplier = value;
	return ndsSuccess;
}
ndsStatus BaseChannel::getClockMultiplier(asynUser* pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	*value = _ClockMultiplier;
	return ndsSuccess;
}

ndsStatus BaseChannel::setClockDivisor(asynUser* pasynUser, epicsInt32 value)
{
    _ClockDivisor = value;
    return ndsSuccess;
}

ndsStatus BaseChannel::getClockDivisor(asynUser* pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    *value = _ClockDivisor;
    return ndsSuccess;
}

ndsStatus BaseChannel::setRange(asynUser* pasynUser, epicsInt32 value)
{
    _Range = value;
    return ndsSuccess;
}

ndsStatus BaseChannel::getRange(asynUser* pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    *value = _Range;
    return ndsSuccess;
}


ndsStatus BaseChannel::setClockSourceId(epicsInt32 value)
{
	return ndsSuccess;
}
ndsStatus BaseChannel::setClockTargetId(epicsInt32 value)
{
	return ndsSuccess;
}
ndsStatus BaseChannel::setConvertClockBaseId(epicsInt32 value)
{
	return ndsSuccess;
}
ndsStatus BaseChannel::setConvertClockTargetId(epicsInt32 value)
{
	return ndsSuccess;
}
ndsStatus BaseChannel::setTriggerSourceId(epicsInt32 value)
{
	return ndsSuccess;
}
ndsStatus BaseChannel::setTriggerTargetId(epicsInt32 value)
{
	return ndsSuccess;
}
ndsStatus BaseChannel::setReferenceTriggerSourceId(epicsInt32 value)
{
	return ndsSuccess;
}
ndsStatus BaseChannel::setReferenceTriggerTargetId(epicsInt32 value)
{
	return ndsSuccess;
}
ndsStatus BaseChannel::setPauseTriggerSourceId(epicsInt32 value)
{
	return ndsSuccess;
}
ndsStatus BaseChannel::setPauseTriggerTargetId(epicsInt32 value)
{
	return ndsSuccess;
}

void BaseChannel::addTriggerSource(const std::string& source, int value)
{
	_TriggerSourceMap.insert(
			SourceMap::value_type(source, value) );
}
void BaseChannel::addTriggerTarget(const std::string& source, int value)
{
	_TriggerTargetMap.insert(
				SourceMap::value_type(source, value) );
}

void BaseChannel::addPauseTriggerSource(const std::string& source, int value)
{
	_PauseTriggerSourceMap.insert(
					SourceMap::value_type(source, value) );
}
void BaseChannel::addPauseTriggerTarget(const std::string& source, int value)
{
	_PauseTriggerTargetMap.insert(
					SourceMap::value_type(source, value) );
}

void BaseChannel::addReferenceTriggerSource(const std::string& source, int value)
{
	_ReferenceTriggerSourceMap.insert(
					SourceMap::value_type(source, value) );
}
void BaseChannel::addReferenceTriggerTarget(const std::string& source, int value)
{
	_ReferenceTriggerTargetMap.insert(
					SourceMap::value_type(source, value) );
}

void BaseChannel::addClockSource(const std::string& source, int value)
{
	_ClockSourceMap.insert(
					SourceMap::value_type(source, value) );
}
void BaseChannel::addClockTarget(const std::string& source, int value)
{
	_ClockTargetMap.insert(
					SourceMap::value_type(source, value) );
}

void BaseChannel::addConvertClockBase(const std::string& source, int value)
{
	_ConvertClockBaseMap.insert(
					SourceMap::value_type(source, value) );
}
void BaseChannel::addConvertClockTarget(const std::string& source, int value)
{
	_ConvertClockTargetMap.insert(
					SourceMap::value_type(source, value) );
}

ndsStatus BaseChannel::findSource(const SourceMap& map, const std::string& source,
		boost::function<ndsStatus (BaseChannel*, epicsInt32)> handler )
{
	SourceMap::const_iterator itr =  map.find(source);
	if ( map.end() != itr )
	{
		return handler(this, itr->second);
	}
	return ndsError;
}

ndsStatus BaseChannel::setProcessingMode(asynUser *pasynUser, epicsInt32 value)
{
	_ProcessingMode = value;
	return ndsSuccess;
}

ndsStatus BaseChannel::setSamplesCount(asynUser *pasynUser, epicsInt32 value)
{
	_SamplesCount = value;
	return ndsSuccess;
}

ndsStatus BaseChannel::getSamplesCount(asynUser *pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    *value = _SamplesCount;
    return ndsSuccess;
}

ndsStatus BaseChannel::getProcessingMode(asynUser *pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    *value = _ProcessingMode;
    return ndsSuccess;
}


ndsStatus BaseChannel::setClockPolarity(asynUser *pasynUser, epicsInt32 value)
{
	_ClockPolarity = value;
	return ndsSuccess;
}

ndsStatus BaseChannel::getClockPolarity(asynUser *pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	*value = _ClockPolarity;
	return ndsSuccess;
}

ndsStatus BaseChannel::getInt32(asynUser *pasynUser, epicsInt32* value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    return Base::getInt32(pasynUser, value);
}

ndsStatus BaseChannel::getFloat64(asynUser *pasynUser, epicsFloat64* value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    return Base::getFloat64(pasynUser, value);
}

ndsStatus BaseChannel::getOctet(asynUser *pasynUser, char *data, size_t maxchars, size_t *nbytesTransfered, int *eomReason)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    return Base::getOctet(pasynUser, data, maxchars, nbytesTransfered, eomReason);
}

ndsStatus BaseChannel::getInt8Array(asynUser* pasynUser, epicsInt8 *outArray, size_t nelem,  size_t *nIn)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    return Base::getInt8Array(pasynUser, outArray, nelem,  nIn) ;
}

ndsStatus BaseChannel::getInt16Array(asynUser* pasynUser, epicsInt16 *outArray, size_t nelem,  size_t *nIn)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    return Base::getInt16Array(pasynUser, outArray, nelem,  nIn) ;
}

ndsStatus BaseChannel::getInt32Array(asynUser* pasynUser, epicsInt32 *outArray, size_t nelem,  size_t *nIn)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    return Base:: getInt32Array(pasynUser, outArray, nelem, nIn);
}

ndsStatus BaseChannel::getFloat32Array(asynUser* pasynUser, epicsFloat32 *outArray, size_t nelem,  size_t *nIn)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    return Base::getFloat32Array(pasynUser, outArray, nelem, nIn) ;
}

ndsStatus BaseChannel::getFloat64Array(asynUser* pasynUser, epicsFloat64 *outArray, size_t nelem,  size_t *nIn)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    return Base::getFloat64Array(pasynUser, outArray, nelem,  nIn);
}

ndsStatus BaseChannel::getUInt32Digital(asynUser *pasynUser, epicsUInt32 *value, epicsUInt32 mask)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    return Base::getUInt32Digital(pasynUser, value, mask);
}

FunctionalChannelState* BaseChannel::getCurrentStateObj()
{
    return _funcState;
}

ndsStatus BaseChannel::registerOnRequestStateHandlerForAll(nds::ChannelStates requestedState,
        StateHandler handler)
{
    for (int i = CHANNEL_STATE_DISABLED; i!= CHANNEL_STATE_last; ++i)
    {
        registerOnRequestStateHandler( (nds::ChannelStates)i, requestedState,
                handler);
    }
    return ndsSuccess;
}

ndsStatus BaseChannel::setEnabled(asynUser* pasynUser, epicsInt32 value)
{
    return Base::setEnabled(pasynUser, value);
}

ndsStatus BaseChannel::getEnabled(asynUser* pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    return Base::getEnabled(pasynUser, value);
}

ndsStatus BaseChannel::handleStartMsg(asynUser *pasynUser, const nds::Message& value)
{
    this->start();
    return ndsSuccess;
}

ndsStatus BaseChannel::handleStopMsg(asynUser *pasynUser, const nds::Message& value)
{
    this->stop();
    return ndsSuccess;
}

ndsStatus BaseChannel::getValueUInt32Digital(asynUser *pasynUser, epicsUInt32 *value, epicsUInt32 mask)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    return ndsSuccess;
}

ndsStatus BaseChannel::setValueUInt32Digital(asynUser *pasynUser, epicsUInt32 value, epicsUInt32 mask)
{
    return ndsSuccess;
}

ndsStatus BaseChannel::getDIODirection(asynUser *pasynUser, epicsInt32 *value)
{
    *value = _DIODirection;
    return ndsSuccess;
}

ndsStatus BaseChannel::setDIODirection(asynUser *pasynUser, epicsInt32 value)
{
    _DIODirection = value;
    return ndsSuccess;
}

ndsStatus BaseChannel::getDifferential(asynUser *pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    *value = _Differential;
    return ndsSuccess;
}

ndsStatus BaseChannel::setDifferential(asynUser *pasynUser, epicsInt32 value)
{
    _Differential = value;
    return ndsSuccess;
}

ndsStatus BaseChannel::getGround(asynUser *pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    *value = _Ground;
    return ndsSuccess;
}

ndsStatus BaseChannel::setGround(asynUser *pasynUser, epicsInt32 value)
{
    _Ground = value;
    return ndsSuccess;
}
