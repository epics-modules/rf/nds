/*
 * ChannelStates.cpp
 *
 *  Created on: 17. avg. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#include <string>

#include "ndsTypes.h"
#include "ndsChannelStates.h"
#include "ndsChannelStateMachine.h"
#include "ndsBaseChannel.h"

using namespace nds;


const std::string ChannelBaseState::toString(const ChannelStates& state)
{
	switch( state )
	{
	case CHANNEL_STATE_IOC_INITIALIZATION:
		return "IOC_INITIALIZATION";
		break;
	case CHANNEL_STATE_OFF:
		return "DISABLED";
	case CHANNEL_STATE_ON:
		return "ENABLED";
	case CHANNEL_STATE_PROCESSING:
		return "PROCESSING";
	case CHANNEL_STATE_DEGRADED:
		return "DEGRADED";
	case CHANNEL_STATE_ERROR:
		return "ERROR";
	case CHANNEL_STATE_DEFUNCT:
		return "DEFUNCT";
    case CHANNEL_STATE_READY:
        return "READY";
    case CHANNEL_STATE_RESETTING:
        return "RESETTING";
	case CHANNEL_STATE_UNKNOWN:
	default:
		return "UNKNOWN";
	}
}

ndsStatus FunctionalChannelState::startIOCinit(ChannelStateMachine* channel)
{
    NDS_WRN("Unexpected state requested: IOCinit, current: %s",
            ChannelBaseState::toString(this->getState()).c_str() );
    return ndsError;
}

ndsStatus FunctionalChannelState::on(ChannelStateMachine* channel)
{
	NDS_WRN("Unexpected state requested: ENABLED, current: %s",
            ChannelBaseState::toString(this->getState()).c_str());
	return ndsError;
}

ndsStatus FunctionalChannelState::off(ChannelStateMachine* channel)
{
	NDS_WRN("Unexpected state requested: DISABLED, current: %s",
            ChannelBaseState::toString(this->getState()).c_str());
	return ndsError;
}

ndsStatus FunctionalChannelState::start(ChannelStateMachine* channel)
{
	NDS_WRN("Unexpected state requested: PROCESSING, current: %s",
            ChannelBaseState::toString(this->getState()).c_str());
	return ndsError;
}

ndsStatus FunctionalChannelState::ready(ChannelStateMachine* channel)
{
	NDS_WRN("Unexpected state requested: READY, current: %s",
            ChannelBaseState::toString(this->getState()).c_str());
	return ndsError;
}

ndsStatus FunctionalChannelState::reset(ChannelStateMachine* channel)
{
    NDS_WRN("Unexpected state requested: RESETTING, current: %s",
            ChannelBaseState::toString(this->getState()).c_str());
    return ndsError;
}

ndsStatus FunctionalChannelState::defunct(ChannelStateMachine* channel)
{
    return setMachineState(channel, CHANNEL_STATE_DEFUNCT);
}

ndsStatus FunctionalChannelState::error(ChannelStateMachine* channel)
{
    return setMachineState(channel, CHANNEL_STATE_ERROR);
}

ndsStatus ChannelStateUnknown::startIOCinit(ChannelStateMachine* channel)
{
	return requestState( channel, CHANNEL_STATE_IOC_INITIALIZATION);
}

 ndsStatus ChannelStateIOCInitialization::off(ChannelStateMachine* channel)
{
	return requestState(channel, CHANNEL_STATE_OFF);
}

 ndsStatus ChannelStateOff::on(ChannelStateMachine* channel)
{
	return requestState(channel, CHANNEL_STATE_ON);
}

ndsStatus ChannelStateOff::start(ChannelStateMachine* channel)
{
    return requestState(channel, CHANNEL_STATE_PROCESSING);
}

ndsStatus ChannelStateOn::off(ChannelStateMachine* channel)
{
	return requestState(channel, CHANNEL_STATE_OFF);
}

ndsStatus ChannelStateOn::start(ChannelStateMachine* channel)
{
	return requestState(channel, CHANNEL_STATE_PROCESSING);
}

ndsStatus ChannelStateOn::reset(ChannelStateMachine* channel)
{
    return setMachineState(channel, CHANNEL_STATE_RESETTING);
}


ndsStatus ChannelStateProcessing::off(ChannelStateMachine* channel)
{
	return requestState(channel, CHANNEL_STATE_DISABLED);
}

ndsStatus ChannelStateProcessing::reset(ChannelStateMachine* channel)
{
    return setMachineState(channel, CHANNEL_STATE_RESETTING);
}


ndsStatus ChannelStateError::off(ChannelStateMachine* channel)
{
	return requestState(channel, CHANNEL_STATE_OFF);
}

ndsStatus ChannelStateError::error(ChannelStateMachine* channel)
{
	return setMachineState(channel, CHANNEL_STATE_DEFUNCT);
}

ndsStatus ChannelStateError::reset(ChannelStateMachine* channel)
{
     return setMachineState(channel, CHANNEL_STATE_RESETTING);
}


ndsStatus ChannelStateReady::off(ChannelStateMachine* channel)
{
    return requestState(channel, CHANNEL_STATE_OFF);
}

ndsStatus ChannelStateReady::on(ChannelStateMachine* channel)
{
    return requestState(channel, CHANNEL_STATE_ON);
}

ndsStatus ChannelStateReady::start(ChannelStateMachine* channel)
{
    return requestState(channel, CHANNEL_STATE_PROCESSING);
}

ndsStatus ChannelStateReady::reset(ChannelStateMachine* channel)
{
    return setMachineState(channel, CHANNEL_STATE_RESETTING);
}
