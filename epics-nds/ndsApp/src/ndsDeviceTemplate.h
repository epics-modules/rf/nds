/*
 * ndsDeviceTemplate.h
 *
 *  Created on: 1. avg. 2013
 *      Author: Slava Isaev
 */

#ifndef NDSDEVICETEMPLATE_H_
#define NDSDEVICETEMPLATE_H_

namespace nds
{

class DeviceTemplate
{
public:
    DeviceTemplate();
    virtual ~DeviceTemplate();
};

} /* namespace nds */
#endif /* NDSDEVICETEMPLATE_H_ */
