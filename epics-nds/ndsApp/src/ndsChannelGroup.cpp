/*
; * ndsChannelGroup.cpp
 *
 *  Created on: 18. jul. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#include <algorithm>
#include "ndsDebug.h"
#include "ndsDevice.h"
#include "ndsChannel.h"
#include "ndsPVContainer.h"
#include "ndsRegisterHandlersCaller.hpp"

#include "ndsChannelGroup.h"

using namespace nds;

#define NDS_CHANNEL_CLASS ChannelGroup

ChannelGroup::ChannelGroup(const std::string& name)
{
	_name = name;

    registerOnEnterStateHandler(CHANNEL_STATE_IOC_INITIALIZATION,
            boost::bind(&ChannelGroup::onIOCInit, this, _1, _2) );

    registerOnEnterStateHandler(CHANNEL_STATE_DISABLED,
            boost::bind(&ChannelGroup::switchChannelsOff, this) );

    pushbackOnEnterStateHandler(CHANNEL_STATE_PROCESSING,
            boost::bind(&ChannelGroup::startChannels, this) );
}

ChannelGroup::~ChannelGroup()
{
    NDS_STK("~ChannelGroup");
    for_each(_nodes.begin(),
             _nodes.end(),
             &deleteMapObj<ChannelContainer::value_type>
            );

    _nodes.clear();
}

void ChannelGroup::destroyPrivate()
{
    int counter=0;
    for( ChannelContainer::iterator itr = _nodes.begin();
         itr != _nodes.end();
         ++itr, ++counter)
    {
        try
        {
            itr->second->destroyPrivate();
        }catch(...)
        {
            NDS_ERR("Attempt to call destroy() on %d Channel throws an error.", counter);
        }
    }
    destroy();
}

ndsStatus ChannelGroup::registerChannel(Channel* channel)
{
  unsigned int channelNum = _nodes.size();

  _nodes.insert(
          ChannelContainer::value_type(channelNum, channel));

  return this->doPostRegistration(channel, channelNum);
}

ndsStatus ChannelGroup::doPostRegistration(Channel* channel, size_t channelNum)
{
  channel->_portAddr = channelNum;
  channel->_device   = _device;
  channel->_group    = this;
  channel->setAsynDriver(_channelPortDriver);
  channel->registerOnRequestStateHandler(CHANNEL_STATE_OFF, CHANNEL_STATE_PROCESSING,
      boost::bind(&ChannelGroup::onRequestChildProcessing, this, _1, _2));

  channel->registerOnRequestStateHandler(CHANNEL_STATE_OFF, CHANNEL_STATE_ON,
      boost::bind(&ChannelGroup::onRequestChildSwitchOn, this, _1, _2));
  channel->onRegistered();
  return ndsSuccess;
}

ndsStatus ChannelGroup::getChannel(ChannelContainer::key_type i, Channel** channel)
{
	NDS_STK("ChannelGroup::getChannel")
    *channel = NULL;
    ChannelContainer::iterator itr = _nodes.find(i);
	if ( _nodes.end() != itr )
	{
		*channel = itr->second;
		return ndsSuccess;
	}else
	{
		NDS_DBG("Group doesn't have %ld channel.", i );
	}
	return ndsError;
}

ndsStatus ChannelGroup::propagateRegisterHandlers(PVContainers* pvContainers)
{
	//Channel specific PV's handlers
	//They will be accessible from the ChannelGroup port
	RegisterHandlerCaller<ChannelContainer::value_type> caller(pvContainers);

	std::for_each(_nodes.begin(),
			_nodes.end(),
			caller );
	return ndsSuccess;
}

ndsStatus ChannelGroup::onSwitchOn()
{
    return switchChannelsOn();
}

ndsStatus ChannelGroup::switchChannelsOn()
{
    for(ChannelContainer::iterator itr=_nodes.begin(); itr!=_nodes.end(); ++itr )
    {
            itr->second->on();
    }
	return ndsSuccess;
}

ndsStatus ChannelGroup::startChannels()
{
    for(ChannelContainer::iterator itr=_nodes.begin(); itr!=_nodes.end(); ++itr )
    {
        if(itr->second->isEnabled())
            itr->second->start();
    }
    return ndsSuccess;
}

ndsStatus ChannelGroup::switchChannelsOff()
{
	std::for_each(_nodes.begin(),
			_nodes.end(),
			boost::bind(&Channel::disable,
			        boost::bind<Channel*>(&ChannelContainer::value_type::second, _1 )
            )
	);
	return ndsSuccess;
}

ndsStatus ChannelGroup::callOnAllChannels(boost::function<ndsStatus(Channel*)> func)
{
    for( ChannelContainer::iterator itr =  _nodes.begin();
            itr != _nodes.end();
            ++itr)
    {
        func(itr->second);
    }
    return ndsSuccess;
}

ndsStatus ChannelGroup::onReady(nds::ChannelStates, nds::ChannelStates requestedState)
{
    NDS_DBG("All channels are ready to process.");
    callOnAllChannels(&Channel::ready);
	return ndsSuccess;
}

ndsStatus ChannelGroup::onRequestChildSwitchOn(nds::ChannelStates, nds::ChannelStates requestedState)
{
	NDS_STK("%s", __func__);
	if ( requestedState == CHANNEL_STATE_ON )
	{
		if ( this->getCurrentState() != CHANNEL_STATE_ON )
		{
			NDS_WRN("Channel group '%s' should be started before channel. Current state: %s",
			        _name.c_str(),
			        ChannelBaseState::toString(this->getCurrentState()).c_str()
			       );
			return ndsBlockTransition;
		}
	}
	return ndsSuccess;
}

ndsStatus ChannelGroup::onRequestChildProcessing(nds::ChannelStates, nds::ChannelStates requestedState)
{
    NDS_STK("%s", __func__);
    if ( requestedState == CHANNEL_STATE_PROCESSING )
    {
        if ( this->getCurrentState() != CHANNEL_STATE_PROCESSING )
        {
            NDS_WRN("Channel group '%s' should be started before channel. Current state: %s",
                    _name.c_str(),
                    ChannelBaseState::toString(this->getCurrentState()).c_str()
                   );
            return ndsBlockTransition;
        }
    }
    return ndsSuccess;
}

ndsStatus ChannelGroup::registerHandlers(PVContainers* pvContainers)
{
	BaseChannel::registerHandlers(pvContainers);

	NDS_PV_REGISTER_INT32("State", &ChannelGroup::setState, &ChannelGroup::getState, &_interruptIdInt32);
	NDS_PV_REGISTER_INT32("Reset", &ChannelGroup::setReset, &ChannelGroup::getReset, &_interruptIdInt32);
	return ndsSuccess;
}

ndsStatus ChannelGroup::setState(asynUser* pasynUser, epicsInt32 value)
{
	return ndsSuccess;
}

ndsStatus ChannelGroup::getState(asynUser* pasynUser, epicsInt32 *value)
{
	return ndsSuccess;
}

ndsStatus ChannelGroup::setReset(asynUser* pasynUser, epicsInt32 value)
{
	return ndsSuccess;
}

ndsStatus ChannelGroup::getReset(asynUser* pasynUser, epicsInt32 *value)
{
    return ndsSuccess;
}

ndsStatus ChannelGroup::setIOCInitialization()
{
    NDS_DBG("%s", __func__);
    BaseChannel::setIOCInitialization();
    return ndsSuccess;
}

ndsStatus ChannelGroup::setIOCInitialized()
{
    NDS_DBG("%s", __func__);
    BaseChannel::setIOCInitialized();
    return ndsSuccess;
}

ndsStatus ChannelGroup::onIOCInit(nds::ChannelStates, nds::ChannelStates)
{
    for(ChannelContainer::iterator itr = _nodes.begin(); itr!=_nodes.end(); ++itr)
    {
        itr->second->setIOCInitialization();
    }
    return ndsSuccess;
}

//ndsStatus ChannelGroup::setIOCInitialized(nds::ChannelStates, nds::ChannelStates)
//{
//    for(ChannelContainer::iterator itr = _nodes.begin(); itr!=_nodes.end(); ++itr)
//    {
//        itr->second->setIOCInitialized();
//    }
//    return ndsSuccess;
//}

void ChannelGroup::printStructure(const std::string& prefix)
{
    PRINT("%sChannelGroup: %s State: %s\n", prefix.c_str(), _name.c_str(), ChannelBaseState::toString(this->getCurrentState()).c_str() );

    for( ChannelContainer::iterator itr =  _nodes.begin();
            itr != _nodes.end();
            ++itr)
    {
        itr->second->printStructure(prefix+"\t");
    }
}
