/*
 * ndsManager.h
 *
 *  Created on: 18. jul. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDSMANAGER_H_
#define NDSMANAGER_H_

#include <string>
#include <map>
#include <initHooks.h>
#include <loki/TypeTraits.h>
#include "singleton.h"

#include "ndsTypes.h"
#include "ndsAsynDriver.h"
#include "ndsAsynMacro.h"
#include "ndsDriver.h"

namespace nds
{
class ManagerBase;
class Device;
/**
 * Manger is singleton class which holds information about all registered drivers.
 * Manager is responsible for driver instance creation.
 *
 */
typedef singleton<ManagerBase> Manager;

/**
 *  NDS Manager.
 *
 *	NDS Manager stays at memory for the whole live of the IOC application
 *	and provides messaging between IOC and NDS device specific driver.
 */
class ManagerBase
{
protected:
	/// @cond NDS_INTERNALS
	typedef std::map<std::string, DriverBase*> Drivers;
	typedef std::map<std::string, AsynDriver*> AsynDevices;

	Drivers _drivers;
	AsynDevices _devices;

	initHookState _initHookState;

	/// @endcond
public:
	/// Dtor
	~ManagerBase();

	/**
	 * Register driver instance.
	 *
	 * It puts driver instance to the list and keeps for the driver initialization.
	 */
	virtual ndsStatus registerDriver(DriverBase* driver);

	/**
	 * Create device by driver name.
	 *
	 * It is called from IOC when user calls ndsCreateDevice from IOC console.
	 */
	ndsStatus createDevice(const char* driverName,
			const char* portName,
			const char* params);

	/**
	 * @param portName - the device port name which was used to create device by nds::createDevice.
	 * @param device   -
	 * @return ndsSuccess - if device found
     * @return ndsError   - if device is not found
	 */
    ndsStatus findDevice(const char* portName,
            Device** device);

	/// @cond NDS_INTERNALS
	void ndsAtExit(void *param);
	void epicsInit(initHookState state);
	/// @endcond

    /**
     *
     */
    void listDevices();

    /**
     *
     */
    void listDrivers();

    /**
     *
     */
    virtual void printDeviceStructure(const std::string&);

};

/**
 * Driver registration helper.
 * It simplifies driver registration.
 */
template<class T>
struct RegisterDriver
{
	/**
	 *  Constructor which sets matching of string driver name to driver class
	 *  and register it in NDS Manager.
	 *
	 *  @param driverName - string name of the driver.
	 */
	RegisterDriver(const std::string& driverName )
	{
		Manager::getInstance().registerDriver(new Driver<T>(driverName));
	}
};


}

#endif /* NDSMANAGER_H_ */
