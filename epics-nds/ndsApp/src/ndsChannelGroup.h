/*
 * NDSChannelGroup.h
 *
 *  Created on: 18. jul. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDSCHANNELGROUP_H_
#define NDSCHANNELGROUP_H_

#include <vector>
#include <string>
#include "ndsTypes.h"
#include "ndsPVContainer.h"
#include "ndsBase.h"
#include "ndsBaseChannel.h"

namespace nds
{

class Channel;
class Device;

#define NDS_INSTANTIATE_FOR_EACH_CHANNEL_GROUP_FUNCTION \
    NDS_INSTANTIATE(Int32, State) \
    NDS_INSTANTIATE(Int32, Reset) \

/**
 *	Channel's group
 *
 *      Channel group is a logical representation of channel's union.
 *      In asynDriver channel group is represented by asynPort.
 *      E.g. If you named device as 'pxi6259.0' (see device description),
 *      then port for channel group with prefix ai (analog input)
 *      will looks like  'pxi6259.0.ai' .
 *      This port should be used to configure additional functionality in IOC.
 *      (E.g. trace messages mask).
 *
 */
class ChannelGroup: public BaseChannel
{
public:
    typedef std::map<size_t, Channel*> ChannelContainer;

protected:
	/// @cond NDS_INTERNALS
	friend class Device;

	ChannelContainer  _nodes;
	std::string       _portName;
	AsynDriver*       _channelPortDriver;

	ChannelGroup(const ChannelGroup&);
	ChannelGroup& operator= (const ChannelGroup&);

#define NDS_INSTANTIATE       NDS_DECLARE_SIMPL_PROP
#define NDS_INSTANTIATE_OCTET NDS_DECLARE_OCTET_PROP
#define NDS_INSTANTIATE_ARRAY NDS_DECLARE_ARRAY_PROP
	NDS_INSTANTIATE_FOR_EACH_CHANNEL_GROUP_FUNCTION
#undef NDS_INSTANTIATE
#undef NDS_INSTANTIATE_OCTET
#undef NDS_INSTANTIATE_ARRAY

    virtual ndsStatus doPostRegistration(Channel* channel, size_t channelNum);

	/// @endcond NDS_INTERNALS
public:

	/// Ctor
	ChannelGroup(const std::string& name);

	/// Dtor
	virtual ~ChannelGroup();

    /// @cond NDS_INTERNALS
    virtual void destroyPrivate();
    /// @endcond NDS_INTERNALS

	/**
	 * Returns device name
	 */
	const std::string& getName()const { return _name; }

	/// Return asyn port name
	virtual const std::string& getPortName() const  { return _portName; }

	/// @cond NDS_INTERNALS
    void setChannelAsynDriver(AsynDriver* portDriver){ _channelPortDriver = portDriver;}

	/**
	 * Private function to register handlers for each node.
	 */
    virtual ndsStatus propagateRegisterHandlers(PVContainers* pvContainers);

	/// @endcond NDS_INTERNALS


	/**
	 * Register new channel in ChannelGroup.
	 *
	 * Sequencing number will be assigned to new channel automatically.
	 * This number is used to access channel through channelGroup's asynPort.
	 *
	 */
	ndsStatus registerChannel(Channel* channel);

	/**
	 * Return's channel by index from channel list
	 */
	ndsStatus getChannel(ChannelContainer::key_type i, Channel** channel);

    /**
     * Return's channel by index from channel list
     */
	template<typename T>
    ndsStatus getChannel(ChannelContainer::key_type i, T** channel)
	{
	    NDS_STK("getChannel")
	    nds::Channel* obj;
        if ( ndsSuccess == getChannel(i, &obj) )
        {
	        try
	        {
	            *channel = dynamic_cast<T*>( obj );
	            return ndsSuccess;
	        }catch(std::bad_cast)
	        {
	            NDS_ERR("Channel %ld can't be casted to requested type.", i );
	            return ndsError;
	        }
	    }else
	    {
	        NDS_WRN("Group doesn't have %ld channel.", i );
	    }
	    return ndsError;
	}

    /**
     * Registration of PV handlers
     *
     * Registration of PV handlers is done inside this member.
     * User have to override this function if additional handlers should be registered.
     */
    virtual ndsStatus registerHandlers(PVContainers* pvContainers);

    /**
     *  Allows to switch all channels to On state
     */
    ndsStatus switchChannelsOn() __attribute__ ((deprecated));

    /**
     *  Allows to switch all channels to Off state
     */
    ndsStatus switchChannelsOff();

    /**
     *  Start processing of all channels
     */
    ndsStatus startChannels();

    /**
     *  Allows to perform a method on all registered channels
     */
    ndsStatus callOnAllChannels(boost::function<ndsStatus(nds::Channel*)>);

    /**
     *	Switch On handler
     */
	ndsStatus onSwitchOn() __attribute__ ((deprecated));

    /**
     *  Ready handler
     */
	ndsStatus onReady(nds::ChannelStates, nds::ChannelStates requestedState);

    /**
     *  Channel request state handler.
     *  It will prevent child object to be switched on if parent object
     *  is not yet switched on.
     */
    ndsStatus onRequestChildSwitchOn(ChannelStates, ChannelStates);

    ndsStatus onRequestChildProcessing(ChannelStates, ChannelStates);

    /** Set channel group state [PV record setter]
     *
     *  Asyn Reason: State \n
     *  PV Record: \n
     */
	virtual ndsStatus setState(asynUser* pasynUser, epicsInt32 value);

    /** Get channel group state [PV record getter]
     *
     *  Asyn Reason: State \n
     *  PV Record: \n
     */
	virtual ndsStatus getState(asynUser* pasynUser, epicsInt32 *value);

    /** Reset [PV record setter]
     *
     *  Asyn Reason: Reset \n
     *  PV Record: \n
     */
	virtual ndsStatus setReset(asynUser* pasynUser, epicsInt32 value);

	virtual ndsStatus getReset(asynUser* pasynUser, epicsInt32 *value);

    virtual ndsStatus setIOCInitialization();
    virtual ndsStatus setIOCInitialized();

    ndsStatus onIOCInit(nds::ChannelStates, nds::ChannelStates);

    ndsStatus onEnterOff(nds::ChannelStates, nds::ChannelStates);

    virtual void printStructure(const std::string& prefix);

};

}

#endif /* NDSCHANNELGROUP_H_ */
