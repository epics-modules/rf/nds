/*
 * ndsFirmware.cpp
 *
 *  Created on: 11. mar. 2013
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#include <cstring>
#include <libxml/uri.h>
#include <curl/curl.h>
#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>

#include <boost/archive/tmpdir.hpp>

#include "ndsFirmware.h"
#include "ndsDebug.h"


ndsStatus nds::Firmware::downloadImage()
{
	NDS_INF("Download image file: %s", image.c_str());
	localFileCreated=false;
	if ( image.empty() )
		return ndsError;

	xmlURIPtr uri =	xmlParseURI(image.c_str());
	NDS_DBG("scheme: %s opaque: %s server: %s path: %s", uri->scheme,  uri->opaque,
			uri->server,
			uri->path );
	if  ( (0 == uri->scheme) || (0 == uri->server) || !strcmp(uri->scheme,"") || !strcmp(uri->scheme, "file:") )
	{
		boost::filesystem::path imagePath( image );

		if ( imagePath.has_branch_path() && boost::filesystem::exists(image) )
		{
		}else
		{
			boost::filesystem::path metaPath( metaFile );
			imagePath = metaPath.branch_path() / imagePath;
		}

		imageAbsPath = imagePath.string();
		xmlFreeURI(uri);
		return ndsSuccess;
	}
	xmlFreeURI(uri);

	localFileCreated=true;
	char mask[] = "bin_XXXXXX";

	boost::filesystem::path filePath;

//	BOOST 1.45 Filesystem v.3
//	filePath /= boost::filesystem::path::temp_directory_path();

	filePath /= boost::archive::tmpdir();
	filePath /= mktemp(mask);
	imageAbsPath = filePath.string();

	NDS_INF("Temp image file: %s", imageAbsPath.c_str() );

	if ( imageAbsPath.empty() )
		return ndsError;

	bool result = download(image, imageAbsPath.c_str() );
	return (result)?ndsSuccess:ndsError;
}

ndsStatus nds::Firmware::loadFile(const std::string& fileName)
{
    xmlDocPtr doc;
    xmlNodePtr cur;
    nds::Firmware firmware;

    metaFile = fileName;

    doc = xmlParseFile(fileName.c_str());
    if (doc == NULL )
    {
    	NDS_ERR("Document was not parsed successfully.");
    	return ndsError;
    }

    cur = xmlDocGetRootElement(doc);

    if (cur == NULL) {
    	NDS_ERR("Document is empty.");
    	xmlFreeDoc(doc);
		return ndsError;
	}

	if ( xmlStrcmp(cur->name, (const xmlChar *) "firmware")) {
		NDS_ERR("Wrong document's type, root node != firmware");
		xmlFreeDoc(doc);
		return ndsError;
	}

//	if ((!strcmp(cur->name, "firmware")) && (cur->ns == ns))
    loadXML(doc, cur);
    xmlFreeDoc(doc);
    return ndsSuccess;
}


void xmlChar2Str(xmlNodePtr node, const char *name, std::string& res)
{
	char *c_str = (char*) xmlGetProp(node, (const xmlChar *) name);
	if ( NULL != c_str )
	{
		res = std::string(c_str);
	}
}

void nds::Firmware::loadXML(xmlDocPtr doc, xmlNodePtr cur)
{
    xmlChar2Str(cur, "image", image);
    xmlChar2Str(cur, "version", version);
    xmlChar2Str(cur, "sha1", sha1);
}

void nds::Target::loadXML(xmlDocPtr doc, xmlNodePtr cur)
{
    xmlChar2Str(cur, "device", device);
    xmlChar2Str(cur, "module", module);

    cur = cur->children;
    while ( cur != NULL )
    {
    	NDS_DBG("Children: %s", (char*)cur->name);

    	if ((!xmlStrcmp( cur->name, (const xmlChar *)"hardware-revision")) )
    		hardware.push_back(  (char*) xmlNodeListGetString(doc, cur->xmlChildrenNode, 1) );

    	if ((!xmlStrcmp( cur->name, (const xmlChar *)"firmware-version")) )
    		firmware.push_back( (char*) xmlNodeListGetString(doc, cur->xmlChildrenNode, 1) );

    	cur = cur->next;
    }
}

ndsStatus nds::Firmware::checkSHA()
{
	if ( sha1.empty() )
	{
		NDS_WRN("SHA-1 of image is not provided.");
		return ndsSuccess;
	}

	return ndsSuccess;
}

nds::Firmware::~Firmware()
{

}

static size_t write_data(void *ptr, size_t size, size_t nmemb, void *stream)
{
  size_t written = fwrite(ptr, size, nmemb, (FILE *)stream);
  return written;
}

bool download(const std::string &uri, const std::string &filePath)
{
	CURL *curl_handle;
	FILE *file;
	CURLcode result;

	curl_global_init(CURL_GLOBAL_ALL);

	/* init the curl session */
	curl_handle = curl_easy_init();

	/* set URL to get here */
	curl_easy_setopt(curl_handle, CURLOPT_URL, uri.c_str() );

	/* Switch on full protocol/debug output while testing */
	curl_easy_setopt(curl_handle, CURLOPT_VERBOSE, 1L);

	/* disable progress meter, set to 0L to enable and disable debug output */
	curl_easy_setopt(curl_handle, CURLOPT_NOPROGRESS, 1L);

	/* send all data to this function  */
	curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, write_data);

	/* open the file */
	file = fopen(filePath.c_str(), "wb");
	if (file)
	{
	    /* write the page body to this file handle. CURLOPT_FILE is also known as
	       CURLOPT_WRITEDATA*/
	    curl_easy_setopt(curl_handle, CURLOPT_FILE, file);

	    result = curl_easy_perform(curl_handle);

        if(result != CURLE_OK)
        {
	        /* if errors have occured, tell us wath's wrong with 'result'*/
        	NDS_ERR( "curl_easy_perform() failed: %s\n", curl_easy_strerror(result));
        }

	    /* close the header file */
	    fclose(file);
	}else
	{
		NDS_ERR("Can't open file: '%s'", filePath.c_str());
		return ndsError;
	}

	/* cleanup curl stuff */
    curl_easy_cleanup(curl_handle);
    return (result != CURLE_OK)?ndsError:ndsSuccess;
}
