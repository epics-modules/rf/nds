/*
 * ndsThreadTask.h
 *
 *  Created on: 2. sep. 2013
 *      Author: Slava Isaev
 */

#ifndef NDSTHREADTASK_H_
#define NDSTHREADTASK_H_

#include "ndsThread.h"

namespace nds
{

class ThreadTask : public Thread
{

protected:
    TaskBody _body;
    void* _data;

    ThreadTask(const std::string& name,
            unsigned int     stackSize,
            unsigned int     priority,
            TaskServiceBase* service,
            TaskBody  body
           );

public:

    static ThreadTask* create(const std::string& name,
            unsigned int     stackSize,
            unsigned int     priority,
            TaskBody  body);

    virtual ~ThreadTask();

    virtual void run(TaskServiceBase&  service);
};

} /* namespace nds */
#endif /* NDSTHREADTASK_H_ */
