/*
 * ndsDriver.h
 *
 *  Created on: 18. jul. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDSDRIVER_H_
#define NDSDRIVER_H_

#include <string>
#include "loki/HierarchyGenerators.h"

#include "ndsTypes.h"

namespace nds
{

class ChannelGroup;
class Device;

/** NDS Driver interface.
 *
 */
class DriverBase
{
protected:
	/// Driver name
	std::string _driverName;
public:
	/** Ctor
	 *
	 * @param driverName - driver name
	 */
	DriverBase(const std::string& driverName)
		:_driverName(driverName)
	{}

	/// Dtor
	virtual ~DriverBase(){}

	/// Create device driver with requested parameters
	virtual Device *createDevice(const std::string& name)
	{
		return 0;
	}

	/// Get driver name
	const std::string getName()
	{
		return _driverName;
	}
};


/*** NDS Driver is a fabric which keeps Device type
*    and creates it when it is requested.
*/
template<class T>
class Driver: public DriverBase
{
protected:
	typedef T DeviceType;
public:
	/// Ctor
	Driver(const std::string& driverName)
		:DriverBase(driverName){}

	/// Create specific device driver type
	virtual T *createDevice(const std::string& name)
	{
		return new DeviceType(name);
	}
};


}

#endif /* NDSDRIVER_H_ */
