/*
 * ndsMacro.h
 *
 *  Created on: 10. avg. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDSMACRO_H_
#define NDSMACRO_H_

#define NDS_INSTANTIATE_FOR_EACH_CHANNEL_FUNCTION \
    NDS_INSTANTIATE(Int32,   DecimationFactor) \
    NDS_INSTANTIATE(Int32,   DecimationOffset) \
    NDS_INSTANTIATE(Int32,   SignalType) \
    NDS_INSTANTIATE(Int32,   FFTWindow) \
    NDS_INSTANTIATE(Float64, SignalFrequency) \
    NDS_INSTANTIATE(Float64, SignalAmplitude) \
    NDS_INSTANTIATE(Float64, SignalOffset) \
    NDS_INSTANTIATE(Float64, SignalPhase) \
    NDS_INSTANTIATE(Float64, SignalDutyCycle) \
    NDS_INSTANTIATE(Float64, FFTSize) \
    NDS_INSTANTIATE(Float64, FFTOverlap) \
    NDS_INSTANTIATE(Float64, FFTSmoothing)

#define NDS_INSTANTIATE_STUB_FOR_EACH_TYPE \
    NDS_INSTANTIATE_OCTET(Octet, Octet) \
    NDS_INSTANTIATE(Int32, Int32) \
    NDS_INSTANTIATE(Float64, Float64) \
    NDS_INSTANTIATE_ARRAY(Int8, Int8Array) \
    NDS_INSTANTIATE_ARRAY(Int16, Int16Array) \
    NDS_INSTANTIATE_ARRAY(Int32, Int32Array) \
    NDS_INSTANTIATE_ARRAY(Float32, Float32Array) \
    NDS_INSTANTIATE_ARRAY(Float64, Float64Array) \

#define NDS_DECLARE_SIMPL_PROP(type, function) \
int _interruptId##function;\
epics##type _##function;

#define NDS_DECLARE_ARRAY_PROP(type, function) \
int _interruptId##function;\
epics##type* _##function;\
size_t _##function##Size;

#define NDS_DECLARE_OCTET_PROP(type, function) \
int _interruptId##function;\
char* _##function;

#define NDS_TRACE_INT_VALUE(type, function) \
NDS_TRACE("portAddr: %d _interruptId"#function": %d\n", _portAddr, _interruptId##function);


#define NDS_DECLARE_FUNCTIONS(type, function) \
	virtual ndsStatus set##function(asynUser* pasynUser, epics##type value);\
	virtual ndsStatus get##function(asynUser* pasynUser, epics##type *value);

#define NDS_DECLARE_ARRAY_FUNCTIONS(type, function) \
	virtual ndsStatus set##function(asynUser* pasynUser, epics##type *inArray,  size_t nelem);\
	virtual ndsStatus get##function(asynUser* pasynUser, epics##type *outArray, size_t nelem,  size_t *nIn);

#define NDS_DECLARE_OCTET_FUNCTIONS(type, function) \
	virtual ndsStatus set##function(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransfered);\
	virtual ndsStatus get##function(asynUser *pasynUser, char *data, size_t maxchars, size_t *nbytesTransfered, int *eomReason);


//#define NDS_FUNC_TRACE(pref, function) do{ asynPrint(pasynUser, ASYN_TRACE_FLOW, "NDS: "#pref##function); while(0)
#define NDS_FUNC_TRACE(pref, function) do{ NDS_TRC("NDS: %s%s\n", #pref, #function); } while(0)

#define NDS_IMPLEMENT_FUNCTIONS(type, function) \
ndsStatus NDS_CHANNEL_CLASS::set##function(asynUser* pasynUser, epics##type value)\
{\
	NDS_FUNC_TRACE(write, function);\
	 _##function = value;\
	return ndsSuccess;\
}\
ndsStatus NDS_CHANNEL_CLASS::get##function(asynUser* pasynUser, epics##type *value)\
{\
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )\
    {\
        return ndsError;\
    }\
	NDS_FUNC_TRACE(read, function);\
	*value =  _##function;\
	return ndsSuccess;\
}

#define NDS_IMPLEMENT_ARRAY_FUNCTIONS(type, function) \
ndsStatus NDS_CHANNEL_CLASS::set##function(asynUser* pasynUser, epics##type *inArray,  size_t nelem)\
{\
	NDS_FUNC_TRACE(write, function);\
    memcpy(_##function, inArray, sizeof(epics##type)* nelem);\
	return ndsSuccess;\
}\
ndsStatus NDS_CHANNEL_CLASS::get##function(asynUser* pasynUser,  epics##type *outArray, size_t nelem,  size_t *nIn)\
{\
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )\
    {\
        return ndsError;\
    }\
	NDS_FUNC_TRACE(read, function);\
    memcpy(outArray, _##function, sizeof(epics##type)* nelem);\
	return ndsSuccess;\
}

#define NDS_IMPLEMENT_OCTET_FUNCTIONS(type, function) \
ndsStatus NDS_CHANNEL_CLASS::set##function(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransfered)\
{\
	NDS_FUNC_TRACE(write, function);\
	return ndsSuccess;\
}\
ndsStatus NDS_CHANNEL_CLASS::get##function(asynUser *pasynUser, char *data, size_t maxchars, size_t *nbytesTransfered, int *eomReason)\
{\
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )\
    {\
        return ndsError;\
    }\
	NDS_FUNC_TRACE(read, function);\
	*nbytesTransfered=0;\
	*eomReason=ASYN_EOM_END;\
	return ndsSuccess;\
}


#define NDS_IMPLEMENT_STUBS(type, function) \
ndsStatus NDS_CHANNEL_CLASS::set##function(asynUser* pasynUser, epics##type value)\
{\
    NDS_FUNC_TRACE(write, function);\
     _##function = value;\
    return ndsSuccess;\
}\
ndsStatus NDS_CHANNEL_CLASS::get##function(asynUser* pasynUser, epics##type *value)\
{\
    NDS_FUNC_TRACE(read, function);\
    *value =  _##function;\
    return ndsSuccess;\
}

#define NDS_IMPLEMENT_ARRAY_STUBS(type, function) \
ndsStatus NDS_CHANNEL_CLASS::set##function(asynUser* pasynUser, epics##type *inArray,  size_t nelem)\
{\
    NDS_FUNC_TRACE(write, function);\
    memcpy(_##function, inArray, sizeof(epics##type)* nelem);\
    return ndsSuccess;\
}\
ndsStatus NDS_CHANNEL_CLASS::get##function(asynUser* pasynUser,  epics##type *outArray, size_t nelem,  size_t *nIn)\
{\
    NDS_FUNC_TRACE(read, function);\
    memcpy(outArray, _##function, sizeof(epics##type)* nelem);\
    return ndsSuccess;\
}

#define NDS_IMPLEMENT_OCTET_STUBS(type, function) \
ndsStatus NDS_CHANNEL_CLASS::set##function(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransfered)\
{\
    NDS_FUNC_TRACE(write, function);\
    return ndsSuccess;\
}\
ndsStatus NDS_CHANNEL_CLASS::get##function(asynUser *pasynUser, char *data, size_t maxchars, size_t *nbytesTransfered, int *eomReason)\
{\
    NDS_FUNC_TRACE(read, function);\
    *nbytesTransfered=0;\
    *eomReason=ASYN_EOM_END;\
    return ndsSuccess;\
}

#define NDS_REG_SIMPL_FUNCTIONS(type, function) \
    pvContainers->register##type(_portAddr,\
               #function,\
                        this,\
                        &NDS_CHANNEL_CLASS::set##function,\
                        &NDS_CHANNEL_CLASS::get##function,\
                        #function,\
                        #function,\
                        &_interruptId##function);

#define NDS_REG_ARRAY_FUNCTIONS(type, function) \
	pvContainers->register##type##Array(\
				_portAddr,\
				#function,\
				this,\
				&NDS_CHANNEL_CLASS::set##function, \
				&NDS_CHANNEL_CLASS::get##function, \
                        #function,\
                        #function,\
				&_interruptId##function);

#define NDS_REG_OCTET_FUNCTIONS(type, function) \
	pvContainers->register##type(\
                        _portAddr, #function,\
                        this,\
			&NDS_CHANNEL_CLASS::set##function, \
			&NDS_CHANNEL_CLASS::get##function, \
                        #function,\
                        #function,\
			&_interruptId##function);

#endif /* NDSMACRO_H_ */
