/*
 * ndsADIOChannel.h
 *
 *  Created on: 22. avg. 2012
 *      Author: Slava Isaev (C) Cosylab. Slovenia.
 */

#ifndef NDSADIOCHANNEL_H_
#define NDSADIOCHANNEL_H_


#include "ndsChannel.h"

namespace nds
{

#define NDS_INSTANTIATE_FOR_EACH_ADIO_CHANNEL_FUNCTION \
		NDS_INSTANTIATE_ARRAY(Float32, Spline) \
		NDS_INSTANTIATE(Int32,   Gain) \
		NDS_INSTANTIATE(Int32,   Coupling) \
		NDS_INSTANTIATE(Float64, Offset) \
		NDS_INSTANTIATE(Float64, MeasuredOffset) \
		NDS_INSTANTIATE(Float64, Bandwidth) \
		NDS_INSTANTIATE(Float64, Impedance) \

/***
 * Analog/Digital channel specification
 *
 * This specification adds functionality which is common for the Anolog and Digital channels.
 *
 */
class ADIOChannel : public Channel
{
protected:

	/// Interrupt ID to dispatch event for Spline
	int _interruptIdSpline;
	/// Spline buffer
	epicsFloat32* _Spline;
	/// Size of the Spline buffer
	size_t _SplineSize;

	/// Interrupt ID to dispatch Gain event
	int _interruptIdGain;
	epicsInt32 _Gain;

	/// Interrupt ID to dispatch Coupling event
	int _interruptIdCoupling;
	epicsInt32 _Coupling;

	/// Interrupt ID to dispatch Offset event
	int _interruptIdOffset;
	epicsFloat64 _Offset;

	/// Interrupt ID to dispatch MeasuredOffset event
	int _interruptIdMeasuredOffset;
	epicsFloat64 _MeasuredOffset;

	/// Interrupt ID to dispatch Bandwidth event
	int _interruptIdBandwidth;
	epicsFloat64 _Bandwidth;

	/// Interrupt ID to dispatch Impedance event
	int _interruptIdImpedance;
	epicsFloat64 _Impedance;

public:

	/**
	 *	Register ADIOChannel handlers.
	 */
	virtual ndsStatus registerHandlers(PVContainers* pvContainers);

	/**
	 *	Set default buffer processing.
	 */
    virtual ndsStatus bufferProcessed();


    /** [PV record setter]
     *  Asyn Reason: Gain
     *  PV Record:   GAIN
     *
     *  For analog input channels, this is the amplification factor by which the signal is amplified before it is digitized.
	 *  For analog output channels, this is the amplification factor by which the signal
	 *  is amplified just after it has been converted to analog.
	 *	If the channel does not support a particular gain factor,
	 *	alarm severity (SEVR field) of this record is set to MINOR and alarm state (STAT) is set to WRITE.
	 *	The gain is set to the nearest supported smaller value.
     *
     */
	virtual ndsStatus setGain(asynUser* pasynUser, epicsInt32 value);

    /** [PV record getter]
     *  Asyn Reason: Gain
     *  PV Record:
     */
	virtual ndsStatus getGain(asynUser* pasynUser, epicsInt32 *value);

    /** Set the coupling (AC or DC) of the analog input channel. [PV record setter]
     *  Asyn Reason: Coupling
     *  PV Record:
     *
     *  Values are:
     *  0	AC	Coupling for alternating current.
     *  1	DC	Coupling for direct current.
     *
     */
	virtual ndsStatus setCoupling(asynUser* pasynUser, epicsInt32 value);

    /** [PV record getter]
     *  Asyn Reason: Coupling
     *  PV Record: ACDC
     */
	virtual ndsStatus getCoupling(asynUser* pasynUser, epicsInt32 *value);

    /** Offset voltage of a channel. [PV record setter].
     *
     *  Asyn Reason: Offset \n
     *  PV Record: OFS \n
     *
     *  For analog input channels, the offset voltage is subtracted before it is amplified.
     *  For analog output channels, the offset voltage is added after it is amplified.
     *  If the channel does not support a particular offset, alarm severity (SEVR field)
     *  of this record is set to MINOR and alarm state (STAT) is set to WRITE.
     *  The offset is set to the nearest supported smaller value.
     *
     */
	virtual ndsStatus setOffset(asynUser* pasynUser, epicsFloat64 value);

    /** \brief Offset voltage of a channel. [PV record getter]
     *
     *  Asyn Reason: Offset \n
     *  PV Record: \n
     *
     *  E.g., the sine signal would range from SGNO-SGNA to SGNO+SGNA.
     *  Offset is specified in terms of the output’s engineering units.
     */
	virtual ndsStatus getOffset(asynUser* pasynUser, epicsFloat64 *value);

    /** \brief  [PV record setter]
     *
     *  Asyn Reason: MeasuredOffset
     *  PV Record: MOFS
     */
	virtual ndsStatus setMeasuredOffset(asynUser* pasynUser, epicsFloat64 value);

    /** Return the measured offset of an analog input channel. [[PV record getter]]
     *  Asyn Reason: MeasuredOffset
     *  PV Record: MOFS
     *
     */
	virtual ndsStatus getMeasuredOffset(asynUser* pasynUser, epicsFloat64 *value);

    /** [PV record setter]
     *
     *  Asyn Reason: Bandwidth \n
     *  PV Record: BW \n
     */
	virtual ndsStatus setBandwidth(asynUser* pasynUser, epicsFloat64 value);

    /** Get the bandwidth of an analog input channel.
     *
     *  Asyn Reason: Bandwidth \n
     *  PV Record: BW \n
     */
	virtual ndsStatus getBandwidth(asynUser* pasynUser, epicsFloat64 *value);

    /** Set the impedance of the analog input channel. [PV record setter]
     *
     *  Asyn Reason: Impedance \n
     *  PV Record: - \n
     */
	virtual ndsStatus setImpedance(asynUser* pasynUser, epicsFloat64 value);

    /** Get the impedance of the analog input channel.[PV record getter]
     *
     *  Asyn Reason: Impedance\n
     *  PV Record: IMPD\n
     */
	virtual ndsStatus getImpedance(asynUser* pasynUser, epicsFloat64 *value);

    /** [PV record setter]
     *
     *  Asyn Reason: Spline \n
     *  PV Record: \n
     */
	virtual ndsStatus setSpline(asynUser* pasynUser, epicsFloat32 *inArray,  size_t nelem);

    /** [PV record getter]
     *
     *  Asyn Reason: Spline \n
     *  PV Record: \n
     */
	virtual ndsStatus getSpline(asynUser* pasynUser, epicsFloat32 *outArray, size_t nelem,  size_t *nIn);
};

}


#endif /* NDSADIOCHANNEL_H_ */
