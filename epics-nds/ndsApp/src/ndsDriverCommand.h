/*
 * ndsDriverCommand.h
 *
 *  Created on: 17. apr. 2013
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDSDRIVERCOMMAND_H_
#define NDSDRIVERCOMMAND_H_

#include <vector>
#include <string>

namespace nds
{

/** Class to keep parsed asyn reason.
 *
 *  Asyn reason could include additional arguments to reason itself.
 *  Arguments should be put in braces and separated by comma.
 *  Arguments could be empty to keep consistent enumeration of the arguments.
 *  E.g.:
 *
 *  field(OUT,  "@asyn($(ASYN_PORT).$(CHANNEL_PREFIX), $(ASYN_ADDR)) Event(arg1,arg2,,arg4)")
 *
 *  Reason for this PV will be "Event". This reason will be registered for the event handlers (setter and getter).
 *  It allows to have multiple EPICS records with the same reason and as result the same handlers, but with different
 *  arguments.
 *
 *  This makes possible to add another abstraction layer to the PV handling and to make specification on
 *  EPICS Database level.
 *
 */
class DriverCommand
{
	std::string _reason;
	std::string _command;
public:
	/// Vector of reason's arguments
	std::vector<std::string> Args;

	/** C-tor. It parses passed command for splitting reason and reason's arguments.
	 *  Reason should be presented as reason(arg1,arg2,...)
	*/
	DriverCommand(std::string command);

	/// D-tor
	virtual ~DriverCommand();

	/// Reason's getter. Returns clear reason.
	std::string getReason() { return _reason; }

    /// Command's getter. Returns full command passed to driver: reason(args)
	std::string getCommand(){ return _command; }
};

} /* namespace nds */
#endif /* NDSDRIVERCOMMAND_H_ */
