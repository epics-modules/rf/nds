/*
 * ndsPulse.h
 *
 *  Created on: 18. apr. 2013
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDSPULSE_H_
#define NDSPULSE_H_

#include <ndsTimeEvent.h>

namespace nds
{

/*** Implementation of pulse event
 *
 */
class Pulse : public TimeEvent
{
protected:
    int _interruptIdWidth;
    epicsFloat64 _Width;
public:
    /// C-tor
    Pulse();

    /// D-tor
    virtual ~Pulse();

    /// Register additional PV's handlers
    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);

    /// Time event width setter
    virtual ndsStatus setWidth(asynUser* pasynUser, epicsFloat64 value);

    /// Time event width getter
    virtual ndsStatus getWidth(asynUser* pasynUser, epicsFloat64* value);
};


} /* namespace nds */
#endif /* NDSPULSE_H_ */
