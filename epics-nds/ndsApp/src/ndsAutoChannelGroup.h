/*
 * ndsAutoChannelGroup.h
 *
 *  Created on: 22. avg. 2013
 *      Author: Slava Isaev
 */

#ifndef NDSAUTOCHANNELGROUP_H_
#define NDSAUTOCHANNELGROUP_H_

#include <boost/function.hpp>

#include <ndsChannelGroup.h>

namespace nds
{

class Channel;
typedef boost::function<nds::Channel* ()> ChannelCreator;


class AutoChannelGroup : public nds::ChannelGroup
{
protected:
    ChannelCreator _channelCreator;
public:
    AutoChannelGroup(const std::string& name, const ChannelCreator& channelCreator);

    virtual ~AutoChannelGroup();

    nds::Channel* createChannel();

    ndsStatus drvUserCreate(asynUser *pasynUser, const char *drvInfo, const char **pptypeName, size_t *psize, int portAddr);
};


} /* namespace nds */
#endif /* NDSAUTOCHANNELGROUP_H_ */
