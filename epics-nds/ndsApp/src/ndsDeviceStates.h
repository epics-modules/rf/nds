/*
 * ndsDeviceStates.h
 *
 *  Created on: 21. avg. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDSDEVICESTATES_H_
#define NDSDEVICESTATES_H_

namespace nds
{

/***
 *  Device's states
 */
typedef enum
{
	DEVICE_STATE_UNKNOWN=0,
	DEVICE_STATE_IOC_INIT,
	DEVICE_STATE_OFF,
    DEVICE_STATE_INIT,
	DEVICE_STATE_ON,
	DEVICE_STATE_ERROR,
	DEVICE_STATE_FAULT,
    DEVICE_STATE_RESETTING,

    DEVICE_STATE_last,
    DEVICE_STATE_DEFUNCT = DEVICE_STATE_FAULT,
} DeviceStates;

}


#endif /* NDSDEVICESTATES_H_ */
