/*
 * ndsTimeEvent.h
 *
 *  Created on: 18. apr. 2013
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDSTIMeEVENT_H_
#define NDSTIMeEVENT_H_

#include "ndsChannel.h"
#include "ndsTypes.h"

namespace nds
{

/*** Time event implementation
 *
 *   It is a base class for the all time events.
 *
 */
class TimeEvent : public Channel
{
private:
    int _interruptIdOriginTime;
    int _interruptIdDelay;
    int _interruptIdEnabled;
    int _interruptIdGenerated;
    int _interruptIdLevel;

    epicsFloat64 _OriginTime;   ///< Origin time
    epicsFloat64 _Delay;        ///< Delay
    epicsFloat64 _Generated;    ///< Delay for origin time when event was actually generated
    epicsInt32   _Enabled;      ///< Shows if event is enabled
    epicsInt32   _Level;        ///< Event level

public:
    TimeEvent();
    virtual ~TimeEvent();

    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);

    /// Origin time setter
    virtual ndsStatus setOriginTime(asynUser* pasynUser, epicsFloat64 value);

    /// Origin time getter
    virtual ndsStatus getOriginTime(asynUser* pasynUser, epicsFloat64* value);

    /// Delay setter
    virtual ndsStatus setDelay(asynUser* pasynUser, epicsFloat64 value);

    /// Delay getter
    virtual ndsStatus getDelay(asynUser* pasynUser, epicsFloat64* value);

    /// Enabled setter
    virtual ndsStatus setEnabled(asynUser* pasynUser, epicsInt32 value);

    /// Enabled getter
    virtual ndsStatus getEnabled(asynUser* pasynUser, epicsInt32* value);

    /// Level setter
    virtual ndsStatus setLevel(asynUser* pasynUser, epicsInt32 value);

    /// Level getter
    virtual ndsStatus getLevel(asynUser* pasynUser, epicsInt32* value);
};

} /* namespace nds */
#endif /* NDSTIMEEVENT_H_ */
