/*
 * ndsChannelStates.h
 *
 *  Created on: 21. avg. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDSCHANNELSTATES_H_
#define NDSCHANNELSTATES_H_

#include "ndsAbstractStateMachine.h"
#include "ndsChannelStates.h"

namespace nds
{

/*** Channel states
 *
 */
typedef enum
{
	CHANNEL_STATE_UNKNOWN=0,
	CHANNEL_STATE_IOC_INITIALIZATION=1,

    CHANNEL_STATE_DISABLED=2,
	CHANNEL_STATE_PROCESSING=4,
	CHANNEL_STATE_ERROR=6,
	CHANNEL_STATE_RESETTING=7,
	CHANNEL_STATE_FAULT=8,

    CHANNEL_STATE_last,

    // Depreciated states
    CHANNEL_STATE_DEGRADED=5,    /// Degraded state is depreciated and indicated through service quality field.
    CHANNEL_STATE_ENABLED=3,     /// Enabled state is depreciated
    CHANNEL_STATE_READY=9,       /// Channel configured and ready to processed [Depricated].

    CHANNEL_STATE_OFF = CHANNEL_STATE_DISABLED,
    CHANNEL_STATE_ON = CHANNEL_STATE_ENABLED,
    CHANNEL_STATE_DEFUNCT = CHANNEL_STATE_FAULT,

}ChannelStates;


class ChannelStatesDescriptor : public StateMachineDescriptor<ChannelStates>
{
public:
    std::string getStateName(ChannelStates state);
    bool isTransitionAlowed(ChannelStates currentState, ChannelStates requestedState);
    void configureSateMachine( AbstractStateMachine<ChannelStates>* );
};


}

#endif /* NDSCHANNELSTATES_H_ */
