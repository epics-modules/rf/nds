/*
 * ndsClock.h
 *
 *  Created on: 18. apr. 2013
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDSCLOCK_H_
#define NDSCLOCK_H_

#include "ndsPulse.h"

namespace nds
{

/*** Clock time event implementation.
 *
 *   Clock inherits all the configuration parameters from the TimeEvent and Pulse
 */
class Clock: public Pulse
{
private:

    int _interruptIdDutyCycle;
    int _interruptIdTimeEnd;

    epicsFloat64 _DutyCycle; ///< Duty cycle of the
    epicsFloat64 _TimeEnd;   ///< Clock generation end time, delay from the origin time
public:
    /// C-tor
    Clock();

    /// D-tor
    virtual ~Clock();

    /// Register additional PV's handlers
    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);

    /// Duty cycle setter
    virtual ndsStatus setDutyCycle(asynUser* pasynUser, epicsFloat64 value);

    /// Duty cycle getter
    virtual ndsStatus getDutyCycle(asynUser* pasynUser, epicsFloat64* value);

    /// Time end setter
    virtual ndsStatus setTimeEnd(asynUser* pasynUser, epicsFloat64 value);

    /// Time end getter
    virtual ndsStatus getTimeEnd(asynUser* pasynUser, epicsFloat64* value);

};

} /* namespace nds */
#endif /* NDSCLOCK_H_ */
