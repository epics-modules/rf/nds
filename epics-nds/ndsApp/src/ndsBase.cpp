/*
 * ndsBase.cpp
 *
 *  Created on: 2. avg. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#include <sstream>
#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <epicsThread.h>

#include "ndsDebug.h"
#include "ndsBase.h"

using namespace nds;

static void processThread(void *drvPvt)
{
    Base* base = (Base*) drvPvt;
    base->processPrivate();
}

Base::Base():
		_stopProcessing(false)
		,_portDriver(0)
		,_portAddr(-1)
{
  // Registration of the handlers for the messaging mechanism
  registerMessageWriteHandler(
      "TEST", /// Name of the message type which will be handled by this handler
      boost::bind(
          &Base::doTest, /// Address of the handler
          this,                            /// Object which owns the handler
          _1,_2)                           /// Stub functors (see boost::bind documentation for details.)
  );

  // Registration of the handlers for the messaging mechanism
  registerMessageWriteHandler(
      "LIST", /// Name of the message type which will be handled by this handler
      boost::bind(
          &Base::listCommands, /// Address of the handler
          this,                            /// Object which owns the handler
          _1,_2)                           /// Stub functors (see boost::bind documentation for details.)
  );

  // Registration of the handlers for the messaging mechanism
  registerMessageWriteHandler(
      "ON", /// Name of the message type which will be handled by this handler
      boost::bind(
          &Base::handleOnMsg, /// Address of the handler
          this,                            /// Object which owns the handler
          _1,_2)                           /// Stub functors (see boost::bind documentation for details.)
  );

  // Registration of the handlers for the messaging mechanism
  registerMessageWriteHandler(
      "OFF", /// Name of the message type which will be handled by this handler
      boost::bind(
          &Base::handleOffMsg, /// Address of the handler
          this,                            /// Object which owns the handler
          _1,_2)                           /// Stub functors (see boost::bind documentation for details.)
  );

  // Registration of the handlers for the messaging mechanism
  registerMessageWriteHandler(
      "RESET", /// Name of the message type which will be handled by this handler
      boost::bind(
          &Base::handleResetMsg, /// Address of the handler
          this,                            /// Object which owns the handler
          _1,_2)                          /// Stub functors (see boost::bind documentation for details.)
  );

}

bool Base::isEnabled()
{
    return _isEnabled;
}

ndsStatus Base::doTest(asynUser *pasynUser, const Message& value)
{
  Message response;
  response.messageType = "TEST";
  response.insert("TEXT", "Not implemented");
  response.insert("CODE", "-1");
  doCallbacksMessage(response);
  return ndsSuccess;
}

ndsStatus Base::listCommands(asynUser *pasynUser, const Message& value)
{
	NDS_DBG("listCommands");
	Message response;
	response.messageType = "LIST";
	response.insert("CODE", "0");

	int i=0;
	for(MessageWriteHandlersMap::const_iterator itr = _messageWriteHandlers.begin();
      itr !=  _messageWriteHandlers.end();
      ++itr, ++i)
	{
		nds::insert(response, i, itr->first );
	}
	response.insert("CODE","0");
	nds::insert(response, "N", i);
	doCallbacksMessage(response);
	return ndsSuccess;
}

ndsStatus Base::handleOnMsg(asynUser *pasynUser, const Message& value)
{
	Message response;
	response.messageType = "ON";

	ndsStatus res = on();

	if (ndsError == res)
	{
		response.insert("CODE", "-1");
	}else
	{
		response.insert("CODE", "0");
	}
	doCallbacksMessage(response);
	return res;
}

ndsStatus Base::handleOffMsg(asynUser *pasynUser, const Message& value)
{
	Message response;
	response.messageType = "OFF";
	ndsStatus res = off();
	if (ndsError == res)
	{
		response.insert("CODE", "-1");
	}else
	{
		response.insert("CODE", "0");
	}
	doCallbacksMessage(response);
	return res;
}

ndsStatus Base::handleResetMsg(asynUser *pasynUser, const Message& value)
{
	Message response;
	response.messageType = "RESET";
	ndsStatus res = reset();
	if (ndsSuccess == res)
	{
		response.insert("CODE", "0");
	}else
	{
		response.insert("CODE", "-1");
	}
	doCallbacksMessage(response);
	return res;
}

ndsStatus Base::on()
{
	return ndsSuccess;
}

ndsStatus Base::off()
{
	return ndsSuccess;
}

ndsStatus Base::reset()
{
	return ndsSuccess;
}

const std::string Base::toString() const
{
    std::stringstream ss (std::stringstream::in | std::stringstream::out);
    ss << getPortName() << "[" << _portAddr << "]";
    return ss.str();
}

ndsStatus Base::drvUserCreate(asynUser *pasynUser, const char *drvInfo, const char **pptypeName, size_t *psize, int portAddr)
{
	return ndsSuccess;
}

ndsStatus Base::drvUserGetType(asynUser *pasynUser, const char **pptypeName, size_t *psize)
{
	NDS_TRC( "NDS: Base::drvUserGetType");
	return ndsSuccess;
}

ndsStatus Base::drvUserDestroy(asynUser *pasynUser)
{
	NDS_TRC( "NDS: Base::drvUserDestroy");
	return ndsSuccess;
}

ndsStatus Base::connect(asynUser *pasynUser)
{
	NDS_TRC( "NDS: Base::connect");
    pasynManager->exceptionConnect(pasynUser);
	return ndsSuccess;
}

ndsStatus Base::disconnect(asynUser *pasynUser)
{
    NDS_TRC( "NDS: Base::disconnect");
    pasynManager->exceptionDisconnect(pasynUser);
	return ndsSuccess;
}

ndsStatus Base::registerHandlers(PVContainers* pvContainers)
{
	NDS_TRC( "NDS: Base::registerHandlers: %p", pvContainers);

	NDS_PV_REGISTER_OCTET("Command", &Base::writeAsynMessage, &Base::readAsynMessage, &_interruptIdCommand);
	NDS_PV_REGISTER_INT32("State",   &Base::setState,   &Base::getState,   &_interruptIdState);
    NDS_PV_REGISTER_INT32("Enabled", &Base::setEnabled, &Base::getEnabled, &_interruptIdEnabled);

	return ndsSuccess;
}

ndsStatus Base::propagateRegisterHandlers(PVContainers* pvContainers)
{
	NDS_TRC( "NDS: Base::propagateRegisterHandlers");
	return ndsSuccess;
}

ndsStatus Base::doCallbacksInt8Array(epicsInt8 *value, size_t nElements, int reason, int addr, ndsStatus status)
{
	if (!_portDriver)
		return ndsError;
	return (ndsStatus) _portDriver->doCallbacksInt8Array(value, nElements, reason, addr, status);
}

ndsStatus Base::doCallbacksInt8Array(epicsInt8 *value, size_t nElements, int reason, int addr, epicsTimeStamp timestamp, ndsStatus status)
{
	if (!_portDriver)
		return ndsError;
	return (ndsStatus) _portDriver->doCallbacksInt8Array(value, nElements, reason, addr, status, timestamp);
}

ndsStatus  Base::doCallbacksInt16Array(epicsInt16 *value, size_t nElements, int reason, int addr, ndsStatus status)
{
	if (!_portDriver)
		return ndsError;
	return (ndsStatus) _portDriver->doCallbacksInt16Array(value, nElements, reason, addr, status);
}

ndsStatus  Base::doCallbacksInt16Array(epicsInt16 *value, size_t nElements, int reason, int addr, epicsTimeStamp timestamp, ndsStatus status)
{
	if (!_portDriver)
		return ndsError;
	return (ndsStatus) _portDriver->doCallbacksInt16Array(value, nElements, reason, addr, status, timestamp);
}

ndsStatus  Base::doCallbacksInt32Array(epicsInt32 *value, size_t nElements, int reason, int addr, ndsStatus status)
{
	if (!_portDriver)
		return ndsError;
	return (ndsStatus) _portDriver->doCallbacksInt32Array(value, nElements, reason, addr, status);
}

ndsStatus  Base::doCallbacksInt32Array(epicsInt32 *value, size_t nElements, int reason, int addr, epicsTimeStamp timestamp, ndsStatus status)
{
	if (!_portDriver)
		return ndsError;
	return (ndsStatus) _portDriver->doCallbacksInt32Array(value, nElements, reason, addr, status, timestamp);
}

ndsStatus  Base::doCallbacksFloat32Array(epicsFloat32 *value, size_t nElements, int reason, int addr, ndsStatus status)
{
	if (!_portDriver)
		return ndsError;
	return (ndsStatus) _portDriver->doCallbacksFloat32Array(value, nElements, reason, addr, status);
}

ndsStatus  Base::doCallbacksFloat32Array(epicsFloat32 *value, size_t nElements, int reason, int addr, epicsTimeStamp timestamp, ndsStatus status)
{
	if (!_portDriver)
		return ndsError;
	return (ndsStatus) _portDriver->doCallbacksFloat32Array(value, nElements, reason, addr, status, timestamp);
}

ndsStatus  Base::doCallbacksFloat64Array(epicsFloat64 *value, size_t nElements, int reason, int addr, ndsStatus status)
{
	if (!_portDriver)
		return ndsError;
	return (ndsStatus) _portDriver->doCallbacksFloat64Array(value, nElements, reason, addr, status);
}

ndsStatus  Base::doCallbacksFloat64Array(epicsFloat64 *value, size_t nElements, int reason, int addr, epicsTimeStamp timestamp, ndsStatus status)
{
	if (!_portDriver)
		return ndsError;
	return (ndsStatus) _portDriver->doCallbacksFloat64Array(value, nElements, reason, addr, status, timestamp);
}

ndsStatus  Base::doCallbacksGenericPointer(void *pointer, int reason, int addr)
{
	NDS_TRC("doCallbacksGenericPointer");
	if (!_portDriver)
		return ndsError;
	return (ndsStatus) _portDriver->doCallbacksGenericPointer(pointer, reason, addr);
}

ndsStatus Base::doCallbacksInt32(epicsInt32 value, int reason, int addr, ndsStatus status)
{
	if (!_portDriver)
		return ndsError;
	return (ndsStatus) _portDriver->doCallbacksInt32(value, reason, addr, status);
}

ndsStatus Base::doCallbacksInt32(epicsInt32 value, int reason, int addr, epicsTimeStamp timestamp, ndsStatus status)
{
	if (!_portDriver)
		return ndsError;
	return (ndsStatus) _portDriver->doCallbacksInt32(value, reason, addr, status, timestamp);
}

ndsStatus Base::doCallbacksFloat64(epicsFloat64 value, int reason, int addr, ndsStatus status)
{
	if (!_portDriver)
		return ndsError;
	return (ndsStatus) _portDriver->doCallbacksFloat64(value, reason, addr, status);
}

ndsStatus Base::doCallbacksFloat64(epicsFloat64 value, int reason, int addr, epicsTimeStamp timestamp, ndsStatus status)
{
	if (!_portDriver)
		return ndsError;
	return (ndsStatus) _portDriver->doCallbacksFloat64(value, reason, addr, status, timestamp);
}

ndsStatus Base::doCallbacksOctet(char *data, size_t numchars, int eomReason, int reason, int addr, ndsStatus status)
{
	if (!_portDriver)
		return ndsError;
	return (ndsStatus) _portDriver->doCallbacksOctet(data, numchars, eomReason, reason, addr, status);
}

ndsStatus Base::doCallbacksOctet(char *data, size_t numchars, int eomReason, int reason, int addr, epicsTimeStamp timestamp, ndsStatus status)
{
	if (!_portDriver)
		return ndsError;
	return (ndsStatus) _portDriver->doCallbacksOctet(data, numchars, eomReason, reason, addr, status, timestamp);
}

void Base::setThreadName(const std::string& name)
{
	_threadName = name;
}

ndsStatus Base::initThread()
{
    if(epicsThreadCreate(
        _threadName.c_str(),
        epicsThreadPriorityMedium,
        epicsThreadGetStackSize(epicsThreadStackSmall),
        processThread,
        (void*)this) == 0)
    {
        cantProceed(
            "Can't create thread '%s'.",
            _threadName.c_str() );
        return ndsError;
    }
    return ndsSuccess;
}

ndsStatus Base::setIOCInitialization()
{
	NDS_TRC("Base::setEpicsInitialization");
	return ndsSuccess;
}

ndsStatus Base::setIOCInitialized()
{
	NDS_TRC("Base::setEpicsInitialized");
	return ndsSuccess;
}

void Base::processPrivate()
{
	// check if the object initialized and the state is ON
	// call user process member
	process(_stopProcessing);
}

ndsStatus Base::readAsynMessage(asynUser *pasynUser, char *data, size_t maxchars, size_t *nbytesTransfered, int *eomReason)
{
	NDS_TRC("readAsynMessage");

	Message msg;
	readMessage(pasynUser, msg);
	std::string strMsg = msg.str();
	memset(data, 0,  maxchars);
	snprintf(data, maxchars, "%s", strMsg.c_str());

	if ( strMsg.length() > maxchars )
	{
		*nbytesTransfered= maxchars;
		*eomReason=ASYN_EOM_CNT;
	}else
	{
		*nbytesTransfered = strMsg.length();
		*eomReason=ASYN_EOM_END;
	}
	return ndsSuccess;
}

ndsStatus Base::readMessage(asynUser *pasynUser, Message& value)
{
	NDS_TRC("readMessage");
	value.messageType = "ReadMessage";
	value.parameters.insert(
			MessageParameters::value_type("TEXT", "Default handler."));

	value.parameters.insert(
			MessageParameters::value_type("CODE", "-1") );
	return ndsSuccess;
}

ndsStatus Base::updateMessage(const std::string& type, const std::string& text, int code )
{
	NDS_TRC("updateMessage");

	Message value;

	value.messageType = type;
	nds::insert(value, "CODE", code);
	nds::insert(value, "TEXT", text);

	return doCallbacksMessage(value);
}

ndsStatus Base::updateMessage(const std::string& type, int code, const std::string& fmt, ... )
{
	size_t text_size=250;
	char text[text_size];

	snprintf(text, text_size, "%s", fmt.c_str() );

	va_list args;
	va_start (args, fmt);
	vprintf (text, args);
	va_end (args);
	return updateMessage(type, fmt, code );
}


ndsStatus Base::writeAsynMessage(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransfered)
{
	NDS_TRC("writeAsynMessage: %s", data);

	Message msg;
	msg.load(data, numchars, nbytesTransfered);
	writeMessage(pasynUser, msg);
	return ndsSuccess;
}

ndsStatus Base::writeMessage(asynUser *pasynUser, const Message& message)
{
	dispatchWriteMessage(pasynUser, message);
	return ndsSuccess;
}

ndsStatus Base::doCallbacksMessage(const Message& message)
{
	NDS_DBG("id: %d port: %d msg: %s", _interruptIdCommand, _portAddr, message.messageType.c_str() );
	std::string msgTemp = message.str();

	size_t size = MSG_MAX_SIGE;
	char buff[size];

	memset(buff, 0,  size);
	snprintf(buff, size, "%s", msgTemp.c_str());
	return doCallbacksOctet(buff, size, ASYN_EOM_END, _interruptIdCommand, _portAddr );
}

ndsStatus Base::doCallbacksMessage(const std::string& messageType,
                             int code,
                             const std::string& messageText)
{
    Message value;

    value.messageType = messageType;
    nds::insert(value, "CODE", code);
    nds::insert(value, "TEXT", messageText);

    return doCallbacksMessage(value);
}

ndsStatus Base::doCallbacksMessage(const std::string& messageType,
                             int code,
                             int messageId,
                             const std::string& messageText)
{
    Message value;

    value.messageType = messageType;
    nds::insert(value, "CODE", code);
    nds::insert(value, "ID",   messageId);
    nds::insert(value, "TEXT", messageText);

    return doCallbacksMessage(value);
}

ndsStatus Base::dispatchWriteMessage(asynUser *pasynUser, const Message& message)
{
	NDS_TRC("dispatchWriteMessage: %s", message.messageType.c_str() );
	MessageWriteHandlersMap::const_iterator itr =  _messageWriteHandlers.find( message.messageType );
	if ( itr != _messageWriteHandlers.end() )
	{
		for (MessageWriteHandlers::const_iterator itr_handler = itr->second.begin();
				itr_handler != itr->second.end();
				++itr_handler)
		{
		    try
		    {
		    	(*itr_handler)(pasynUser, message);
		    }catch(...)
		    {
		        NDS_ERR( "Handler for '%s' message type throws error.", message.messageType.c_str() );
		    }
		}
	}else
	{
		responseMessagehandlerNotRegistered(pasynUser, message);
	}
	return ndsSuccess;
}

ndsStatus Base::responseMessagehandlerNotRegistered(asynUser *pasynUser, const Message& value)
{
	NDS_TRC("responseMessageHandlerNotRegistered");
	Message response;
	response.messageType = value.messageType;
	response.insert(
			"CODE", "-1" );
	response.insert(
			"TEXT", "\"Type not registered\"" );
	doCallbacksMessage(response);
	return ndsSuccess;
}

ndsStatus Base::registerMessageWriteHandler(const std::string& messageType,
		 MessageWriter writer )
{
	_messageWriteHandlers[messageType].push_back(writer);
	return ndsSuccess;
}

ndsStatus Base::setState(asynUser* pasynUser, epicsInt32 value)
{
	return ndsError;
}

ndsStatus Base::getState(asynUser* pasynUser, epicsInt32 *value)
{
	return ndsError;
}

ndsStatus Base::doCallbacksOctet(char *data, size_t numchars, int eomReason,
        int reason, ndsStatus status)
{
    return doCallbacksOctet(data, numchars, eomReason,
            reason, _portAddr, status);
}

ndsStatus Base::doCallbacksOctet(char *data, size_t numchars, int eomReason,
        int reason, epicsTimeStamp timestamp, ndsStatus status)
{
    return doCallbacksOctet(data, numchars, eomReason,
            reason, _portAddr, timestamp, status);
}

ndsStatus Base::doCallbacksOctetStr(const std::string &data, int eomReason,
                int reason, int addr, ndsStatus status)
{
    if (!_portDriver)
        return ndsError;

    char * buff = (char*)malloc( data.length()+1 );
    memset(buff, 0, data.length()+1);
    snprintf(buff, data.length()+1,"%s", data.c_str() );

    ndsStatus result =  (ndsStatus) _portDriver->doCallbacksOctet(buff,
            data.length()+1, eomReason, reason, addr, status);
    free(buff);

    return result;
}

ndsStatus Base::doCallbacksOctetStr(const std::string &data, int eomReason,
                int reason, int addr, epicsTimeStamp timestamp, ndsStatus status)
{
    if (!_portDriver)
             return ndsError;

    char * buff = (char*)malloc( data.length()+1 );
    memset(buff, 0, data.length()+1);
    snprintf(buff, data.length()+1,"%s", data.c_str() );

    ndsStatus result =  (ndsStatus) _portDriver->doCallbacksOctet(buff,
            data.length()+1, eomReason, reason, addr, status);

    free(buff);
    return result;
}

//ndsStatus Base::doCallbacksOctetStr(const std::string &data, int reason, ndsStatus status)
//{
//    return doCallbacksOctetStr(data, ASYN_EOM_END, reason, _portAddr);
//}
//
//ndsStatus Base::doCallbacksOctetStr(const std::string &data, int reason, epicsTimeStamp timestamp, ndsStatus status)
//{
//    return doCallbacksOctetStr(data, ASYN_EOM_END, reason, _portAddr, timestamp);
//}

ndsStatus Base::doCallbacksOctetStr(const std::string &data, int eomReason,
                int reason, ndsStatus status)
{
    return doCallbacksOctetStr(data, eomReason, reason, _portAddr, status);
}

ndsStatus Base::doCallbacksOctetStr(const std::string &data, int eomReason,
                int reason, epicsTimeStamp timestamp, ndsStatus status)
{
    return doCallbacksOctetStr(data, eomReason, reason, _portAddr, timestamp, status);
}

ndsStatus Base::doCallbacksInt8Array(epicsInt8 *value, size_t nElements, int reason, ndsStatus status)
{
    return doCallbacksInt8Array(value, nElements, reason, _portAddr, status);
}

ndsStatus Base::doCallbacksInt8Array(epicsInt8 *value, size_t nElements,
        int reason, epicsTimeStamp timestamp, ndsStatus status)
{
    return doCallbacksInt8Array(value, nElements,
            reason, _portAddr, timestamp, status);
}

ndsStatus Base::doCallbacksInt16Array(epicsInt16 *value, size_t nElements, int reason, ndsStatus status)
{
    return doCallbacksInt16Array(value, nElements, reason, _portAddr, status);
}

ndsStatus Base::doCallbacksInt16Array(epicsInt16 *value, size_t nElements,
        int reason, epicsTimeStamp timestamp, ndsStatus status)
{
    return doCallbacksInt16Array(value,  nElements,
             reason, _portAddr, timestamp, status);
}

ndsStatus Base::doCallbacksInt32Array(epicsInt32 *value, size_t nElements, int reason, ndsStatus status)
{
    return doCallbacksInt32Array(value, nElements, reason, _portAddr, status);
}

ndsStatus Base::doCallbacksInt32Array(epicsInt32 *value, size_t nElements,
        int reason, epicsTimeStamp timestamp, ndsStatus status)
{
    return doCallbacksInt32Array(value, nElements,
            reason, _portAddr, timestamp, status);
}

ndsStatus Base::doCallbacksFloat32Array(epicsFloat32 *value, size_t nElements, int reason, ndsStatus status)
{
    return doCallbacksFloat32Array(value, nElements, reason, _portAddr, status);
}

ndsStatus Base::doCallbacksFloat32Array(epicsFloat32 *value,
        size_t nElements, int reason, epicsTimeStamp timestamp, ndsStatus status)
{
    return doCallbacksFloat32Array(value,
            nElements, reason, _portAddr, timestamp, status);
}

ndsStatus Base::doCallbacksFloat64Array(epicsFloat64 *value,
        size_t nElements, int reason, ndsStatus status)
{
    return doCallbacksFloat64Array(value,
            nElements, reason, _portAddr, status);
}

ndsStatus Base::doCallbacksFloat64Array(epicsFloat64 *value,
        size_t nElements, int reason, epicsTimeStamp timestamp, ndsStatus status)
{
    return doCallbacksFloat64Array(value,
            nElements, reason, _portAddr, timestamp, status);
}

ndsStatus Base::doCallbacksGenericPointer(void *pointer, int reason)
{
    return doCallbacksGenericPointer(pointer, reason, _portAddr);
}

ndsStatus Base::doCallbacksInt32(epicsInt32 value, int reason, ndsStatus status)
{
    return doCallbacksInt32(value, reason, _portAddr, status);
}

ndsStatus Base::doCallbacksInt32(epicsInt32 value, int reason, epicsTimeStamp timestamp, ndsStatus status)
{
    return doCallbacksInt32(value, reason, _portAddr, timestamp, status);
}

ndsStatus Base::doCallbacksFloat64(epicsFloat64 value, int reason, ndsStatus status)
{
    return doCallbacksFloat64(value, reason, _portAddr, status);
}

ndsStatus Base::doCallbacksFloat64(epicsFloat64 value, int reason, epicsTimeStamp timestamp, ndsStatus status)
{
    return doCallbacksFloat64(value, reason, _portAddr, timestamp, status);
}

ndsStatus Base::doCallbacksUInt32Digital(epicsUInt32 value, int reason, int addr, ndsStatus status)
{
    if (!_portDriver)
        return ndsError;
    return (ndsStatus) _portDriver->doCallbacksUInt32Digital(value, reason, addr, status);
}

ndsStatus Base::doCallbacksUInt32Digital(epicsUInt32 value, int reason, int addr, epicsTimeStamp timestamp, ndsStatus status)
{
    if (!_portDriver)
        return ndsError;
    return (ndsStatus) _portDriver->doCallbacksUInt32Digital(value, reason, addr, status, timestamp);
}

ndsStatus Base::doCallbacksUInt32Digital(epicsUInt32 value, int reason, ndsStatus status)
{
    return doCallbacksUInt32Digital(value, reason, _portAddr, status);
}

ndsStatus Base::doCallbacksUInt32Digital(epicsUInt32 value, int reason, epicsTimeStamp timestamp, ndsStatus status)
{
    return doCallbacksUInt32Digital(value, reason, _portAddr, timestamp, status);
}

ndsStatus Base::getUInt32Digital(asynUser *pasynUser, epicsUInt32 *value, epicsUInt32 mask)
{
    return ndsSuccess;
}

ndsStatus Base::setUInt32Digital(asynUser *pasynUser, epicsUInt32 value, epicsUInt32 mask)
{
    return ndsSuccess;
}

ndsStatus Base::setEnabled(asynUser* pasynUser, epicsInt32 value)
{
    _isEnabled=value;
    return ndsSuccess;
}

ndsStatus Base::getEnabled(asynUser* pasynUser, epicsInt32 *value)
{
    *value = _isEnabled;
    return ndsSuccess;
}

ndsStatus Base::lock()
{
    if (!_portDriver)
    {
        NDS_ERR("Object is not registered within a asynPort.");
        return ndsError;
    }
    return (ndsStatus) _portDriver->lock();
}

ndsStatus Base::unlock()
{
    if (!_portDriver)
    {
        NDS_ERR("Object is not registered within a asynPort.");
        return ndsError;
    }
    return (ndsStatus) _portDriver->unlock();
}

#define NDS_CHANNEL_CLASS Base
#define NDS_INSTANTIATE NDS_IMPLEMENT_STUBS
#define NDS_INSTANTIATE_OCTET NDS_IMPLEMENT_OCTET_STUBS
#define NDS_INSTANTIATE_ARRAY NDS_IMPLEMENT_ARRAY_STUBS
	NDS_INSTANTIATE_STUB_FOR_EACH_TYPE
#undef NDS_INSTANTIATE
#undef NDS_INSTANTIATE_OCTET
#undef NDS_INSTANTIATE_ARRAY
#undef NDS_CHANNEL_CLASS

